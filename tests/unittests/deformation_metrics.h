#pragma once

#include "electro-mechanics/deformation_metrics.h"


TEST_SUITE("deformation metrics") {
  using namespace DeformationMetrics;

  std::array<Vec3, 8> create_pos(num x, num y, num z, num s) {
    /* clang-format off */
    return {{
        Vec3(x,     y,     z),
        Vec3(x + s, y,     z),
        Vec3(x + s, y + s, z),
        Vec3(x,     y + s, z),

        Vec3(x,     y,     z + s),
        Vec3(x + s, y,     z + s),
        Vec3(x + s, y + s, z + s),
        Vec3(x,     y + s, z + s)
    }};
    /* clang-format on */
  }

  TEST_CASE("volume") {
    auto pos = create_pos(0, 0, 0, 1);
    FAST_CHECK_EQ(Volume(pos), Approx(1.));

    pos = create_pos(5, 4, 3, 1);
    FAST_CHECK_EQ(Volume(pos), Approx(1.));

    pos = create_pos(1, 1, 1, 2);
    FAST_CHECK_EQ(Volume(pos), Approx(2. * 2 * 2));

    pos = create_pos(9, 7, 5, 3);
    FAST_CHECK_EQ(Volume(pos), Approx(3. * 3 * 3));
  }
}