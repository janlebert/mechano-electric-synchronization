#pragma once
#include <algorithm>

#include "helper/arrays.h"

TEST_CASE("iterators") {
  constexpr size_t N = 10;
  std::array<int, N> a;

  SUBCASE("RawIterator") {
    for (int i = 0; i < N; i++) {
      a[i] = i;
    }

    auto it  = RawIterator<int>(a.data());
    auto end = RawIterator<int>(a.data() + N);
    FAST_CHECK_EQ(end - it, N);

    for (int i = 0; it < end; it++, i++) {
      FAST_CHECK_EQ(a[i], *it);
    }
    FAST_CHECK_EQ(std::accumulate(RawIterator<int>(a.data()), RawIterator<int>(a.data() + N), 0),
                  std::accumulate(a.begin(), a.end(), 0));

    for (int i = 0; i < N; i++) {
      a[i] = i;
    }

    std::array<int, N> sorted;
    it  = RawIterator<int>(a.data() + N) - N;
    end = RawIterator<int>(a.data()) + N;
    std::partial_sort_copy(it, end, sorted.begin(), sorted.end());
    FAST_CHECK_EQ(a, sorted);
  }

  SUBCASE("StrideIterator") {
    for (int i = 0; i < N; i++) {
      a[i] = i;
    }

    int skip = 2;

    using it_t = std::array<int, N>::iterator;
    StrideIterator<it_t> first(a.begin(), skip);
    StrideIterator<it_t> last(a.end(), skip);

    for (int i = 0; first < last; first++, i += skip) {
      FAST_CHECK_EQ(*first, a[i]);
    }

    first = StrideIterator<it_t>(a.begin() + 1, 2 * skip);
    last  = StrideIterator<it_t>(a.end(), 2 * skip);
    for (int i = 1; first < last; first++, i += 2 * skip) {
      FAST_CHECK_EQ(*first, a[i]);
    }
  }

  SUBCASE("IdxVecIterator") {
    IdxVec dims(7, 9, 11);

    auto begin = IdxVecIterator::begin(dims);
    auto end   = IdxVecIterator::end(dims);

    int sum = 0;
    std::vector<IdxVec> vpos;
    vpos.reserve(dims.element_product());

    for (auto it = begin; it != end; it++) {
      vpos.push_back(*it);
      sum += 1;
    }

    FAST_CHECK_EQ(sum, dims.element_product());

    std::vector<IdxVec> vpos2;
    vpos2.reserve(dims.element_product());
    for (int z = 0; z < dims[2]; z++) {
      for (int y = 0; y < dims[1]; y++) {
        for (int x = 0; x < dims[0]; x++) {
          vpos2.emplace_back(IdxVec(x, y, z));
        }
      }
    }
    FAST_CHECK_EQ(vpos.size(), vpos2.size());
    FAST_CHECK_EQ(vpos, vpos2);
  }

  SUBCASE("PosFuncHolder") {
    IdxVec dims(7, 9, 11);

    Array3<int> a(dims);
    auto f = [&a](IdxVec pos) -> int& {
      return a(pos);
    };

    PosFuncHolder<int, decltype(f)> h(dims, f);
    FAST_CHECK_EQ(std::distance(h.begin(), h.end()), dims.element_product());

    int tmp = 0;
    for (auto& v : a) {
      v = tmp;
      tmp++;
    }
    FAST_CHECK_EQ(std::accumulate(a.begin(), a.end(), 0), std::accumulate(h.begin(), h.end(), 0));
  }
}