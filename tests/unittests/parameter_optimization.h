#pragma once

#include <unordered_set>

#include "helper/parameter_optimization.h"

TEST_CASE("grid_scan") {

  SUBCASE("1 variable") {
    std::unordered_set<string> xs;
    xs.reserve(101);

    auto runsim = [&xs](num x1) -> num {
      const string str = fmt::format("{}", x1);
      if (xs.count(str)) {
        fmt::print("FAIL: {} in xs, size {}\n", str, xs.size());
        FAST_WARN_UNARY_FALSE(xs.count(str));
      }

      xs.insert(str);

      return x1;
    };

    ParameterOptimization::find_min_grid<1>(runsim, {0}, {100}, {false}, 101);

    FAST_CHECK_EQ(xs.size(), 101);
    FAST_CHECK_EQ(xs.count(fmt::to_string(0)), 1);
    FAST_CHECK_EQ(xs.count(fmt::to_string(1)), 1);
    FAST_CHECK_EQ(xs.count(fmt::to_string(26)), 1);
    FAST_CHECK_EQ(xs.count(fmt::to_string(50)), 1);
    FAST_CHECK_EQ(xs.count(fmt::to_string(100)), 1);
  }

  SUBCASE("2 variables") {
    std::unordered_set<string> xs;
    xs.reserve(100);

    auto runsim = [&xs](num x1, num x2) -> num {
      const string str = Vec2(x1, x2).to_string();
      if (xs.count(str)) {
        fmt::print("FAIL: {} in xs, size {}\n", str, xs.size());
        FAST_WARN_UNARY_FALSE(xs.count(str));
      }

      xs.insert(str);

      return x1;
    };

    ParameterOptimization::find_min_grid<2>(runsim, {0, 1}, {9, 100}, {false, false}, 100);

    FAST_CHECK_EQ(xs.size(), 100);
    FAST_CHECK_EQ(xs.count(Vec2(0, 1).to_string()), 1);
    FAST_CHECK_EQ(xs.count(Vec2(9, 100).to_string()), 1);
  }

  SUBCASE("3 variables") {
    std::unordered_set<string> xs;
    xs.reserve(1000);

    auto runsim = [&xs](num x1, num x2, num x3) -> num {
      const string str = Vec3(x1, x2, x3).to_string();
      if (xs.count(str)) {
        fmt::print("FAIL: {} in xs, size {}\n", str, xs.size());
        FAST_WARN_UNARY_FALSE(xs.count(str));
      }

      xs.insert(str);

      return x1 + std::abs(x2) + x3;
    };

    auto r = ParameterOptimization::find_min_grid<3>(runsim, {0, -5, 10}, {1, 0, 1000},
                                                     {false, false, false}, 1000);

    FAST_CHECK_EQ(xs.size(), 1000);
    FAST_CHECK_UNARY(xs.count(Vec3(0, -5, 10).to_string()));
    FAST_CHECK_UNARY(xs.count(Vec3(1, 0, 1000).to_string()));

    FAST_CHECK_EQ(r.second, static_cast<num>(10));
  }
}
