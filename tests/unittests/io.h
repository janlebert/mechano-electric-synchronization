#pragma once

#include "helper/io.h"
#include "helper/arrays.h"

TEST_CASE("io") {
  string tmpfilepath = "unittest_outstream_tmpfile.tmp";

  SUBCASE("Empty InStream") {
    InStream<num> in("");
    FAST_CHECK_EQ(in.getSkip(), 1);
    in.setSkip(3);
    FAST_CHECK_EQ(in.getSkip(), 3);
    in.setSkip(1);

    in.clear();
    FAST_CHECK_UNARY_FALSE(in.in.good());
  }

  SUBCASE("OutStream and InStream basics") {
    {  // create tmp file
      OutStream out(tmpfilepath, true);
      out.write<num>(0);
      out.write<num>(1);
      out.write<num>(3.16);

      io::writeToFile(out.out, static_cast<num>(3));
    }

    REQUIRE(io::fileExists(tmpfilepath));

    InStream<num> in(tmpfilepath);
    REQUIRE(in.in.good());

    CHECK_EQ(io::getFileSize(in.in), sizeof(num) * 4);
    CHECK_EQ(in.read(1), 1);
    CHECK_EQ(in.read(2), Approx(3.16));
    CHECK_EQ(in.read(0), 0);
    CHECK_EQ(in.read(3), 3);

    CHECK_EQ(io::readFromFile<num>(in.in, 2), Approx(3.16));
  }

  SUBCASE("OutStream and InStream array") {
    io::deleteFile(tmpfilepath);

    Array3<num> a(7, 11, 13);

    num i = 0;
    for (auto& v : a) {
      v = i;
      i++;
    }

    FAST_CHECK_EQ(a.size(), 7 * 11 * 13);
    FAST_CHECK_EQ(i, 7 * 11 * 13);

    a.save(tmpfilepath);

    Array3<num> b(7, 11, 13);
    b.load(tmpfilepath, 0);
    FAST_CHECK_EQ(a, b);
  }

  io::deleteFile(tmpfilepath);
}
