#include "doctest/doctest.h"

#include "parameters.h"

using doctest::Approx;

// Forward declarations
Parameters prm;

string format_number(num x);

template <typename T>
vector<string> format_numbers(T v);

#include "./deformation_metrics.h"
#include "./hilbert.h"
#include "./assim_helpers.h"

TEST_SUITE_BEGIN("helpers");
#include "./arrays.h"
#include "./vectors.h"
#include "./threading.h"
#include "./iterators.h"
#include "./io.h"
#include "./parameter_optimization.h"
TEST_SUITE_END();

// main() entry point is automatically added

string format_number(num x) {
  auto s = fmt::sprintf("%4.4f", x);
  if (s == "-0.0000"s) {
    s = "0.0000"s;
  }
  return s;
}
template<typename T>
vector<string> format_numbers(T v) {
  vector<string> r;
  for (const auto &x : v) {
    r.emplace_back(format_number(x));
  }
  return r;
}
