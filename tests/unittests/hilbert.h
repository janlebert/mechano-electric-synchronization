#pragma once

#include <regex>
#include "fmt/printf.h"

#include "data_assim/filters/hilbert.h"

const std::regex regx_number(R"(\-?\d\.?\d*)");

string format_number(fftw::stdcomplex x) {
  return fmt::sprintf("(%4.4f, %4.4f)", x.real(), x.imag());
}

vector<num> extract_numbers(string matlab_result) {
  vector<num> r;
  std::smatch sm;
  while (std::regex_search(matlab_result, sm, regx_number)) {

    num x = std::stod(sm[0].str());
    r.push_back(x);
    matlab_result = sm.suffix();
  }

  return r;
}

TEST_SUITE("hilbert transformation") {

  void test_hilbert_1D(vector<num> in, string matlab_hilbert_s, string matlab_angle_s) {
    const int N        = in.size();
    array<int, 1> dims = {{N}};

    vector<fftw::stdcomplex> kspace(N);
    vector<num> out(N);

    auto plans = Hilbert::createPlans(dims.data(), in.data(), kspace.data());

    Hilbert::analyticSignal(plans, kspace, out.data());

    SUBCASE("real part of hilbert transformation should be original data") {
      vector<string> in_vec;
      vector<string> in_vec_verify;
      for (int j = 0; j < N; j++) {
        in_vec_verify.emplace_back(format_number(kspace[j].real()));
        in_vec.emplace_back(format_number(in[j]));
      }

      CHECK(in_vec == in_vec_verify);
    }

    SUBCASE("calculated hilbert transformation and hilbert phase should match matlab output") {
      vector<string> matlab_hilbert;
      vector<string> matlab_angle;

      SUBCASE("matlab string parsing should return correct number of elements") {
        matlab_hilbert = format_numbers(extract_numbers(matlab_hilbert_s));
        matlab_angle   = format_numbers(extract_numbers(matlab_angle_s));

        CHECK(matlab_hilbert.size() == N);
        CHECK(matlab_angle.size() == N);
      }

      vector<string> hilbert_vec;
      vector<string> angle_vec;
      for (int j = 0; j < N; j++) {
        hilbert_vec.emplace_back(format_number(kspace[j].imag()));
        angle_vec.emplace_back(format_number(out[j]));
      }

      CHECK(matlab_hilbert == hilbert_vec);
      CHECK(matlab_angle == angle_vec);
    }

    fftw::destroy_plan(plans.first);
    fftw::destroy_plan(plans.second);
  }

  TEST_CASE("1D Hilbert transform") {
    SUBCASE("constant data") {
      // matlab -nodesktop -r "x=hilbert([1 1 1 1 1 1]);a=angle(x);disp(imag(x));disp(a);quit;"
      vector<num> in = {1, 1, 1, 1, 1, 1};
      test_hilbert_1D(in, "0 0 0 0 0 0", "0 0 0 0 0 0");
    }

    SUBCASE("even numbered 1D data from matlab documentation") {
      // matlab -nodesktop -r "x=hilbert([1 2 3 4]);a=angle(x);disp(imag(x));disp(a);quit;"
      vector<num> in4 = {1, 2, 3, 4};
      test_hilbert_1D(in4, "1    -1    -1     1", "0.7854   -0.4636   -0.3218    0.2450");
    }

    SUBCASE("odd numbered 1D data") {
      // matlab -nodesktop -r "x=hilbert([1 2 3 4 5 6 7 8 9]);a=angle(x);disp(imag(x));disp(a);quit;"
      vector<num> in9 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
      test_hilbert_1D(in9,
                      "4.6929   -0.9784   -0.6144   -2.3465   -1.5074   -2.3465   -0.6144   -0.9784    4.6929",
                      "1.3608   -0.4550   -0.2020   -0.5305   -0.2928   -0.3728   -0.0875   -0.1217    0.4806");
    }
  }

  TEST_CASE("FFTW stride test") {
    SUBCASE("array") {
      array<int, 1> dims = {{4}};

      array<num, 4> trace = {{0, 1, 2, 3}};
      array<fftw::stdcomplex, 4> trace_fftw{{}};
      array<fftw::stdcomplex, 4> trace_precomputed = {{{6, 0}, {-2, 2}, {-2, 0}, {0, 0}}};

      fftw::plan plan_1d = fftw::plan_dft_r2c(
          1, dims.data(), trace.data(), reinterpret_cast<fftw::complex *>(trace_fftw.data()), FFTW_ESTIMATE);
      fftw::execute(plan_1d);
      fftw::destroy_plan(plan_1d);
      FAST_CHECK_EQ(trace_fftw, trace_precomputed);

      // Now do the same with strides
      array<num, 4 * 25> d;
      for (int t = 0; t < 4; t++) {
        for (int y = 0; y < 5; y++) {
          for (int x = 0; x < 5; x++) {
            d[t * 25 + y * 5 + x] = trace[t];
          }
        }
      }

      array<fftw::stdcomplex, 4 * 25> o;

      for (int t = 0; t < 4; t++) {
        for (int y = 0; y < 5; y++) {
          for (int x = 0; x < 5; x++) {
            o[t * 25 + y * 5 + x] = fftw::stdcomplex(0, 0);
          }
        }
      }

      fftw::complex *out = reinterpret_cast<fftw::complex *>(o.data());

      int *n        = dims.data();
      fftw::plan plan = fftw::plan_many_dft_r2c(1, n, 25, d.data(), n, 25, 1, out, n, 25, 1, FFTW_ESTIMATE);
      fftw::execute(plan);

      array<fftw::stdcomplex, 4> tmp_trace;
      bool fftw_is_computed_correctly = true;
      for (int y = 0; y < 5; y++) {
        for (int x = 0; x < 5; x++) {
          for (int t = 0; t < 4; t++) {
            tmp_trace[t] = o[t * 25 + y * 5 + x];
          }
          fftw_is_computed_correctly &= tmp_trace == trace_fftw;
        }
      }
      FAST_CHECK_UNARY(fftw_is_computed_correctly);

      fftw::destroy_plan(plan);
    }

    SUBCASE("CircularArray") {
      array<int, 1> dims                           = {{4}};
      array<fftw::stdcomplex, 4> trace_precomputed = {{{6, 0}, {-2, 2}, {-2, 0}, {0, 0}}};

      CircularArray<num> dd(5, 5, 5, 4);
      num *d = dd.data();
      for (const auto &pos : dd.positions) {
        int i = 0;
        for (auto it = dd.begin(pos); it < dd.end(pos); it++, i++) {
          *it = i;
        }
      }

      CircularArray<fftw::stdcomplex> oo(5, 5, 5, 4);
      fftw::stdcomplex *o = oo.data();
      oo.fill({0, 0});

      fftw::complex *out = reinterpret_cast<fftw::complex *>(o);

      int *n        = dims.data();
      fftw::plan plan = fftw::plan_many_dft_r2c(1, n, 25 * 5, d, n, 25 * 5, 1, out, n, 25 * 5, 1, FFTW_ESTIMATE);
      fftw::execute(plan);


      bool fftw_is_computed_correctly = true;
      for (auto &pos : oo.positions) {
        auto tt = oo.timeTrace(pos);
        fftw_is_computed_correctly &= std::equal(trace_precomputed.begin(), trace_precomputed.end(), tt.begin());
      }

      FAST_CHECK_UNARY(fftw_is_computed_correctly);

      fftw::destroy_plan(plan);
    }
  }

  template <typename T>
  void check_CircularArray_trace(CircularArray<T> & a, vector<T> & trace, string info = "") {
    bool r = true;
    for (long t = a.get_t_head(); t <= a.get_t_end(); t++) {
      for (auto it = a.begin(t); it < a.end(t); it++) {
        r &= format_number(*it) == format_number(trace[t]);
        if (!r) {
          fmt::print("{} {} != {}\n", info, format_number(*it), format_number(trace[t]));
        }
      }
    }

    if (!info.empty()) {
      INFO(info);
    }
    FAST_CHECK_UNARY(r);
  }

  void test_hilbert_class(
      Vector4<int> dimensions, string input_s, string output_s, string phase_s, const bool use_static_fcts) {
    array<int, 1> dims = {{dimensions[3]}};

    vector<num> inv    = extract_numbers(input_s);
    vector<num> outv   = extract_numbers(output_s);
    vector<num> phasev = extract_numbers(phase_s);
    vector<fftw::stdcomplex> a_signalv(dims[0]);

    FAST_CHECK_EQ(dimensions[3], inv.size());
    FAST_CHECK_EQ(dimensions[3], outv.size());
    FAST_CHECK_EQ(dimensions[3], phasev.size());

    CircularArray<num> in(dimensions);
    CircularArray<num> in_correct(dimensions);
    CircularArray<num> phase(dimensions);
    CircularArray<fftw::stdcomplex> a_signal(dimensions);
    a_signal.fill({0, 0});
    phase.fill(0);

    for (const auto &pos : in.positions) {
      for (int t = in.get_t_head(); t <= in.get_t_end(); t++) {
        in(pos, t)         = inv[t];
        in_correct(pos, t) = inv[t];
      }
    }
    for (int t = in.get_t_head(); t <= in.get_t_end(); t++) {
      a_signalv[t] = {inv[t], outv[t]};
    }

    if (use_static_fcts) {

      auto nxyz  = in.Nx() * in.Ny() * in.Nz();
      auto plans = Hilbert::createPlans(dims.data(), in.data(), a_signal.data(), nxyz, nxyz);

      Hilbert::analyticSignal(plans, a_signal.data(), a_signal.size(), phase.data(), nxyz);

      fftw::destroy_plan(plans.first);
      fftw::destroy_plan(plans.second);

    } else {

      Hilbert::CircularArrayHolder hilbert(in, &phase);
      hilbert.apply();
      a_signal = std::move(hilbert.analytic_signal);
    }

    FAST_CHECK_UNARY(in == in_correct);
    check_CircularArray_trace(a_signal, a_signalv, "comparing analytic signal");
    check_CircularArray_trace(phase, phasev, "comparing phase");
  }

  TEST_CASE("Hilbert CircularArray") {
    SUBCASE("constant data") {
      test_hilbert_class({7, 11, 13, 5}, "1 1 1 1 1", "0 0 0 0 0", "0 0 0 0 0", false);
      test_hilbert_class({7, 11, 13, 5}, "1 1 1 1 1", "0 0 0 0 0", "0 0 0 0 0", true);
    }

    SUBCASE("even data") {
      test_hilbert_class({7, 5, 11, 4}, "1 2 3 4", "1 -1 -1 1", "0.7854   -0.4636   -0.3218    0.2450", false);
      test_hilbert_class({7, 5, 11, 4}, "1 2 3 4", "1 -1 -1 1", "0.7854   -0.4636   -0.3218    0.2450", true);
    }

    // Currently broken
    SUBCASE("odd data") {
      string in    = "1 2 3 4 5 6 7 8 9";
      string out   = "4.6929   -0.9784   -0.6144   -2.3465   -1.5074   -2.3465   -0.6144   -0.9784    4.6929";
      string phase = "1.3608   -0.4550   -0.2020   -0.5305   -0.2928   -0.3728   -0.0875   -0.1217    0.4806";
      //test_hilbert_class({3, 5, 11, 9}, in, out, phase, false);
      //test_hilbert_class({3, 5, 11, 9}, in, out, phase, true);
    }
  }
}