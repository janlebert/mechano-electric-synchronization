#pragma once

#include "helper/arrays.h"
using std::abs;

TEST_CASE("Array1") {
  Array1<num> a(7);
  FAST_CHECK_EQ(a.Nx(), 7);
  FAST_CHECK_EQ(a.size(), 7);
  FAST_CHECK_EQ(std::distance(a.begin(), a.end()), a.size());

  a.Load(0.f);
  num sum = 0;
  for (int i = 0; i < a.Nx(); i++) {
    sum += a(i);
  }

  FAST_REQUIRE_EQ(sum, Approx(0));
  FAST_REQUIRE_EQ(sum, Approx(std::accumulate(a.begin(), a.end(), 0)));

  a.Load(2.f);
  sum = 0;
  for (int i = 0; i < a.Nx(); i++) {
    sum += a(i);
  }
  FAST_CHECK_EQ(sum, 2 * a.size());
  FAST_CHECK_EQ(sum, std::accumulate(a.begin(), a.end(), 0));

  sum = 0;
  for (int i = 0; i < a.Nx(); i++) {
    a(i) = i * 1e3;
    sum += a(i);
  }

  FAST_CHECK_EQ(a(3), 3 * 1e3);

  std::vector<num> vec;
  vec.reserve(a.size());
  num sum_it = 0;
  for (auto &x : a) {
    sum_it += x;
    vec.push_back(x);
  }

  FAST_CHECK_EQ(vec.size(), a.size());
  FAST_CHECK_EQ(sum, sum_it);
  FAST_REQUIRE_UNARY(std::equal(vec.begin(), vec.end(), a.begin()));

  Array1<num> b(a.size());
  b.Load(vec.data());
  FAST_CHECK_NE(b.data(), vec.data());
  FAST_CHECK_UNARY(a == b);
  b(1) = 0;
  FAST_CHECK_UNARY(a != b);
}

TEST_CASE("Array2") {
  Array2<num> a(7, 11);
  FAST_CHECK_EQ(a.Nx(), 7);
  FAST_CHECK_EQ(a.Ny(), 11);
  FAST_CHECK_EQ(a.dimensions(), Vector2<int>(7, 11));
  FAST_CHECK_EQ(a.size(), 7 * 11);
  FAST_CHECK_EQ(std::distance(a.begin(), a.end()), a.size());


  a.Load(0.f);
  num sum = 0;
  for (int i = 0; i < a.Nx(); i++) {
    for (int j = 0; j < a.Ny(); j++) {
      sum += a(i, j);
    }
  }

  FAST_REQUIRE_EQ(sum, Approx(0));
  FAST_REQUIRE_EQ(sum, Approx(std::accumulate(a.begin(), a.end(), 0)));

  a.Load(2.f);
  sum = 0;
  for (int i = 0; i < a.Nx(); i++) {
    for (int j = 0; j < a.Ny(); j++) {
      sum += a(i, j);
    }
  }
  FAST_CHECK_EQ(sum, 2 * a.size());
  FAST_CHECK_EQ(sum, std::accumulate(a.begin(), a.end(), 0));

  sum = 0;
  for (int i = 0; i < a.Nx(); i++) {
    for (int j = 0; j < a.Ny(); j++) {
      a(i, j) = i * 1e3 + j * 1e2;
      sum += a(i, j);
    }
  }

  FAST_CHECK_EQ(a(3, 8), 3 * 1e3 + 8 * 1e2);
  FAST_CHECK_EQ(a(8 * 7 + 3), 3 * 1e3 + 8 * 1e2);

  std::vector<num> vec;
  vec.reserve(a.size());
  num sum_it = 0;
  for (auto &x : a) {
    sum_it += x;
    vec.push_back(x);
  }

  FAST_CHECK_EQ(vec.size(), a.size());
  FAST_CHECK_EQ(sum, sum_it);
  FAST_REQUIRE_UNARY(std::equal(vec.begin(), vec.end(), a.begin()));

  Array2<num> b(a.dimensions());
  b.Load(vec.data());
  FAST_CHECK_NE(b.data(), vec.data());
  FAST_CHECK_UNARY(a == b);
  b(1, 1) = 0;
  FAST_CHECK_UNARY(a != b);
}

TEST_CASE("Array3") {
  Array3<num> a(7, 11, 13);
  FAST_CHECK_EQ(a.Nx(), 7);
  FAST_CHECK_EQ(a.Ny(), 11);
  FAST_CHECK_EQ(a.Nz(), 13);
  FAST_CHECK_EQ(a.dimensions(), Vector3<int>(7, 11, 13));
  FAST_CHECK_EQ(a.size(), 7 * 11 * 13);
  FAST_CHECK_EQ(std::distance(a.begin(), a.end()), a.size());


  a.Load(0.f);
  num sum = 0;
  for (int i = 0; i < a.Nx(); i++) {
    for (int j = 0; j < a.Ny(); j++) {
      for (int k = 0; k < a.Nz(); k++) {
        sum += a(i, j, k);
      }
    }
  }

  FAST_REQUIRE_EQ(sum, Approx(0));
  FAST_REQUIRE_EQ(sum, Approx(std::accumulate(a.begin(), a.end(), 0)));

  a.Load(2.f);
  sum = 0;
  for (auto &pos : a.positions) {
    sum += a(pos);
  }
  FAST_CHECK_EQ(sum, 2 * a.size());
  FAST_CHECK_EQ(sum, std::accumulate(a.begin(), a.end(), 0));

  sum = 0;
  for (int i = 0; i < a.Nx(); i++) {
    for (int j = 0; j < a.Ny(); j++) {
      for (int k = 0; k < a.Nz(); k++) {
        a(i, j, k) = i * 1e3 + j * 1e2 + k;
        sum += a(i, j, k);
      }
    }
  }

  FAST_CHECK_EQ(a(IdxVec(3, 8, 4)), 3 * 1e3 + 8 * 1e2 + 4);
  FAST_CHECK_EQ(a(4 * 7 * 11 + 8 * 7 + 3), 3 * 1e3 + 8 * 1e2 + 4);

  std::vector<num> vec;
  vec.reserve(a.size());
  num sum_it = 0;
  for (auto &x : a) {
    sum_it += x;
    vec.push_back(x);
  }

  FAST_CHECK_EQ(vec.size(), a.size());
  FAST_CHECK_EQ(sum, sum_it);
  FAST_REQUIRE_UNARY(std::equal(vec.begin(), vec.end(), a.begin()));

  Array3<num> b(a.dimensions());
  b.Load(vec.data());
  FAST_CHECK_NE(b.data(), vec.data());
  FAST_CHECK_UNARY(a == b);
  b(1, 1, 1) = 0;
  FAST_CHECK_UNARY(a != b);

  Array2<num> c2 = b[2];
  FAST_CHECK_EQ(c2.Nx(), b.Nx());
  FAST_CHECK_EQ(c2.Ny(), b.Ny());
  FAST_CHECK_EQ(c2.size(), 7 * 11);
  FAST_CHECK_EQ(c2.data(), b.data() + 2 * 7 * 11);
  c2(3, 3) = 10;
  FAST_CHECK_EQ(b(3, 3, 2), 10);
}

TEST_CASE("Array4") {
  Array4<num> a(7, 11, 13, 9);
  FAST_CHECK_EQ(a.Nx(), 7);
  FAST_CHECK_EQ(a.Ny(), 11);
  FAST_CHECK_EQ(a.Nz(), 13);
  FAST_CHECK_EQ(a.Nt(), 9);
  FAST_CHECK_EQ(a.dimensions(), Vector4<int>(7, 11, 13, 9));
  FAST_CHECK_EQ(a.size(), 7 * 11 * 13 * 9);


  a.Load(0.);

  num sum = 0;
  for (int i = 0; i < a.Nx(); i++) {
    for (int j = 0; j < a.Ny(); j++) {
      for (int k = 0; k < a.Nz(); k++) {
        for (int m = 0; m < a.Nt(); m++) {
          sum += abs(a(i, j, k, m));
        }
      }
    }
  }

  FAST_REQUIRE_EQ(sum, Approx(0));
  FAST_REQUIRE_EQ(a.size(), std::distance(a.begin(), a.end()));
  FAST_REQUIRE_EQ(Approx(sum), std::accumulate(a.begin(), a.end(), 0.f));
  FAST_REQUIRE_EQ(a.Nt(), std::distance(a.begin({5, 2, 8}), a.end({5, 2, 8})));
  FAST_REQUIRE_EQ(Approx(sum), std::accumulate(a.begin({5, 2, 8}), a.end({5, 2, 8}), 0.f));

  a.Load(2.);
  for (int t = 0; t < a.Nt(); t++) {
    for (auto &pos : a.positions) {
      sum += a(pos, t);
    }
  }

  FAST_CHECK_EQ(sum, 2 * a.size());
  FAST_REQUIRE_EQ(Approx(sum), std::accumulate(a.begin(), a.end(), 0.f));
  FAST_REQUIRE_EQ(a.Nt() * 2.f, std::accumulate(a.begin({5, 2, 8}), a.end({5, 2, 8}), 0.f));

  sum = 0;
  for (int i = 0; i < a.Nx(); i++) {
    for (int j = 0; j < a.Ny(); j++) {
      for (int k = 0; k < a.Nz(); k++) {
        for (int m = 0; m < a.Nt(); m++) {
          a(i, j, k, m) = i * 1e2 + j * 1e1 + k + m * 1e-1;
          sum += a(i, j, k, m);
        }
      }
    }
  }

  num tmp = 3 * 1e2 + 8 * 1e1 + 4 + 2 * 1e-1;
  FAST_CHECK_EQ(a(IdxVec(3, 8, 4), 2), tmp);
  FAST_CHECK_EQ(a(2 * 7 * 11 * 13 + 4 * 7 * 11 + 8 * 7 + 3), tmp);

  std::vector<num> vec;
  vec.reserve(a.size());
  num sum_it = 0;
  for (auto &x : a) {
    sum_it += x;
    vec.push_back(x);
  }

  FAST_CHECK_EQ(vec.size(), a.size());
  FAST_CHECK_EQ(sum, Approx(sum_it).epsilon(0.0001));
  FAST_REQUIRE_UNARY(std::equal(vec.begin(), vec.end(), a.begin()));

  Array4<num> b(a.dimensions());
  b.Load(vec.data());
  FAST_CHECK_UNARY(a == b);
  b(1, 1, 1, 0) = 0;
  FAST_CHECK_UNARY(a != b);

  Array3<num> c2 = b[2];
  FAST_CHECK_EQ(c2.Nx(), b.Nx());
  FAST_CHECK_EQ(c2.Ny(), b.Ny());
  FAST_CHECK_EQ(c2.Nz(), b.Nz());
  FAST_CHECK_EQ(c2.size(), 7 * 11 * 13);
  FAST_CHECK_EQ(c2.data(), b.data() + 2 * 7 * 11 * 13);
  c2(3, 3, 3) = 10;
  FAST_CHECK_EQ(c2(3 * 7 * 11 + 3 * 7 + 3), 10);
  FAST_CHECK_EQ(b(2 * 7 * 11 * 13 + 3 * 7 * 11 + 3 * 7 + 3), 10);
  FAST_CHECK_EQ(b(3, 3, 3, 2), 10);
}

TEST_CASE("Array:: free functions") {
  Array3<num> a(7, 11, 13);
  a.Load(11.11);
  a(5, 5, 5) = -7.77;
  FAST_CHECK_EQ(Array::max(a), Approx(11.11));
  FAST_CHECK_EQ(Array::min(a), Approx(-7.77));

  std::pair<num, num> minmax = {-7.77, 11.11};
  FAST_CHECK_EQ(Array::minmax(a), minmax);


  Array3<num> out(a.dimensions());
  Array::threshold(a, out, 0, std::less<num>());
  FAST_CHECK_EQ(out(5, 5, 5), Approx(-7.77));
  FAST_CHECK_EQ(Array::max(out), 0);
  FAST_CHECK_EQ(Array::min(out), Approx(-7.77));
}

TEST_CASE("CircularArray") {
  void test_circulararray(int buffer_multiplier);

  SUBCASE("CircularArray buffer_multiplier 1") { test_circulararray(1); }
  SUBCASE("CircularArray buffer_multiplier 2") { test_circulararray(2); }
  SUBCASE("CircularArray buffer_multiplier 3") { test_circulararray(3); }
}

void test_circulararray(int buffer_multiplier) {
  CircularArray<num> a(7, 11, 13, 9, buffer_multiplier);

  // test basic consistency
  FAST_CHECK_EQ(a.Nx(), 7);
  FAST_CHECK_EQ(a.Ny(), 11);
  FAST_CHECK_EQ(a.Nz(), 13);
  FAST_CHECK_EQ(a.Nt(), 9);
  FAST_CHECK_EQ(a.get_t_head(), 0);
  FAST_CHECK_EQ(a.get_t_end(), 8);
  FAST_CHECK_EQ(a.dimensions(), Vector4<int>(7, 11, 13, 9));
  FAST_CHECK_EQ(a.size(), 7 * 11 * 13 * 9);

  num sum = 0;
  for (int i = 0; i < a.Nx(); i++) {
    for (int j = 0; j < a.Ny(); j++) {
      for (int k = 0; k < a.Nz(); k++) {
        for (int m = 0; m < a.Nt(); m++) {
          sum += a(i, j, k, m);
        }
      }
    }
  }
  FAST_CHECK_EQ(sum, 0);

  sum = 0;
  a.fill(1.);
  for (int i = 0; i < a.Nx(); i++) {
    for (int j = 0; j < a.Ny(); j++) {
      for (int k = 0; k < a.Nz(); k++) {
        for (int m = 0; m < a.Nt(); m++) {
          sum += a(i, j, k, m);
        }
      }
    }
  }
  FAST_CHECK_EQ(sum, a.size());

  // test iterators
  FAST_REQUIRE_EQ(a.size(), std::distance(a.begin(), a.end()));
  FAST_REQUIRE_EQ(a.Nt(), std::distance(a.begin({5, 2, 8}), a.end({5, 2, 8})));
  FAST_CHECK_EQ(std::accumulate(a.begin(), a.end(), 0), a.size());

  for (int i = 0; i < a.Nx(); i++) {
    for (int j = 0; j < a.Ny(); j++) {
      for (int k = 0; k < a.Nz(); k++) {
        for (int m = 0; m < a.Nt(); m++) {
          a(i, j, k, m) = k * 1e5 + j * 1e4 + i * 1e2 + m;
        }
      }
    }
  }

  num *p = a.data();
  for (int i = 0; i < 9; i++) {
    FAST_CHECK_EQ(p[i * (7 * 11 * 13)], static_cast<num>(i));
    FAST_CHECK_EQ(p[i * (7 * 11 * 13) + 1], static_cast<num>(i + 100));
    FAST_CHECK_EQ(p[i * (7 * 11 * 13) + 3 + 5 * 7], static_cast<num>(i + 300 + 5e4));
  }

  num *pt = a[5];
  FAST_CHECK_EQ(pt[0], 5);
  FAST_CHECK_EQ(pt[1], 100 + 5);
  FAST_CHECK_EQ(pt[3 + 5 * 7], 50000 + 300 + 5);
  pt[3 + 5 * 7] = 777;
  FAST_CHECK_EQ(p[5 * (7 * 11 * 13) + 3 + 5 * 7], 777);
  FAST_CHECK_EQ(a(3, 5, 0, 5), 777);
  Array3<num> r = a[5];
  FAST_CHECK_EQ(r(3, 5, 0), 777);
  r(3, 5, 0) = 50000 + 300 + 5;
  FAST_CHECK_EQ(pt[3 + 5 * 7], 50000 + 300 + 5);
  FAST_CHECK_EQ(a(3, 5, 0, 5), 50000 + 300 + 5);

  // test moving timeframe
  a(IdxVec(0, 0, 0), 9);
  FAST_CHECK_EQ(a.get_t_head(), 1);
  a(IdxVec(0, 0, 0), 10);
  FAST_CHECK_EQ(a.get_t_end(), 10);
  FAST_CHECK_EQ(a(3, 5, 0, 2), 50000 + 300 + 2);
  FAST_CHECK_EQ(a(3, 5, 0, 7), 50000 + 300 + 7);
  FAST_CHECK_EQ(a(3, 5, 0, 9), 1);

  for (int t = 11; t < 3 * a.Nt(); t++) {
    a(IdxVec(0, 0, 0), t);
    FAST_CHECK_EQ(a.get_t_end(), t);
  }

  a.set_t_end(7 * a.Nt());
  FAST_CHECK_EQ(a.get_t_end(), 7 * a.Nt());

  a(IdxVec(0, 0, 0), 71 * a.Nt() - 1);
  FAST_CHECK_EQ(a.get_t_end(), 71 * a.Nt() - 1);
  FAST_CHECK_EQ(std::accumulate(a.begin(), a.end(), 0), a.size());

  for (auto &x : a) {
    x = 2;
  }

  sum = 0;
  for (int i = 0; i < a.Nx(); i++) {
    for (int j = 0; j < a.Ny(); j++) {
      for (int k = 0; k < a.Nz(); k++) {
        for (int m = 70 * a.Nt(); m < 71 * a.Nt(); m++) {
          sum += a(IdxVec(i, j, k), m);
        }
      }
    }
  }
  FAST_CHECK_EQ(sum, 2 * a.size());
  FAST_CHECK_EQ(std::accumulate(a.begin(), a.end(), 0), 2 * a.size());

  sum = 0;
  for (int m = 70 * a.Nt(); m < 71 * a.Nt(); m++) {
    for (int i = 0; i < a.Nx(); i++) {
      for (int j = 0; j < a.Ny(); j++) {
        for (int k = 0; k < a.Nz(); k++) {
          a(i, j, k, m) = i * 1e4 + j * 1e3 + k * 1e2 + m % a.Nt();
          sum += a(i, j, k, m);
        }
      }
    }
  }
  FAST_CHECK_EQ(*(a.begin()), a(0, 0, 0, 70 * a.Nt()));
  FAST_CHECK_EQ(*(a.end() - 1), a(a.Nx() - 1, a.Ny() - 1, a.Nz() - 1, 71 * a.Nt() - 1));
  FAST_CHECK_EQ(a(IdxVec(3, 8, 4), 70 * 9 + 2), 3 * 1e4 + 8 * 1e3 + 4 * 1e2 + 2);

  // test timetrace
  auto pos = IdxVec(3, 8, 4);

  auto timetrace        = a.timeTrace(pos, 71 * 9 - 1);
  auto timetrace2       = a.timeTrace(pos);
  auto timetrace_it     = a.begin(pos);
  auto timetrace_it_end = a.end(pos);

  FAST_REQUIRE_EQ(timetrace.size(), 9);
  FAST_CHECK_EQ(timetrace_it_end - timetrace_it, 9);
  for (int i = 0; i < timetrace.size(); i++) {
    FAST_CHECK_EQ(timetrace[i], a(pos, 70 * 9 + i));
    FAST_CHECK_EQ(timetrace2[i], a(pos, 70 * 9 + i));
    FAST_CHECK_EQ(*timetrace_it, a(pos, 70 * 9 + i));
    timetrace_it++;
  }
  FAST_CHECK_EQ(timetrace_it, timetrace_it_end);

  timetrace[2] = 100;
  a.loadTimeTrace(pos, 71 * 9 - 1, timetrace);

  FAST_CHECK_EQ(a(IdxVec(3, 8, 4), 70 * 9), 3 * 1e4 + 8 * 1e3 + 4 * 1e2);
  FAST_CHECK_EQ(a(IdxVec(3, 8, 4), 70 * 9 + 2), 100);
  FAST_CHECK_EQ(a(IdxVec(3, 8, 4), 70 * 9 + 8), 3 * 1e4 + 8 * 1e3 + 4 * 1e2 + 8);

  timetrace_it    = a.begin(pos);
  timetrace_it[2] = 3 * 1e4 + 8 * 1e3 + 4 * 1e2 + 2;
  FAST_CHECK_EQ(a(IdxVec(3, 8, 4), 70 * 9 + 2), 3 * 1e4 + 8 * 1e3 + 4 * 1e2 + 2);

  // test iterator compliance with STL algorithms
  std::generate(a.begin(), a.end(), [n = 0]() mutable { return n++; });
  CircularArray<num> tmp(7, 11, 13, 9);
  tmp = a;
  std::sort(tmp.begin(), tmp.end());
  FAST_CHECK_UNARY(tmp == a);

  CircularArray<num> tmp2(std::move(tmp));
  bool isequal = true;
  for (int m = 70 * a.Nt(); m < 71 * a.Nt(); m++) {
    for (int i = 0; i < a.Nx(); i++) {
      for (int j = 0; j < a.Ny(); j++) {
        for (int k = 0; k < a.Nz(); k++) {
          const num t = (m - 70 * 9) * 7 * 11 * 13 + i + j * 7 + k * 7 * 11;
          isequal &= (tmp2(i, j, k, m) == t);
        }
      }
    }
  }
  FAST_CHECK_UNARY(isequal);

  // test pos iterators
  std::vector<IdxVec> vpos;
  vpos.reserve(7 * 11 * 13);
  for (auto &pos : a.positions) {
    vpos.push_back(pos);
  }
  FAST_CHECK_EQ(vpos.size(), 7 * 11 * 13);

  std::vector<IdxVec> vpos2;
  vpos2.reserve(7 * 11 * 13);
  for (int z = 0; z < a.Nz(); z++) {
    for (int y = 0; y < a.Ny(); y++) {
      for (int x = 0; x < a.Nx(); x++) {
        vpos2.emplace_back(IdxVec(x, y, z));
      }
    }
  }
  FAST_CHECK_EQ(vpos.size(), vpos2.size());
  FAST_CHECK_EQ(vpos, vpos2);

  // test resize
  a.resize(17);
  FAST_CHECK_EQ(a.Nx(), 7);
  FAST_CHECK_EQ(a.Ny(), 11);
  FAST_CHECK_EQ(a.Nz(), 13);
  FAST_CHECK_EQ(a.Nt(), 17);
  FAST_CHECK_EQ(a.get_t_head(), 0);
  FAST_CHECK_EQ(a.get_t_end(), 16);
  FAST_CHECK_EQ(a.dimensions(), Vector4<int>(7, 11, 13, 17));
  FAST_CHECK_EQ(a.size(), 7 * 11 * 13 * 17);
  FAST_CHECK_EQ(std::accumulate(a.begin(), a.end(), 0), a.size());
}