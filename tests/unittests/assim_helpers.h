#pragma once
#include "da_sim.h"

TEST_SUITE("da helper functions") {
  TEST_CASE("setup") {
    prm.da_delay = 70;
    prm.tw       = 10;
    prm.tw_da    = 40;
    prm.Nt       = 5000;
  }

  TEST_CASE("da SimTime DataTime basics") {
    FAST_CHECK_EQ(prm.da_delay, 70);

    SimTime ts{25};
    FAST_CHECK_EQ(ts.t(), 25);

    DataTime td{ts};
    FAST_CHECK_EQ(ts.t() + prm.da_delay, td.t());
    FAST_CHECK_EQ(ts, static_cast<SimTime>(td));
    FAST_CHECK_EQ(static_cast<DataTime>(ts), td);

    SimTime ts2{td};
    FAST_CHECK_EQ(ts, ts2);
    FAST_CHECK_EQ(ts.t(), ts2.t());

    DataTime td2 = ts2;
    FAST_CHECK_EQ(td, td2);
    FAST_CHECK_EQ(td.t(), td2.t());
  }

  TEST_CASE("da SimTime DataTime io calculations") {
    FAST_CHECK_EQ(prm.da_delay, 70);
    FAST_CHECK_EQ(prm.tw_da, 40);

    for (int i = 3; i < 8; i++) {
      SimTime ts1{i * prm.tw_da};
      DataTime td1 = ts1;
      SimTime ts2{i * prm.tw_da - prm.da_delay};
      DataTime td2(ts2);

      FAST_CHECK_EQ(ts1 - ts2, prm.da_delay);

      FAST_CHECK_UNARY_FALSE(td1.available());
      FAST_CHECK_UNARY(td2.available());

      FAST_CHECK_EQ(td1.t() + prm.tw_da, td1.next().t());
      FAST_CHECK_EQ(td2.t() + prm.tw_da, td2.next().t());
      FAST_CHECK_UNARY_FALSE(td1.next().available());
      FAST_CHECK_UNARY(td2.next().available());

      DataTime nearest1 = td1.nearestData();
      DataTime nearest2 = td2.nearestData();
      DataTime nearest3 = DataTime(SimTime(ts1.t() + 20)).nearestData();

      FAST_CHECK_EQ(nearest2.t(), td2.t());
      FAST_CHECK_EQ(nearest1, td2.next().next());
      FAST_CHECK_EQ(nearest3, td2.next().next());
    }
  }
}