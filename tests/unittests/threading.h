#pragma once

#include "helper/threading.h"

extern Parameters prm;

TEST_CASE("threading") {
  prm.turn_off_logging();

  SUBCASE("get_thread_count") {
    auto cores = get_thread_count();

    FAST_CHECK_UNARY(((cores > 0) && (cores <= 64)));
  }

  SUBCASE("make_chunks") {
    using std::distance;

    std::vector<int> v{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};


    SUBCASE("even chunk") {
      ChunkVec<int> c = make_chunks(v, 2);
      CHECK(c.size() == 2);
      CHECK(distance(c[0].first, c[0].second) == distance(c[1].first, c[1].second));
      CHECK(distance(v.begin(), c[0].first) == 0);
      CHECK(distance(v.begin(), c[0].second) == 5);
      CHECK(distance(v.begin(), c[1].first) == 5);
      CHECK(distance(v.end(), c[1].second) == 0);

      int sum = 0;
      for (auto &chunck : c) {
        for (auto it = chunck.first; it < chunck.second; it++) {
          sum += *it;
        }
      }
      CHECK(sum == std::accumulate(v.begin(), v.end(), 0));
    }


    SUBCASE("uneven chunk") {
      ChunkVec<int> c = make_chunks(v, 3);
      CHECK(c.size() == 3);
      CHECK(distance(v.begin(), c[0].first) == 0);
      CHECK(distance(c[0].first, c[0].second) == distance(c[1].first, c[1].second));
      CHECK(distance(c[2].second, v.end()) == 0);
      int sum = 0;
      for (auto &chunck : c) {
        for (auto it = chunck.first; it < chunck.second; it++) {
          sum += *it;
        }
      }
      CHECK(sum == std::accumulate(v.begin(), v.end(), 0));
    }


    SUBCASE("chunk size larger than input size") {
      ChunkVec<int> c = make_chunks(v, 15);
      CHECK(c.size() == 15);
      CHECK(distance(v.begin(), c[0].first) == 0);
      CHECK(distance(c[10].first, c[10].second) == 0);
      long pt = 0;
      for (int i = 0; i < 15; i++) {
        pt += std::abs(distance(c[i].first, c[i].second));
      }
      CHECK(pt == 10);
      int sum = 0;
      for (auto &chunck : c) {
        for (auto it = chunck.first; it < chunck.second; it++) {
          sum += *it;
        }
      }
      CHECK(sum == std::accumulate(v.begin(), v.end(), 0));
    }
  }
}