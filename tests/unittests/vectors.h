#pragma once

#include <unordered_set>

#include "helper/vectors.h"
#include "filters/noiseadder.h"


TEST_CASE("vectors") {

  SUBCASE("Vec3") {
    Vec3 a{1, 2, 3};
    Vec3 b{3, 2, 1};
    Vec3 ab(4, 4, 4);
    Vec3 amb = Vec3(-2, 0, 2);
    Vec3 a2{2, 4, 6};

    FAST_CHECK_EQ(a[1], 2);
    FAST_CHECK_EQ(a.length_squared(), 1 + 2 * 2 + 3 * 3);
    FAST_CHECK_EQ(b.length_squared(), 1 + 2 * 2 + 3 * 3);
    FAST_CHECK_EQ(a + b, ab);
    FAST_CHECK_EQ(a.to_string(), "(1, 2, 3)");
    FAST_CHECK_EQ(a - b, amb);

    auto a_o = a;
    a -= b;
    FAST_CHECK_EQ(a, amb);
    a += b;
    FAST_CHECK_EQ(a, a_o);

    FAST_CHECK_EQ(a * 2, a2);
    a = a * 2;
    FAST_CHECK_EQ(a, a2);
    a = a / 2;
    FAST_CHECK_EQ(a, a_o);

    Vec3 c(std::move(ab));
    FAST_CHECK_EQ(c, a + b);

    a = static_cast<Vec3>(Vector3<int>(5, 5, 5));
    FAST_CHECK_EQ(a, Vec3(5, 5, 5));

    /* clang-format off */
    SUBCASE("Rotation") {

      a = {3, 7, 5};

      using std::cos;
      using std::sin;
      NoiseAdder noiseAdder(M_PI);

      // get a new random angle for each check
      num theta = noiseAdder.random_number();

      // Rotated around x axis
      Vec3 rotated = Vec3(a[0],
                          a[1] * cos(theta) - a[2] * sin(theta),
                          a[1] * sin(theta) + a[2] * cos(theta));
      FAST_CHECK_EQ(a.rotate({1, 0, 0}, theta).to_string(), rotated.to_string());


      // Rotated around y axis
      theta = noiseAdder.random_number();
      rotated = Vec3(a[0] * cos(theta) + a[2] * sin(theta),
                     a[1],
                     -a[0] * sin(theta) + a[2] * cos(theta));
      FAST_CHECK_EQ(a.rotate({0, 1, 0}, theta).to_string(), rotated.to_string());


      // Rotated around z axis
      theta = noiseAdder.random_number();
      rotated = Vec3(a[0] * cos(theta) - a[1] * sin(theta),
                     a[0] * sin(theta) + a[1] * cos(theta),
                     a[2]);
      FAST_CHECK_EQ(a.rotate({0, 0, 1}, theta).to_string(), rotated.to_string());
    }
    /* clang-format on */
  }

  SUBCASE("Vec2") {
    Vec2 a{1, 2};
    Vec2 b{2, 1};
    Vec2 ab(3, 3);
    Vec2 amb = Vec2(-1, 1);
    Vec2 a2{2, 4};

    FAST_CHECK_EQ(a[1], 2);
    FAST_CHECK_EQ(a.length_squared(), 1 + 2 * 2);
    FAST_CHECK_EQ(b.length_squared(), 1 + 2 * 2);
    FAST_CHECK_EQ(a + b, ab);
    FAST_CHECK_EQ(a.to_string(), "(1, 2)");
    FAST_CHECK_EQ(a - b, amb);

    auto a_o = a;
    a -= b;
    FAST_CHECK_EQ(a, amb);
    a += b;
    FAST_CHECK_EQ(a, a_o);

    FAST_CHECK_EQ(a * 2, a2);
    a = a * 2;
    FAST_CHECK_EQ(a, a2);
    a = a / 2;
    FAST_CHECK_EQ(a, a_o);

    Vec2 c(std::move(ab));
    FAST_CHECK_EQ(c, a + b);

    a = static_cast<Vec2>(Vector2<int>(5, 5));
    FAST_CHECK_EQ(a, Vec2(5, 5));
  }

  SUBCASE("Vec4") {
    Vec4 a{1, 2, 3, 4};
    Vec4 b{4, 3, 2, 1};
    Vec4 ab(5, 5, 5, 5);
    Vec4 amb = Vec4(-3, -1, 1, 3);
    Vec4 a2{2, 4, 6, 8};

    FAST_CHECK_EQ(a[1], 2);
    FAST_CHECK_EQ(a.length_squared(), 1 + 2 * 2 + 3 * 3 + 4 * 4);
    FAST_CHECK_EQ(b.length_squared(), 1 + 2 * 2 + 3 * 3 + 4 * 4);
    FAST_CHECK_EQ(a + b, ab);
    FAST_CHECK_EQ(a.to_string(), "(1, 2, 3, 4)");
    FAST_CHECK_EQ(a - b, amb);

    auto a_o = a;
    a -= b;
    FAST_CHECK_EQ(a, amb);
    a += b;
    FAST_CHECK_EQ(a, a_o);

    FAST_CHECK_EQ(a * 2, a2);
    a = a * 2;
    FAST_CHECK_EQ(a, a2);
    a = a / 2;
    FAST_CHECK_EQ(a, a_o);

    Vec4 c(std::move(ab));
    FAST_CHECK_EQ(c, a + b);

    a = static_cast<Vec4>(Vector4<int>(5, 5, 5, 5));
    FAST_CHECK_EQ(a, Vec4(5, 5, 5, 5));
  }

  SUBCASE("IdxVec Hashing") {
    std::unordered_set<IdxVec> s;

    IdxVec dims(150, 150, 150);

    const auto begin = IdxVecIterator::begin(dims);
    const auto end   = IdxVecIterator::end(dims);

    size_t count = 0;
    for (auto it = begin; it != end; it++) {
      count += s.count(*it);
      s.insert(*it);
    }
    FAST_CHECK_EQ(count, 0);

    count = 0;
    for (auto it = begin; it != end; it++) {
      count += s.count(*it);
    }
    FAST_CHECK_EQ(count, dims.element_product());
  }
}