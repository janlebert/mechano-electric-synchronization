#include "myocardium.h"
#include "filters/noiseadder.h"
#include "helper.h"

namespace {
  bool isOrthogonal(const Vec3 &a, const Vec3 &b) {
    auto r = a.dot(b);
    return (abs(r) < 1e-4);
  }
}  // namespace

Myocardium::Myocardium(bool write_output) : myocytes(prm.Lx * prm.Ly * prm.Lz) {

#if DEBUG
  log_msg("Starting mass-spring simulation …");
#endif

  // Remove old files
  if (write_output) {
    /* clang-format off */
    io::deleteFiles({filename_u,
                     filename_v,
                     filename_T,
                     filename_pos,
                     filename_fibers});
    /* clang-format on */
  }


  constructMyocytes();


  // connect myocytes through gapjunctions (diffusion)
  constructGapjunctions();


  // set up fiber/sheet/sheetnormal orientations
  orientateFibers(write_output);


  // creating cells/hexaedra from each myocyte and its 7 next myocytes, create hexasprings on the fly
  constructHexaedras();


  // construct home spring:
  // hold the outermost myocytes in place by connecting them with
  // a spring to their reference position
  constructHomesprings();


  // create and start the threadpool
  unsigned thread_count = get_thread_count();
  log_msg("using {} threads for myocardium timesteps", thread_count);

  boundary   = make_unique<BoundaryConditions>(this, prm.noflux, thread_count);
  threadpool = make_unique<Myocardium::ThreadPool>(thread_count, myocytes, gapjunctions, hexaedras,
                                                   homesprings, *boundary);
}

void Myocardium::constructMyocytes() {
  const Vec3 center = static_cast<Vec3>(prm.dims_padded) / 2.0;

  for (int x = 0; x < prm.Lx; x++) {  // creating myocytes in a grid
    for (int y = 0; y < prm.Ly; y++) {
      for (int z = 0; z < prm.Lz; z++) {

        Vec3 pos = prm.spacing * (Vec3(x, y, z) - center);

        getMyocyteWithPadding(x, y, z)->resetPos(pos);
        getMyocyteWithPadding(x, y, z)->writea(prm.a);
        getMyocyteWithPadding(x, y, z)->writeb(prm.b);

        if (!idx_inside_system({x, y, z})) {
          getMyocyteWithPadding(x, y, z)->makeUnexcitable();
        }
      }
    }
  }

#if DEBUG
  log_msg("Initialized {} myocytes", myocytes.size());
#endif
}

void Myocardium::constructGapjunctions() {
  // connecting myocytes through Gapjunctions, 27-Point Laplacian inside medium

  auto makeGapjunctions = [this](const std::array<Myocyte *, 27> &m) {
    gapjunctions.emplace_back(m);
  };

  for (int x = 1; x < prm.Lx - 1; x++) {
    for (int y = 1; y < prm.Ly - 1; y++) {
      for (int z = 1; z < prm.Lz - 1; z++) {
        makeGapjunctions(
            {{getMyocyteWithPadding(x, y, z),  //m0

              getMyocyteWithPadding(x + 1, y, z),  //m1  factor 1
              getMyocyteWithPadding(x - 1, y, z),
              getMyocyteWithPadding(x, y + 1, z),
              getMyocyteWithPadding(x, y - 1, z),
              getMyocyteWithPadding(x, y, z + 1),
              getMyocyteWithPadding(x, y, z - 1),

              getMyocyteWithPadding(x, y - 1, z - 1),  // m7, in plane, factor sqrt(2)
              getMyocyteWithPadding(x, y - 1, z + 1),
              getMyocyteWithPadding(x, y + 1, z - 1),
              getMyocyteWithPadding(x, y + 1, z + 1),
              getMyocyteWithPadding(x - 1, y, z - 1),  // m11, in plane, factor sqrt(2)
              getMyocyteWithPadding(x - 1, y, z + 1),
              getMyocyteWithPadding(x + 1, y, z - 1),
              getMyocyteWithPadding(x + 1, y, z + 1),
              getMyocyteWithPadding(x - 1, y - 1, z),  // m15, in plane, factor sqrt(2)
              getMyocyteWithPadding(x + 1, y - 1, z),
              getMyocyteWithPadding(x - 1, y + 1, z),
              getMyocyteWithPadding(x + 1, y + 1, z),
              getMyocyteWithPadding(x - 1, y - 1, z - 1),  // m19, volume, factor sqrt(3)
              getMyocyteWithPadding(x - 1, y - 1, z + 1),
              getMyocyteWithPadding(x - 1, y + 1, z - 1),
              getMyocyteWithPadding(x - 1, y + 1, z + 1),
              getMyocyteWithPadding(x + 1, y - 1, z - 1),
              getMyocyteWithPadding(x + 1, y - 1, z + 1),
              getMyocyteWithPadding(x + 1, y + 1, z - 1),
              getMyocyteWithPadding(x + 1, y + 1, z + 1)}});
      }
    }
  }
}

void Myocardium::orientateFibers(bool write_output) {
  //writing initial fiber orientations of cardiomyocytes
  if (abs(prm.fiber_zmax_angle) < 1e-2) {  // no rotation in Z
    realignMyocytesLinear(prm.linear_fiber, prm.linear_hint, true);
  } else {  // rotate in Z
    realignMyocytesRotatedInZ(prm.linear_fiber, prm.linear_hint, prm.fiber_zmax_angle, true);
  }

  if (write_output) saveFiberOrientations();
}

void Myocardium::constructHexaedras() {
  // creating cells/hexaedra from each myocyte and its 7 next myocytes, create hexasprings on the fly

  auto makeHexaedra = [this](Myocyte *m0, Myocyte *m1, Myocyte *m2, Myocyte *m3, Myocyte *m4,
                             Myocyte *m5, Myocyte *m6, Myocyte *m7) {
    hexaedras.emplace_back(m0, m1, m2, m3, m4, m5, m6, m7);
  };

  for (int x = 0; x < prm.Lx - 1; x++) {
    for (int y = 0; y < prm.Ly - 1; y++) {
      for (int z = 0; z < prm.Lz - 1; z++) {
        /* Hexahedron
         *    7---6
         *   /|  /|
         *  3---2 |
         *  | 4-|-5
         *  |/  |/
         *  0---1
         */
        makeHexaedra(getMyocyteWithPadding(x, y, z),          // m0
                     getMyocyteWithPadding(x + 1, y, z),      // m1
                     getMyocyteWithPadding(x + 1, y + 1, z),  // m2
                     getMyocyteWithPadding(x, y + 1, z),      // m3

                     getMyocyteWithPadding(x, y, z + 1),          // m4
                     getMyocyteWithPadding(x + 1, y, z + 1),      // m5
                     getMyocyteWithPadding(x + 1, y + 1, z + 1),  // m6
                     getMyocyteWithPadding(x, y + 1, z + 1));     // m7
      }
    }
  }
}

void Myocardium::constructHomesprings() {
  // construct home forces
  std::unordered_set<IdxVec> homespring_set;

  for (int x = 0; x < prm.Lx; x++) {
    for (int z = 0; z < prm.Lz; z++) {
      IdxVec top(x, 0, z);
      IdxVec bottom(x, prm.Ly - 1, z);

      homespring_set.insert(top);
      homespring_set.insert(bottom);
    }
  }

  for (int y = 0; y < prm.Ly; y++) {
    for (int z = 0; z < prm.Lz; z++) {
      IdxVec top(0, y, z);
      IdxVec bottom(prm.Lx - 1, y, z);

      homespring_set.insert(top);
      homespring_set.insert(bottom);
    }
  }

  auto makeHomespring = [this](Myocyte *m) { homesprings.emplace_back(m); };

  for (auto &hs : homespring_set) {
    makeHomespring(getMyocyteWithPadding(hs));
  }
}

void Myocardium::timeStep() {
  threadpool->timeStep();
}

void Myocardium::ThreadPool::timeStepThread(Chunk<Myocyte> myocyte_c,
                                            Chunk<Diffusion> gapjunction_c,
                                            Chunk<Hexaedra> hexaedra_c,
                                            Chunk<Homespring> homespring_c,
                                            BoundaryConditions *boundary,
                                            LightweightSemaphore *run_semaphore,
                                            AutoResetEvent *timestep_finished,
                                            std::shared_ptr<ThreadBarrier> barrier,
                                            std::atomic<bool> &exit,
                                            int id) {

  while (true) {
    run_semaphore->wait();  // wait until main thread calls run_semaphore.signal()

    if (exit) return;

    if (prm.electrophysiology) {
      for (int i = 0; i < prm.ite; i++) {
        for (auto it = myocyte_c.first; it < myocyte_c.second; ++it)
          it->electrophysiologyTimeStep();  // calculate u,v of each particle at the next time step.
      }

      barrier->wait();  // synchronize threads

      // Boundary condition
      boundary->apply(id);

      barrier->wait();

      for (auto it = gapjunction_c.first; it < gapjunction_c.second; ++it) {
        it->applyDiffusion();
      }
    }
    // ELECTROPHYSIOLOGY AT TOP

    // MECHANICS FROM HERE

    if (prm.mechanics) {
      for (int i = 0; i < prm.itm; i++) {
        barrier->wait();

        for (auto it = hexaedra_c.first; it < hexaedra_c.second; ++it) {
          it->updateHexaSprings();
        }

        barrier->wait();

        for (auto it = homespring_c.first; it < homespring_c.second; ++it) {
          it->updateHomesprings();
        }

        barrier->wait();

        for (auto it = myocyte_c.first; it < myocyte_c.second; ++it) {
          // update positions of myocytes according to forces which act on them
          it->mechanicsTimeStep();
        }
      }
    }
    barrier->wait();
    if (id == 0) {                  // prevent thread race
      timestep_finished->signal();  // notify main thread that timestep was completed
    }
  }
}

void Myocardium::writeBinaryFileMechanics() {
  OutStream out_pos{filename_pos};

  for (auto &pos : prm.positions) {
    Vec3 position = getMyocyte(pos)->getPos();
    out_pos.write(position);
  }
}

void Myocardium::writeBinaryFileElectro() {
  OutStream out_u{filename_u};
#ifdef WRITEOUT_EVERYTHING
  OutStream out_v{filename_v};
  OutStream out_T{filename_T};
#endif

  for (auto &pos : prm.positions) {
    num u = getMyocyte(pos)->getu();
    out_u.write(u);

#ifdef WRITEOUT_EVERYTHING
    num v = getMyocyte(pos)->getv();
    out_v.write(v);

    num T = getMyocyte(pos)->getT();
    out_T.write(T);
#endif
  }
}

void Myocardium::resetMedium() {
  log_msg("Resetting Medium (u, v, T, and positions)");

  for (auto &pos : prm.positions) {
    getMyocyte(pos)->writeu(0);
    getMyocyte(pos)->writev(0);
    getMyocyte(pos)->writeT(0);
    Vec3 resetposition = getMyocyte(pos)->getRefPos();
    getMyocyte(pos)->makeMovable();
    getMyocyte(pos)->resetPos(resetposition);
  }
}

void Myocardium::set_uv(num u, num v) {
  for (auto &myocyte : myocytes) {
    myocyte.writeu(u);
    myocyte.writev(v);
  }
}

void Myocardium::load_pos(string filename, long t) {
  log_msg("overwriting myocyte positions with those from file {} at t={}", filename, t);

  InStream<Vec3> in{filename, prm.dims, prm.tw};

  for (auto &pos : prm.positions) {
    getMyocyte(pos)->resetPos(in.read(pos, t));
  }
}

void Myocardium::load_u(string filename, long t) {
  log_msg("overwriting myocyte u values with those from file {} at t={}", filename, t);

  InStream<num> in{filename, prm.dims, prm.tw};

  for (auto &pos : prm.positions) {
    getMyocyte(pos)->writeu(in.read(pos, t));
  }
}

void Myocardium::load_v(string filename, long t) {
  log_msg("overwriting myocyte v values with those from file {} at t={}", filename, t);

  InStream<num> in{filename, prm.dims, prm.tw};

  for (auto &pos : prm.positions) {
    getMyocyte(pos)->writev(in.read(pos, t));
  }
}

void Myocardium::load_T(string filename, long t) {
  log_msg("overwriting myocyte T values with those from file {} at t={}", filename, t);

  InStream<num> in{filename, prm.dims, prm.tw};

  for (auto &pos : prm.positions) {
    getMyocyte(pos)->writeT(in.read(pos, t));
  }
}

void Myocardium::load_uvT(string filename_prefix, long t) {
  string u_fn = filename_prefix + "_u.dat";
  string v_fn = filename_prefix + "_v.dat";
  string T_fn = filename_prefix + "_T.dat";

  load_u(u_fn);
  load_v(v_fn);
  if (io::fileExists(T_fn)) load_T(T_fn);
}

void Myocardium::snapshot_uv(string filename_prefix) {
  snapshot_uvT(filename_prefix, false);
}

void Myocardium::snapshot_uvT(string filename_prefix, bool snapshot_T) {
  string u_fn = filename_prefix + "_u.dat";
  string v_fn = filename_prefix + "_v.dat";
  string T_fn = filename_prefix + "_T.dat";

  if (snapshot_T) {
    log_msg("Creating uvT snapshot: [{}, {}, {}]", u_fn, v_fn, T_fn);
  } else {
    T_fn = "";
    log_msg("Creating uv snapshot: [{}, {}]", u_fn, v_fn);
  }

  OutStream out_u{u_fn, true};
  OutStream out_v{v_fn, true};
  OutStream out_T{T_fn, true};

  for (auto &pos : prm.positions) {
    num u = getMyocyte(pos)->getu();
    out_u.write(u);

    num v = getMyocyte(pos)->getv();
    out_v.write(v);

    if (snapshot_T) {
      num T = getMyocyte(pos)->getT();
      out_T.write(T);
    }
  }
}

void Myocardium::saveFiberOrientations(bool initialOrientation) {
  OutStream out{filename_fibers};

  for (auto &pos : prm.positions) {
    array<Vec3, 3> orientations{};
    /* clang-format off */
    if (initialOrientation) {
      orientations = {{getMyocyte(pos)->getInitialFiber(),
                       getMyocyte(pos)->getInitialSheet(),
                       getMyocyte(pos)->getInitialSheetNormal()}};
    } else {
      orientations = {{getMyocyte(pos)->getFiber(),
                       getMyocyte(pos)->getSheet(),
                       getMyocyte(pos)->getSheetNormal()}};
    }
    /* clang-format on */

    out.write(orientations);
  }
}

void Myocardium::realignMyocytesLinear(Vec3 fiber, Vec3 hint, const bool initial) {

  fiber = fiber.normalized();
  hint  = hint.normalized();

  // sheet should be perpendicular to fiber
  Vec3 sheet = hint.cross(fiber).normalized();
  CHECK_ALWAYS(isOrthogonal(fiber, sheet),
               "fiber/sheet directions are not orthogonal! fiber {} sheet {}", fiber, sheet);

  // sheetnormal should be perpendicular to both fiber and sheet
  Vec3 sheetnormal = fiber.cross(sheet).normalized();

  log_msg("aligning myocytes linear");
  log_msg("fiber = {}, sheet = {}, sheetnormal = {}", fiber, sheet, sheetnormal);

  for (auto &pos : prm.positions_padded) {
    if (initial) {
      getMyocyteWithPadding(pos)->writeInitialFiberOrientation(fiber, sheet, sheetnormal);
    }
    getMyocyteWithPadding(pos)->writeFiberOrientation(fiber, sheet, sheetnormal);
  }

  if (!initial) {
    for (auto &hexaedra : hexaedras) {
      hexaedra.redetermineIntersectionPoints();  // find 6 intersection points in each hexaedra
    }
  }
}


void Myocardium::realignMyocytesRotatedInZ(Vec3 fiber, Vec3 hint, num zmax_angle, bool initial) {
  const num z0_angle = 0;
  zmax_angle *= M_PI / 180.;  // deg to rad

  fiber = fiber.normalized();
  hint  = hint.normalized();

  log_msg("rotating fiber direction dependent on z");

  const int z_pad = prm.padding_dims[2];
  for (int z = 0; z < prm.Lz; z++) {
    num theta = 0;

    if (z < z_pad) {
      theta = z0_angle;
    } else if (z < prm.Nz + z_pad) {
      theta = z0_angle + (z - z_pad) * (zmax_angle - z0_angle) / static_cast<num>(prm.Nz - 1);
    } else {
      theta = zmax_angle;
    }

    const Vec3 lfiber = fiber.rotate({0, 0, 1}, theta).normalized();
    const Vec3 lhint  = hint.rotate({0, 0, 1}, theta).normalized();

    // sheet should be perpendicular to fiber
    const Vec3 sheet = lhint.cross(lfiber).normalized();
    // sheetnormal should be perpendicular to both fiber and sheet
    const Vec3 sheetnormal = lfiber.cross(sheet).normalized();

    if (z < prm.Nz + z_pad) {
      log_msg("z = {}: angle {}, fiber = {}, sheet = {}, sheetnormal = {}", z - z_pad,
              theta * 180 / M_PI, lfiber, sheet, sheetnormal);
    }

    CHECK_ALWAYS(
        isOrthogonal(lfiber, sheet) && isOrthogonal(lfiber, sheetnormal),
        "fiber/sheet/sheetnormal directions are not orthogonal! fiber {}, sheet {}, sheetnormal {}",
        lfiber, sheet, sheetnormal);

    for (int x = 0; x < prm.Lx; x++) {
      for (int y = 0; y < prm.Ly; y++) {
        if (initial) {
          getMyocyteWithPadding(x, y, z)->writeInitialFiberOrientation(lfiber, sheet, sheetnormal);
        }
        getMyocyteWithPadding(x, y, z)->writeFiberOrientation(lfiber, sheet, sheetnormal);
      }
    }
  }


  // redetermine intersection points for hexaedras already created
  for (auto &hexaedra : hexaedras) {
    hexaedra.redetermineIntersectionPoints();  // find 6 intersection points in each hexaedra
  }
}

// axis: a vector describing the axis of rotation
// theta: the angle (in radians)
void Myocardium::rotateMyocytes(Vec3 axis, const num theta) {
  axis = axis.normalized();

  for (auto &pos : prm.positions_padded) {
    Vec3 fiber       = getMyocyteWithPadding(pos)->getFiber();
    Vec3 sheet       = getMyocyteWithPadding(pos)->getSheet();
    Vec3 sheetnormal = getMyocyteWithPadding(pos)->getSheetNormal();

    fiber       = fiber.rotate(axis, theta).normalized();
    sheet       = sheet.rotate(axis, theta).normalized();
    sheetnormal = sheetnormal.rotate(axis, theta).normalized();

    getMyocyteWithPadding(pos)->writeFiberOrientation(fiber, sheet, sheetnormal);
  }

  for (auto &hexaedra : hexaedras) {
    hexaedra.redetermineIntersectionPoints();  // find 6 intersection points in each hexaedra
  }

  log_msg("Rotated fibers around axis {} with θ = {}", axis, theta);
}

void Myocardium::realignMyocytesRandom() {
  NoiseAdder noiseAdder;

  for (auto &pos : prm.positions_padded) {
    // create fiber orientation in global coordinate system, completely random, isotropic in 3D space
    Vec3 fiber = noiseAdder.random_unit_vector();

    Vec3 sheet = fiber;  // will be perpendicular to fiber
    while (!isOrthogonal(fiber, sheet)) {
      // random vector to construct perpendicular vector to fiber
      Vec3 hint = noiseAdder.random_unit_vector();
      sheet     = fiber.cross(hint).normalized();
    }
    Vec3 sheetnormal = sheet.cross(fiber);  // this is perpendicular to both fiber and sheet
    sheetnormal      = sheetnormal.normalized();

    getMyocyteWithPadding(pos)->writeFiberOrientation(fiber, sheet, sheetnormal);
  }

  for (auto &hexaedra : hexaedras) {
    hexaedra.redetermineIntersectionPoints();  // find 6 intersection points in each hexaedra
  }
}
