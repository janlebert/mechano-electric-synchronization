#pragma once

#include "quadrangle.h"
#include "myocyte.h"

extern Parameters prm;

class Hexaedra {
 private:

  // Hexaspring Parameters

  Vec3 if1, iff1, if2, iff2, if3, iff3;  // initial intersection points

  Vec3 f1, ff1, f2, ff2, f3, ff3;  // intersection points

  // COMPUTED IN CONSTRUCTOR AT STARTUP
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  // 8 reference positions of myocytes belonging to hexaedra
  const Vec3 vertex0, vertex1, vertex2, vertex3, vertex4, vertex5, vertex6, vertex7;

  Vec3 initialbarycenter;  // barycenter in reference configuration
  Vec3 barycenter_t;

  std::array<Vector4<int>, 6> vertex_indices;
  std::array<Vector4<num>, 6> intpol_coefs;  // interpolation coefs

  num L01, L02, L03;       // rest lengths for 3 axial springs, computed once in constructor
  std::array<num, 8> LV0;  // rest lengths of volume springs, computed once in constructor

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  void redetermineIntersectionPoints(bool constructor);

 public:

  Myocyte *const m0, *const m1, *const m2, *const m3, *const m4, *const m5, *const m6, *const m7;

  /*
   *    7---6
   *   /|  /|
   *  3---2 |
   *  | 4-|-5
   *  |/  |/
   *  0---1
   *
   *  Front   Left  Back   Right
   *  3---2  2---6  6---7  7---3
   *  |   |  |   |  |   |  |   |
   *  |   |  |   |  |   |  |   |
   *  0---1  1---5  5---4  4---0
   *
   *  Bottom  Top
   *  0---1  7---6
   *  |   |  |   |
   *  |   |  |   |
   *  4---5  3---2
   *
   */
  Hexaedra(Myocyte *m0,
           Myocyte *m1,
           Myocyte *m2,
           Myocyte *m3,
           Myocyte *m4,
           Myocyte *m5,
           Myocyte *m6,
           Myocyte *m7);

  void updateHexaSprings();

  void redetermineIntersectionPoints();

  Vec3 getCurrentBarycenter() { return barycenter_t; }
};
