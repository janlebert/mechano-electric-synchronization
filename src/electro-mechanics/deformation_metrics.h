#pragma once

#include <set>

#include "num.h"
#include "vectors.h"

enum class RateType { File, Volumechange };

namespace DeformationMetrics {

  // Helper functions to convert enums to strings and back
  RateType Ratetype(string str);

  string Ratetype(RateType rt);

  // Get all enums as strings
  std::set<string> RateTypesStrings();

  vector<RateType> RateTypes();

  // Functions which can be used to quantify deformation

  num Volume(const array<Vec3, 8> &pos);

}  // namespace DeformationMetrics