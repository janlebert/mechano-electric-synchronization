#pragma once

#include "vectors.h"

class Quadrangle {
 public:
  const Vector4<int> p;
  const std::array<Vec3, 4> vtc;

  Quadrangle(Vector4<int> p, array<Vec3, 4> v) : p(p), vtc(v) {}
};