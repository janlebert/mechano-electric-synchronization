#include "hexaedra.h"
#include "deformation_metrics.h"

namespace {

  Vec3 calculateBarycenter(const array<Vec3, 8> &pos) {
    const auto normalization = static_cast<num>(1. / 8.);
    return (pos[0] + pos[1] + pos[2] + pos[3] + pos[4] + pos[5] + pos[6] + pos[7]) * normalization;
  }

  // Möller–Trumbore intersection algorithm
  // orig and dir defines the ray. v0, v1, v2 defines the triangle.
  // if intersection point is found, it will be stored in ip
  template <typename T>
  bool triangle_intersection(const Vector3<T> &orig,
                             const Vector3<T> &dir,
                             const Vector3<T> &v0,
                             const Vector3<T> &v1,
                             const Vector3<T> &v2,
                             Vector3<T> &ip) {
    Vector3<T> e1 = v1 - v0;
    Vector3<T> e2 = v2 - v0;
    // Calculate planes normal vector
    Vector3<T> pvec = dir.cross(e2);
    T det           = e1.dot(pvec);

    // Ray is parallel to plane
    if (det < 1e-8 && det > -1e-8) {
      return false;
    }

    T inv_det       = 1 / det;
    Vector3<T> tvec = orig - v0;
    T u             = tvec.dot(pvec) * inv_det;
    if (u < 0 || u > 1) {
      return false;
    }

    Vector3<T> qvec = tvec.cross(e1);
    T v             = dir.dot(qvec) * inv_det;
    if (v < 0 || u + v > 1) {
      return false;
    }

    ip = v0 * (1 - u - v) + v1 * u + v2 * v;
    return true;
  }
}  // namespace



Hexaedra::Hexaedra(Myocyte *m0,
                   Myocyte *m1,
                   Myocyte *m2,
                   Myocyte *m3,
                   Myocyte *m4,
                   Myocyte *m5,
                   Myocyte *m6,
                   Myocyte *m7)
    : vertex0(m0->getPos()),
      vertex1(m1->getPos()),
      vertex2(m2->getPos()),
      vertex3(m3->getPos()),
      vertex4(m4->getPos()),
      vertex5(m5->getPos()),
      vertex6(m6->getPos()),
      vertex7(m7->getPos()),
      m0(m0),
      m1(m1),
      m2(m2),
      m3(m3),
      m4(m4),
      m5(m5),
      m6(m6),
      m7(m7) {

  redetermineIntersectionPoints(true);

  // set intial rest lengths of volume preserving structural springs
  array<Vec3, 8> lv0 = {{vertex0 - initialbarycenter, vertex1 - initialbarycenter,
                         vertex2 - initialbarycenter, vertex3 - initialbarycenter,
                         vertex4 - initialbarycenter, vertex5 - initialbarycenter,
                         vertex6 - initialbarycenter, vertex7 - initialbarycenter}};

  for (int i = 0; i < 8; ++i) {
    LV0[i] = lv0[i].length();
  }

}  // end constructor










void Hexaedra::updateHexaSprings() {

  // load vertex positions
  const array<Vec3, 8> pos = {{m0->getPos(), m1->getPos(), m2->getPos(), m3->getPos(), m4->getPos(),
                               m5->getPos(), m6->getPos(), m7->getPos()}};

  // determine intersection points as functions of vertex indices and coefficients
  auto intersection_point = [this, &pos](const int i) -> Vec3 {
    return pos[vertex_indices[i][0]] * intpol_coefs[i][0] +
           pos[vertex_indices[i][1]] * intpol_coefs[i][1] +
           pos[vertex_indices[i][2]] * intpol_coefs[i][2] +
           pos[vertex_indices[i][3]] * intpol_coefs[i][3];
  };

  f1  = intersection_point(0);
  ff1 = intersection_point(1);
  f2  = intersection_point(2);
  ff2 = intersection_point(3);
  f3  = intersection_point(4);
  ff3 = intersection_point(5);


  m0->writeIntersectionPoints({{f1, ff1, f2, ff2, f3, ff3}});  // used for plotting !!!


  // intersecting springs

  const Vec3 ls1 = f1 - ff1;  // cross-diagonal vectors connecting intersectionpoints
  const Vec3 ls2 = f2 - ff2;
  const Vec3 ls3 = f3 - ff3;

  const num L1 = ls1.length();
  const num L2 = ls2.length();
  const num L3 = ls3.length();

  const auto safe_inverse = [](const num &x) -> num {
    if (x != 0) {
      return static_cast<num>(1) / x;
    } else {
      WARN_ERROR(
          "updateHexaSprings(): cross-diagonal vectors connecting intersectionpoint is 0-vector");
      return 1;
    }
  };

  const num Ls1_inv = safe_inverse(L1);
  const num Ls2_inv = safe_inverse(L2);
  const num Ls3_inv = safe_inverse(L3);



  // set up volume springs
  barycenter_t = calculateBarycenter(pos);
  m0->writeBarycenter(barycenter_t);  // sometimes used for plotting/debugging


  // volume preserving springs
  array<Vec3, 8> lv;
  for (int i = 0; i < 8; ++i) {
    lv[i] = pos[i] - barycenter_t;
  }



  // for angular springs, probably rest length
  const num cosang12 = ls1.dot(ls2) * Ls1_inv * Ls2_inv;
  const num cosang13 = ls1.dot(ls3) * Ls1_inv * Ls3_inv;
  const num cosang23 = ls2.dot(ls3) * Ls2_inv * Ls3_inv;

  const Vec3 normal12 = ls1.cross(ls2);
  const Vec3 n112     = ls1.cross(normal12).normalized();
  const Vec3 n212     = ls2.cross(normal12).normalized();
  const Vec3 normal13 = ls1.cross(ls3);
  const Vec3 n113     = ls1.cross(normal13).normalized();
  const Vec3 n313     = ls3.cross(normal13).normalized();
  const Vec3 normal23 = ls2.cross(ls3);
  const Vec3 n223     = ls2.cross(normal23).normalized();
  const Vec3 n323     = ls3.cross(normal23).normalized();

  const num K12 = (prm.ks4 * cosang12);  // angular forces
  const num K13 = (prm.ks5 * cosang13);
  const num K23 = (prm.ks6 * cosang23);

  const Vec3 nl1 = ls1.normalized();  // axial spring directions, normalized
  const Vec3 nl2 = ls2 * Ls2_inv;
  const Vec3 nl3 = ls3 * Ls3_inv;








  // axial forces
  const num T = m0->getT();

  const Vec3 F1 = -nl1 * prm.myofibcontr * (prm.ks1 * (L1 - L01 / (1 + prm.cs * T)));
  const Vec3 F2 = -nl2 * (prm.ks2 * (L2 - L02));
  const Vec3 F3 = -nl3 * (prm.ks3 * (L3 - L03));


  // volume preservation forces

  array<Vec3, 8> Fv;
  for (int i = 0; i < 8; ++i) {
    Fv[i] = -lv[i].normalized() * (prm.ksv * (lv[i].length() - LV0[i]));
  }

  // FOR TORSION/ROTATION OF SPRINGS WORK WITH ANGULAR FORCES

  // axial forces + angular forces


  const Vec3 Fa1 = n112 * K12 + n113 * K13;
  const Vec3 Fa2 = -n212 * K12 + n223 * K23;
  const Vec3 Fa3 = -n313 * K13 - n323 * K23;

  const Vec3 Ff1  = F1 + Fa1;
  const Vec3 Fff1 = -Ff1;
  const Vec3 Ff2  = F2 + Fa2;
  const Vec3 Fff2 = -Ff2;
  const Vec3 Ff3  = F3 + Fa3;
  const Vec3 Fff3 = -Ff3;







  // Accumulate forces (frc) along fiber, sheet and sheetnormal directions f1,ff1,f2,ff2,f3,ff3
  // collect and redistribute forces (frc) to vertices according to their indices


  array<Vec3, 8> frc = {};  // set to zero!

  auto accumulate_forces = [this, &frc](const Vec3 &f, const int i) -> void {
    frc[vertex_indices[i][0]] += f * intpol_coefs[i][0];
    frc[vertex_indices[i][1]] += f * intpol_coefs[i][1];
    frc[vertex_indices[i][2]] += f * intpol_coefs[i][2];
    frc[vertex_indices[i][3]] += f * intpol_coefs[i][3];
  };

  accumulate_forces(Ff1, 0);
  accumulate_forces(Fff1, 1);
  accumulate_forces(Ff2, 2);
  accumulate_forces(Fff2, 3);
  accumulate_forces(Ff3, 4);
  accumulate_forces(Fff3, 5);

  // volume preservation forces
  for (int i = 0; i < 8; ++i) {
    frc[i] += Fv[i];
  }

  // write forces/accelerations to myocytes/vertices of the hexaedra

  m0->addForce(frc[0]);
  m1->addForce(frc[1]);
  m2->addForce(frc[2]);
  m3->addForce(frc[3]);
  m4->addForce(frc[4]);
  m5->addForce(frc[5]);
  m6->addForce(frc[6]);
  m7->addForce(frc[7]);

}  // END UPDATE OF SPRINGS










void Hexaedra::redetermineIntersectionPoints() {
  redetermineIntersectionPoints(true);
}

void Hexaedra::redetermineIntersectionPoints(bool constructor) {
  std::vector<Vec3> intersections;
  intersections.reserve(6);

  // calculate Barycenter
  initialbarycenter = calculateBarycenter(
      {{vertex0, vertex1, vertex2, vertex3, vertex4, vertex5, vertex6, vertex7}});

  if (constructor) {
    barycenter_t = initialbarycenter;
    m0->writeBarycenter(barycenter_t);  // save as myocyte's Barycenter
  }

  Vec3 fiber       = m0->getFiber();
  Vec3 sheet       = m0->getSheet();
  Vec3 sheetnormal = m0->getSheetNormal();
  // orthogonal set of vectors from fiberorientation
  array<Vec3, 6> f123 = {{fiber, -fiber, sheet, -sheet, sheetnormal, -sheetnormal}};

  /* 6 Faces of the Hexaeder
   *
   *    7---6
   *   /|  /|
   *  3---2 |
   *  | 4-|-5
   *  |/  |/
   *  0---1
   *
   */
  array<Vec3, 4> q1_0123 = {{vertex0, vertex1, vertex2, vertex3}};  // 1  front  Face/Quadrangle
  array<Vec3, 4> q2_4567 = {{vertex4, vertex5, vertex6, vertex7}};  // 2  back   Face/Quadrangle
  array<Vec3, 4> q3_0473 = {{vertex0, vertex4, vertex7, vertex3}};  // 3  left
  array<Vec3, 4> q4_1562 = {{vertex1, vertex5, vertex6, vertex2}};  // 4  right
  array<Vec3, 4> q5_4510 = {{vertex4, vertex5, vertex1, vertex0}};  // 5  bottom
  array<Vec3, 4> q6_7623 = {{vertex7, vertex6, vertex2, vertex3}};  // 6  top

  array<Quadrangle, 6> quadrangles = {
      {Quadrangle({0, 1, 2, 3}, q1_0123), Quadrangle({4, 5, 6, 7}, q2_4567),
       Quadrangle({0, 4, 7, 3}, q3_0473), Quadrangle({1, 5, 6, 2}, q4_1562),
       Quadrangle({4, 5, 1, 0}, q5_4510), Quadrangle({7, 6, 2, 3}, q6_7623)}};

  //start triangle intersection here!!!
  for (int axis = 0; axis < 6; axis++) {  // go through 6 axis directions!

    auto nfound_before = intersections.size();

    array<num, 6> coefs = {{}};  // interpolation coefs

    const Vec3 f = f123[axis];  // here are the 3 orthogonal directions

    int loopcount = 0;
    while (intersections.size() == nfound_before) {
      for (auto &quadrangle : quadrangles) {  // go through Quadrangles, Faces

        Vec3 ip;  // intersection point

        // check for intersection
        if (triangle_intersection(initialbarycenter, f, quadrangle.vtc[0], quadrangle.vtc[1],
                                  quadrangle.vtc[2], ip) ||
            triangle_intersection(initialbarycenter, f, quadrangle.vtc[0], quadrangle.vtc[2],
                                  quadrangle.vtc[3], ip)) {

          intersections.push_back(ip);

          vertex_indices[axis] = quadrangle.p;

          Vec3 l1 = quadrangle.vtc[1] - quadrangle.vtc[0];
          Vec3 l3 = quadrangle.vtc[3] - quadrangle.vtc[0];
          Vec3 l  = ip - quadrangle.vtc[0];

          num L1 = l1.length();
          num L3 = l3.length();
          num L  = l.length();

          num cosang1 = l1.dot(l) / (L1 * L);
          num cosang3 = l3.dot(l) / (L3 * L);

          coefs[0] = (L1 - cosang1 * L) / L1;
          coefs[1] = (L3 - cosang3 * L) / L3;
          coefs[2] = 1 - coefs[0];
          coefs[3] = 1 - coefs[1];

          auto size = intersections.size();  // we expect 6 intersections

          if (size > 1 && intersections[size - 1] == intersections[size - 2]) {
            intersections.pop_back();
          } else {
            break;
          }
        }
      }

      if (loopcount > 100) {
        string msg =
            "[WARNING] stuck in loop in Hexaedra::redetermineIntersectionPoints()! "
            "Hexaedra m0 {}, {} intersections found so far! "
            "fiber {}, sheet {}, sheetnormal {}";
        log_msg(msg.c_str(), m0->getPos(), intersections.size(), fiber, sheet, sheetnormal);

        break;
      }
      loopcount++;
    }  // end while loop

    // Precompilation for updateHexasprings
    intpol_coefs[axis][0] = coefs[0] * coefs[1];
    intpol_coefs[axis][1] = coefs[2] * coefs[1];
    intpol_coefs[axis][2] = coefs[2] * coefs[3];
    intpol_coefs[axis][3] = coefs[0] * coefs[3];
  }

  ASSERT_ALWAYS(intersections.size() == 6, "unexpected intersection count");

  // write out ips
  Vec3 ip1 = intersections[0];
  Vec3 ip2 = intersections[1];
  Vec3 ip3 = intersections[2];
  Vec3 ip4 = intersections[3];
  Vec3 ip5 = intersections[4];
  Vec3 ip6 = intersections[5];

  if (constructor) {
    m0->writeIntersectionPoints({{ip1, ip2, ip3, ip4, ip5, ip6}});
  }

  if1  = ip1;
  iff1 = ip2;
  if2  = ip3;
  iff2 = ip4;
  if3  = ip5;
  iff3 = ip6;

  Vec3 wf1  = ip1 - initialbarycenter;
  Vec3 wff1 = ip2 - initialbarycenter;
  Vec3 wf2  = ip3 - initialbarycenter;
  Vec3 wff2 = ip4 - initialbarycenter;
  Vec3 wf3  = ip5 - initialbarycenter;
  Vec3 wff3 = ip6 - initialbarycenter;

  m0->writeFiberOrientation({{wf1, wff1, wf2, wff2, wf3, wff3}});
  if (constructor) {
    m0->writeInitialFiberOrientation({{wf1, wff1, wf2, wff2, wf3, wff3}});
  }

  // set initial rest lengths of springs
  Vec3 l01 = if1 - iff1;  // problem here???
  Vec3 l02 = if2 - iff2;
  Vec3 l03 = if3 - iff3;

  // IMPORTANT!!! needs to be written inside class/object each time of fiber realignment
  L01 = l01.length();
  L02 = l02.length();
  L03 = l03.length();
}
