#pragma once

#include <array>

#include "myocyte.h"

class Diffusion {
  const std::array<Myocyte *, 27> m;  // the myocyte m[0] spreads the excitation to myocytes mi's

  const std::array<num, 4> stencil;

  static std::array<num, 4> calc_stencil() {
    num h = prm.spacing;

    num a, b, c, d;
    // TODO: remove one of them?
    if (prm.reilly_laplace_stencil) {
      // R.C. Reilly, J. Beck. A Family of Large-Stencil Discrete Laplacian Approximations in Three Dimensions
      num factor = 3. / (13. * h * h);
      a          = (-44. / 3.) * factor;
      b          = factor;
      c          = factor / 2.;
      d          = factor / 3.;
    } else {
      // 27-Point O(h^6) stencil for the 3D Laplace operator
      // Spotz, W. F. High-Order Compact Finite Difference Schemes for Computational Mechanics. (The University of Texas at Austin, 1995).
      num factor = 1. / (30 * h * h);
      a          = -128 * factor;
      b          = 14 * factor;
      c          = 3 * factor;
      d          = 1 * factor;
    }

    return {{a, b, c, d}};
  };

 public:
  Diffusion(std::array<Myocyte *, 27> m) : m(m), stencil(calc_stencil()) {}

  void applyDiffusion() {
    m[0]->writeLaplacePrm(m[0]->getu(), stencil[0]);

    for (int i = 1; i <= 6; i++) {
      m[0]->writeLaplacePrm(m[i]->getu(), stencil[1]);  // distance 1
    }

    for (int i = 7; i <= 18; i++) {
      m[0]->writeLaplacePrm(m[i]->getu(), stencil[2]);  // distance sqrt(2)
    }

    for (int i = 19; i <= 26; i++) {
      m[0]->writeLaplacePrm(m[i]->getu(), stencil[3]);  // distance sqrt(3)
    }
  }
};