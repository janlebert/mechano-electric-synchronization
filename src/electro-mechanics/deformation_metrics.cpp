#include "deformation_metrics.h"

#include <map>
#include <algorithm>
#include "eig3/eig3.h"

namespace {
  /* clang-format off */
  static std::map<RateType, std::string> RateTypeMap = {
      {RateType::Volumechange, "volumechange"},
      {RateType::File, "file"}
  };
  /* clang-format on */

  template <typename T>
  auto find_key_from_value(std::map<T, string>& m, string s) {
    auto it = std::find_if(m.begin(), m.end(), [s](const auto& p) { return p.second == s; });

    ASSERT_ALWAYS(it != m.end(), "unknown enum {}", s);

    return (*it).first;
  }

  template <typename T>
  auto get_map_keys(std::map<T, string>& m) {
    vector<T> r;
    for (auto const& p : m) r.emplace_back(p.first);
    return r;
  }

  template <typename T>
  auto get_map_vals(std::map<T, string>& m) {
    std::set<string> r;
    for (auto const& p : m) r.insert(p.second);
    return r;
  }
}  // namespace

namespace DeformationMetrics {
  /* Implementations */

  RateType Ratetype(string str) { return find_key_from_value(RateTypeMap, str); }

  string Ratetype(RateType rt) { return RateTypeMap.at(rt); }

  std::set<string> RateTypesStrings() { return get_map_vals(RateTypeMap); }

  vector<RateType> RateTypes() { return get_map_keys(RateTypeMap); }

  namespace {
    /* helper functions */
    inline num triple_product(const Vec3& a, const Vec3& b, const Vec3& c) { return a.dot(b.cross(c)); }
  }  // namespace

  num Volume(const array<Vec3, 8>& pos) {
    // Calculates hexahedron volume from list of positions which define the hexahedron
    // Grandy J. Efficient Computation of Volume of Hexahedral Cells. Lawrence Livermore National Laboratory; 1997
    // https://www.osti.gov/scitech/biblio/632793/
    constexpr num normalization = static_cast<num>(1 / 12.);
    return (triple_product(pos[6] - pos[1] + pos[7] - pos[0], pos[6] - pos[3], pos[2] - pos[0]) +
            triple_product(pos[7] - pos[0], pos[6] - pos[3] + pos[5] - pos[0], pos[6] - pos[4]) +
            triple_product(pos[6] - pos[1], pos[5] - pos[0], pos[6] - pos[4] + pos[2] - pos[0])) *
           normalization;
  }

}  // namespace DeformationMetrics