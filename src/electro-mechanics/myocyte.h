#pragma once

#include <csignal>

#include "parameters.h"
#include "threading.h"

extern Parameters prm;

class Myocyte {
 private:
  NonRecursiveRWLock u_rwlock;

  num u          = 0;  // the excitability
  num v          = 0;  // the refractoriness
  num T          = 0;  // active stress
  num laplaceprm = 0;

  // other electrical parameters
  num aa = 0;  // local value for parameter a
  num bb = 0;  // local value for parameter b

  static constexpr num epsilon_T0 = 0.1;

  bool movable  = true;  // fix myocyte in space
  bool electro  = true;  // electrophysiology
  Vec3 pos      = {};    // the current position of the myocyte in 3D space
  Vec3 old_pos  = {};  // previous position, used as part of the verlet numerical integration scheme
  Vec3 velocity = {};

  Vec3 acceleration = {};  // current acceleration of the myocyte
  SpinLock acceleration_spinlock;

  Vec3 refpos = {};  // reference position

  Vec3 barycenter = {};

  array<Vec3, 6> fiberOrientation        = {};
  array<Vec3, 6> initialFiberOrientation = {};

  array<Vec3, 6> intersectionPoints = {};

  num volume = 0;

 public:
  Myocyte() = default;

  void makeExcitable() { electro = true; }
  void makeUnexcitable() { electro = false; }
  void makeUnmovable() { movable = false; }
  void makeMovable() { movable = true; }

  num getu() { return u; }
  num getu_threadsafe() {
    ReadLockGuard<NonRecursiveRWLock> lock(u_rwlock);
    return u;
  }
  num getv() { return v; }
  num geta() { return aa; }
  num getT() { return T; }

  // local model dynamics
  void electrophysiologyTimeStep() {
    if (!electro) return;

    u += prm.D * laplaceprm + (-prm.k * u * (u - aa) * (u - 1) - u * v) * prm.dt_e;
    const num epsilon = (prm.epsilon + prm.mu1 * v / (prm.mu2 + u));
    v += epsilon * (-v - prm.k * u * (u - bb - 1)) * prm.dt_e;

    const auto epsilon_T = [this]() -> num {
      if (u < static_cast<num>(0.05)) {
        return 10 * epsilon_T0;
      } else {  // u >= 0.05
        return epsilon_T0;
      }
    };

    const num _T = T * prm.initm;
    T            = _T + epsilon_T() * (prm.kT * u - _T) * prm.dt_e;


    laplaceprm = 0;
  }

  void writeu(num value) { u = value; }
  void writeu_threadsafe(num value) {
    WriteLockGuard<NonRecursiveRWLock> lock(u_rwlock);
    u = value;
  }
  void writev(num value) { v = value; }
  void writeT(num value) { T = value; }
  void writea(num value) { aa = value; }
  void writeb(num value) { bb = value; }

  void addForce(Vec3 f) {  // thread-safe function
    std::lock_guard<SpinLock> lock(acceleration_spinlock);
    acceleration += f;
  }

  void mechanicsTimeStep() {  // using Verlet integration
    if (!movable) return;

    Vec3 temp = pos;

    const num k = 0.001;
    pos         = pos + (pos - old_pos) * k + acceleration * prm.dt_m;

    old_pos = temp;

    acceleration = Vec3(0, 0, 0);  // reset forces for next time step
    velocity     = Vec3(0, 0, 0);
  }

  Vec3 getPos() { return pos; }
  Vec3 getRefPos() { return refpos; }
  Vec3 getVel() { return velocity; }
  Vec3 getBarycenterPos() { return barycenter; }
  Vec3 getIPsPos(int i) { return intersectionPoints.at(i); }

  Vec3 getFiber() { return fiberOrientation[0]; }
  Vec3 getSheet() { return fiberOrientation[2]; }
  Vec3 getSheetNormal() { return fiberOrientation[4]; }
  Vec3 getInitialFiber() { return initialFiberOrientation[0]; }
  Vec3 getInitialSheet() { return initialFiberOrientation[2]; }
  Vec3 getInitialSheetNormal() { return initialFiberOrientation[4]; }

  num getVolume() { return volume; }

  void resetPos(const Vec3 h) {
    pos     = h;
    old_pos = h;
    refpos  = h;
  }

  void movePos(const Vec3 h) {
    if (movable) pos += h;
  }

  void writeLaplacePrm(num m0u, num factor) { laplaceprm += factor * m0u; }

  void writeBarycenter(Vec3 value) { barycenter = value; }

  void writeIntersectionPoints(array<Vec3, 6> ip) { intersectionPoints = ip; };

  void writeInitialFiberOrientation(array<Vec3, 6> ifo) { initialFiberOrientation = ifo; };

  void writeInitialFiberOrientation(Vec3 fiber, Vec3 sheet, Vec3 sheetnormal) {
    writeInitialFiberOrientation({{fiber, -fiber, sheet, -sheet, sheetnormal, -sheetnormal}});
  }

  void writeFiberOrientation(array<Vec3, 6> fo) { fiberOrientation = fo; };

  void writeFiberOrientation(Vec3 fiber, Vec3 sheet, Vec3 sheetnormal) {
    writeFiberOrientation({{fiber, -fiber, sheet, -sheet, sheetnormal, -sheetnormal}});
  }

  void writeVolume(num value) { volume = value; };
};
