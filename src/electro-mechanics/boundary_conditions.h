#pragma once

#include "helper/threading.h"
#include "myocyte.h"

class Myocardium;

class BoundaryConditions {
 private:
  Myocardium *const t;

  const bool noflux;

  vector<Myocyte *> dirichlet_boundary;
  ChunkVec<Myocyte *> dirichlet_chunks;

  vector<std::pair<Myocyte *, Myocyte *>> noflux_boundary;
  ChunkVec<std::pair<Myocyte *, Myocyte *>> noflux_chunks;

  void construct_noflux_cond(int thread_count);

  void construct_dirichlet_cond(int thread_count);

  // ensure Zero-flux boundary conditions
  void apply_noflux(int threadid);

  // ensure 0-Dirichlet boundary conditions
  void apply_dirichlet(int threadid);

 public:
  BoundaryConditions(Myocardium *_t, bool _noflux, const int thread_count) : t(_t), noflux(_noflux) {
    if (noflux) {
      construct_noflux_cond(thread_count);
    } else {
      construct_dirichlet_cond(thread_count);
    }
  }

  void apply() {
    apply(0);
  }

  void apply(const int threadid) {
    if (!noflux_boundary.empty()) {
      apply_noflux(threadid);
    }

    if (!dirichlet_boundary.empty()) {
      apply_dirichlet(threadid);
    }
  }
};