#pragma once

#include <utility>
#include <unordered_set>

#include "parameters.h"
#include "myocyte.h"
#include "diffusion.h"
#include "hexaedra.h"
#include "homespring.h"
#include "boundary_conditions.h"
#include "helper/threading.h"

extern Parameters prm;

class Myocardium {
  friend class BoundaryConditions;

 protected:
  vector<Myocyte> myocytes;        // myocytes that are part of the myocardium
  vector<Diffusion> gapjunctions;  // diffusive connections to neighbours understood as gapjunctions

  vector<Hexaedra> hexaedras;     // for each myocyte create hexaedra
  vector<Homespring> homesprings;

  std::unique_ptr<BoundaryConditions> boundary;  // enforce boundary conditions


  class ThreadPool {
    // number of working threads
    const unsigned thread_count;

    std::vector<std::thread> threads;

    // signal threads to exit, timestep_threads_semaphore->signal() must be called after setting it to true
    std::atomic<bool> kill_threads{false};

    // semaphore, call with .signal(num_threads) for one TimeStep using all threads
    std::unique_ptr<LightweightSemaphore> timestep_threads_semaphore;

    // callback when the TimeStepThreads have completed one TimeStep
    std::unique_ptr<AutoResetEvent> timestep_completed_autoreset;

    static void timeStepThread(Chunk<Myocyte> myocyte_c,
                               Chunk<Diffusion> gapjunction_c,
                               Chunk<Hexaedra> hexaedra_c,
                               Chunk<Homespring> homespring_c,
                               BoundaryConditions *boundary,
                               LightweightSemaphore *run_semaphore,
                               AutoResetEvent *timestep_finished,
                               std::shared_ptr<ThreadBarrier> barrier,
                               std::atomic<bool> &exit,
                               int id);

   public:
    ThreadPool(unsigned thread_count,
               vector<Myocyte> &myocytes,
               vector<Diffusion> &gapjunctions,
               vector<Hexaedra> &hexaedras,
               vector<Homespring> &homesprings,
               BoundaryConditions &boundary)
        : thread_count(thread_count) {
      ChunkVec<Myocyte> myocytes_chunks      = make_chunks(myocytes, thread_count);
      ChunkVec<Diffusion> gapjunction_chunks = make_chunks(gapjunctions, thread_count);
      ChunkVec<Hexaedra> hexaedra_chunks     = make_chunks(hexaedras, thread_count);
      ChunkVec<Homespring> homespring_chunks = make_chunks(homesprings, thread_count);

      timestep_threads_semaphore   = make_unique<LightweightSemaphore>();
      timestep_completed_autoreset = make_unique<AutoResetEvent>();
      auto barrier                 = make_shared<ThreadBarrier>(thread_count);
      kill_threads                 = false;

      threads.reserve(thread_count);
      for (int i = 0; i < thread_count; ++i) {
        threads.emplace_back(timeStepThread,  // thread will execute this function
                             myocytes_chunks[i], gapjunction_chunks[i], hexaedra_chunks[i],
                             homespring_chunks[i], &boundary, timestep_threads_semaphore.get(),
                             timestep_completed_autoreset.get(), barrier, std::ref(kill_threads), i);
      }
    }

    ~ThreadPool() {
      kill_threads = true;
      timestep_threads_semaphore->signal(thread_count);
      for (auto &t : threads) {
        t.join();
      }
    }

    void timeStep() {
      timestep_threads_semaphore->signal(thread_count);  // do one TimeStep with thread_count threads
      timestep_completed_autoreset->wait();              // wait for threads to finish one TimeStep
    }
  };

  std::unique_ptr<Myocardium::ThreadPool> threadpool;

  const string filename_u      = prm.filename_prefix + "_u.dat";
  const string filename_v      = prm.filename_prefix + "_v.dat";
  const string filename_T      = prm.filename_prefix + "_T.dat";
  const string filename_pos    = prm.filename_prefix + "_pos.dat";
  const string filename_fibers = prm.filename_prefix + "_fiberorientation.dat";

  virtual void constructMyocytes();
  void constructGapjunctions();
  void orientateFibers(bool write_output);
  void constructHexaedras();
  void constructHomesprings();

  static bool idx_inside_system(const IdxVec &p) {
    /* clang-format off */
    return (p[0] >= prm.padding_dims[0] &&
            p[1] >= prm.padding_dims[1] &&
            p[2] >= prm.padding_dims[2] &&
            p[0] < prm.Nx + prm.padding_dims[0] &&
            p[1] < prm.Ny + prm.padding_dims[1] &&
            p[2] < prm.Nz + prm.padding_dims[2]);
    /* clang-format on */
  }

  static bool idx_is_valid(const IdxVec &p) {
    return (p[0] >= 0 && p[1] >= 0 && p[2] >= 0 && p[0] < prm.Lx && p[1] < prm.Ly && p[2] < prm.Lz);
  }

  static IdxVec idx_to_padded_idx(const IdxVec &p) { return p + prm.padding_dims; }

 public:
  explicit Myocardium(bool write_output);

  Myocyte *getMyocyte(const IdxVec &idx) {
    auto pidx      = idx_to_padded_idx(idx);
    const size_t p = pidx.dot({1, prm.Lx, prm.Lx * prm.Ly});

    ASSERT_DEBUG(idx_inside_system(pidx), "idx={}, system={}, padded={} out of bounds!", idx,
                 prm.dims, prm.dims_padded);
    ASSERT_ALWAYS(p < myocytes.size(), "getMyocyte parameters out of bounds!");

    return &myocytes[p];
  }

  Myocyte *getMyocyte(int x, int y, int z) { return getMyocyte({x, y, z}); }

  Myocyte *getMyocyteWithPadding(const IdxVec &idx) {
    ASSERT_DEBUG(idx_is_valid(idx), "getMyocyteWithPadding parameters out of bounds!");

    const size_t p = idx.dot({1, prm.Lx, prm.Lx * prm.Ly});

    ASSERT_ALWAYS(p < myocytes.size(), "getMyocyteWithPadding parameters out of bounds!");

    return &myocytes[p];
  }

  Myocyte *getMyocyteWithPadding(int x, int y, int z) { return getMyocyteWithPadding({x, y, z}); }

  Hexaedra *getHexaedra(const IdxVec &idx) {
    auto pidx = idx_to_padded_idx(idx);
    size_t p  = pidx.dot({(prm.Ly - 1) * (prm.Lz - 1), prm.Lz - 1, 1});

    ASSERT_DEBUG(idx_inside_system(pidx), "idx={}, system={} out of bounds!", pidx, prm.dims);
    ASSERT_ALWAYS(p < hexaedras.size(), "getHexaedra parameters out of bounds!");
    ASSERT_ALWAYS(getMyocyte(idx) == hexaedras[p].m0);

    return &(hexaedras[p]);
  }

  // - - - - - - - - - - - - - - - - - - DYNAMICS - - - - - - - - - - - - - - - - - -

  void timeStep();

  // - - - - - - - - - - - - - - - - - - - - FUNCTIONS ON THE MYOCARDIUM - - - - - - - - - - - - - - - - - - - - //

  void resetMedium();

  void set_uv(num u, num v);

  void load_pos(string filename, long t = 0);
  void load_u(string filename, long t = 0);
  void load_v(string filename, long t = 0);
  void load_T(string filename, long t = 0);
  void load_uvT(string filename_prefix, long t = 0);

  // - - - - - - - - - - - - - - - - - save data to file - - - - - - - - - - - - - - - - - - - - - - - //


  void writeBinaryFileElectro();
  void writeBinaryFileMechanics();

  void snapshot_uv(string filename_prefix);
  void snapshot_uvT(string filename_prefix, bool snapshot_T = true);

  void saveFiberOrientations(bool initialOrientation = true);

  // - - - - - - - - - - - - - - - - - fiber manipulation - - - - - - - - - - - - - - - - - - - - - - - //

  void realignMyocytesLinear(Vec3 fiber, Vec3 hint, bool initial = false);

  void realignMyocytesRotatedInZ(Vec3 fiber, Vec3 hint, num zmax_angle = 90, bool initial = false);

  void rotateMyocytes(Vec3 axis, num theta_rad);

  void realignMyocytesRandom();
};