#include "boundary_conditions.h"
#include "myocardium.h"

void BoundaryConditions::construct_noflux_cond(const int thread_count) {
  // ensure Zero-flux boundary conditions
  for (int x = 0; x < prm.Nx; x++) {  // 0-flux boundary conditions
    for (int y = 0; y < prm.Ny; y++) {
      noflux_boundary.emplace_back(t->getMyocyte(x, y, 1), t->getMyocyte(x, y, 0));
      noflux_boundary.emplace_back(t->getMyocyte(x, y, prm.Nz - 2), t->getMyocyte(x, y, prm.Nz - 1));
    }
  }

  for (int y = 0; y < prm.Ny; y++) {  // 0-flux boundary conditions
    for (int z = 0; z < prm.Nz; z++) {
      noflux_boundary.emplace_back(t->getMyocyte(1, y, z), t->getMyocyte(0, y, z));
      noflux_boundary.emplace_back(t->getMyocyte(prm.Nx - 2, y, z), t->getMyocyte(prm.Nx - 1, y, z));
    }
  }

  for (int z = 0; z < prm.Nz; z++) {  // 0-flux boundary conditions
    for (int x = 0; x < prm.Nx; x++) {
      noflux_boundary.emplace_back(t->getMyocyte(x, 1, z), t->getMyocyte(x, 0, z));
      noflux_boundary.emplace_back(t->getMyocyte(x, prm.Ny - 2, z), t->getMyocyte(x, prm.Ny - 1, z));
    }
  }

  noflux_chunks = make_chunks(noflux_boundary, thread_count);
}

void BoundaryConditions::apply_noflux(const int threadid) {
  const auto &noflux_chunk = noflux_chunks[threadid];

  for (auto it = noflux_chunk.first; it < noflux_chunk.second; ++it) {
    const num u = (it->first)->getu_threadsafe();
    (it->second)->writeu_threadsafe(u);
  }
}

void BoundaryConditions::construct_dirichlet_cond(const int thread_count) {
  // ensure 0-Dirichlet boundary conditions

  std::unordered_set<IdxVec> poss;
  for (int x = 0; x < prm.Nx; x++) {
    for (int y = 0; y < prm.Ny; y++) {
      poss.insert({x, y, 0});
      poss.insert({x, y, prm.Nz - 1});
    }
  }
  for (int y = 0; y < prm.Ny; y++) {
    for (int z = 0; z < prm.Nz; z++) {
      poss.insert({0, y, z});
      poss.insert({prm.Nx - 1, y, z});
    }
  }
  for (int z = 0; z < prm.Nz; z++) {
    for (int x = 0; x < prm.Nx; x++) {
      poss.insert({x, 0, z});
      poss.insert({x, prm.Ny - 1, z});
    }
  }

  dirichlet_boundary.reserve(poss.size());
  for (auto &pos : poss) {
    dirichlet_boundary.emplace_back(t->getMyocyte(pos));
  }

  dirichlet_chunks = make_chunks(dirichlet_boundary, thread_count);
}

void BoundaryConditions::apply_dirichlet(const int threadid) {
  const auto &dirichlet_chunk = dirichlet_chunks[threadid];

  for (auto it = dirichlet_chunk.first; it < dirichlet_chunk.second; ++it) {
    (*it)->writeu(0);
  }
}