#pragma once

#include "myocyte.h"

extern Parameters prm;

// Hold the outermost myocytes in place by connecting them with
// a spring to their reference position
class Homespring {
 private:
  Myocyte *m0;

 public:

  Homespring(Myocyte *m0) : m0(m0) { }

  void updateHomesprings() {

    const Vec3 c  = m0->getPos() - m0->getRefPos();
    const Vec3 hf = c * prm.hk;  // home force

    m0->addForce(-hf);
  }
};
