#pragma once

#include "data_assim/rate.h"
#include "electro-mechanics/deformation_metrics.h"

extern Parameters prm;

class Volumechange3D : public Rate3D {

  const pos_func3D input;
  const num initial_volume;

  int clamp_z(int z) override { return clamp(z, 0, prm.Nz - 1); }

 public:
  Volumechange3D(pos_func3D f, string out_filename, int z_begin = 0, int z_end = prm.Nz - 1)
      : Rate3D(out_filename), input(std::move(f)), initial_volume(prm.spacing * prm.spacing * prm.spacing) {

    this->type = DeformationMetrics::Ratetype(RateType::Volumechange);

    set_z_start_end(z_begin, z_end);

    set_minmax(0.5);
  }

  template <typename T>
  Volumechange3D(T t, string out_filename, int z_begin = 0, int z_end = prm.Nz - 1)
      : Volumechange3D(Rate3D::make_pos_func(t), out_filename) {}

  void calc_volumechange(const pos_func3D &input, Array3<num> &output) {
    for (int x = 0; x < prm.Nx - 1; x++) {
      for (int y = 0; y < prm.Ny - 1; y++) {
        for (int z = _z_start; z < _z_end; z++) {
          /* clang-format off */
          std::array<Vec3, 8> pos = {{
              input(x,     y,     z),
              input(x + 1, y,     z),
              input(x + 1, y + 1, z),
              input(x,     y + 1, z),

              input(x,     y,     z + 1),
              input(x + 1, y,     z + 1),
              input(x + 1, y + 1, z + 1),
              input(x,     y + 1, z + 1)
          }};
          /* clang-format on */

          output(x, y, z) = DeformationMetrics::Volume(pos)/initial_volume - 1;
        }
      }
    }
  }

  using Rate3D::compute;

  void compute(int t, Array3<num> &output) final {
    calc_volumechange(input, output);
    if (_z_end == prm.Nz - 1) {
      // use the volumechange vales from plane Nz-2 for plane Nz-1
      output[prm.Nz - 1] = output[prm.Nz - 2];
    } else {
      set_zero(_z_end, prm.Nz, output);
    }
  }
};