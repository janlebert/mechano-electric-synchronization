#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <functional>
#include <algorithm>
#include <vector>

#include "helper/arrays.h"
#include "helper/helper.h"
#include "parameters.h"
#include "electro-mechanics/myocardium.h"
#include "electro-mechanics/deformation_metrics.h"

extern Parameters prm;

using pos_func3D = std::function<Vec3(int, int, int)>;
using num_func3D = std::function<num(int, int, int)>;
using MinMax     = std::pair<num, num>;

/*
 * Base class for volumechangerate …
 * Generalized container of floats to be recomputed regularly with common functionalities
 */
class Rate3D {
 private:
  unsigned calibrated_count = 0;

 public:
  Array3<num> rate;
  bool minmax_fixed = false;
  bool calibrated   = false;
  string type       = "rate";
  int z_slice       = 0;
  std::vector<IdxVec> coords;

  Rate3D(string out_filename) : rate(prm.dims), _outfile(out_filename) {

    _minmax = make_shared<MinMax>(-1, 1);
    create_coords();

    io::deleteFile(out_filename);
  }

  virtual ~Rate3D() = default;

  virtual void compute() { compute(0); }

  virtual void compute(int t) { compute(t, rate); }

  virtual void compute(int t, Array3<num> &output) = 0;

  void write() const {
    if (_outfile.empty()) return;
    rate.save(_outfile);
  }

  int z_start() const { return _z_start; }

  int z_end() const { return _z_end; }

  void set_z_start_end(int z_start, int z_end) {
    _z_start = clamp_z(z_start);
    _z_end   = clamp_z(z_end);
    create_coords();
  }

  void set_outfile(string outfile) { _outfile = outfile; }

  MinMax minmax() {
    ASSERT_ALWAYS(_minmax);

    return *_minmax;
  }

  void set_minmax(num min_max) {
    _minmax->first  = -std::abs(min_max);
    _minmax->second = std::abs(min_max);
  }

  void set_minmax_fixed(num min_max) {
    minmax_fixed = true;
    set_minmax(min_max);
  }

  void set_minmax_fixed(Rate3D *minmax_rate) {
    minmax_fixed = true;
    _minmax      = minmax_rate->_minmax;
  }

  void set_minmax_fixed(MinMax min_max) {
    minmax_fixed = true;
    _minmax = make_shared<MinMax>(min_max);
  }

  void set_zero(int z_begin, int z_end) { set_zero(z_begin, z_end, rate); }

  void set_zero(int z_begin, int z_end, Array3<num> &output) {
    for (int z = z_begin; z < z_end; z++) {
      for (int y = 0; y < prm.Ny - 1; y++) {
        for (int x = 0; x < prm.Nx - 1; x++) {
          output(x, y, z) = 0;
        }
      }
    }
  }

  MinMax calc_minmax() const { return Array::minmax(rate); };

  /*
   * Calibrate min, max values based on min/max of the rate array
   */
  void calibrate_minmax(bool forced = false, num factor = 1) {

    if (minmax_fixed) {
      return;
    }

    if (calibrated && !forced) {
      return;
    }

    auto minmax = Array::minmax(rate);
    minmax.first *= factor;
    minmax.second *= factor;

    calibrate_minmax(minmax, forced);
  }

  /*
   * Calibrate min, max values based on the n'th percentile (so that random peaks are ignored)
   */
  void calibrate_minmax_percentile(const num percentile, bool forced = false) {

    if (minmax_fixed) {
      return;
    }

    if (calibrated && !forced) {
      return;
    }

    calibrate_minmax(Array::percentile_minmax(rate, percentile), forced);
  }

  // Use std::less, std::greater, etc. as ComparisonOperator
  template <typename ComparisonOperator>
  void appy_threshold(num val, ComparisonOperator comp) {
    Array::threshold(this->rate, this->rate, val, comp);
  }

  static pos_func3D make_pos_func(Myocardium *t) {
    return [t](int i, int j, int k) -> Vec3 { return t->getMyocyte(i, j, k)->getPos(); };
  };

  static pos_func3D make_pos_func(Array3<Vec3> *a) {
    return [a](int i, int j, int k) -> Vec3 { return (*a)(i, j, k); };
  };

 protected:
  int _z_start                    = 0;
  int _z_end                      = prm.Nz;
  std::shared_ptr<MinMax> _minmax = {};
  string _outfile;

  virtual int clamp_z(int z) { return clamp(z, prm.Nz); };

  void create_coords() {
    coords.clear();

    for (int z = _z_start; z < _z_end; z++) {
      for (int y = 0; y < rate.Ny(); y++) {
        for (int x = 0; x < rate.Nx(); x++) {
          coords.emplace_back(IdxVec(x, y, z));
        }
      }
    }
  }

  void calibrate_minmax(const MinMax &mm, bool forced) {
    ASSERT_ALWAYS(_minmax);

    num abs = std::max(std::abs(mm.first), std::abs(mm.second));

    const num threshold = 5e-5;  // ignore small values
    if (forced || !calibrated || ((abs > threshold) && (abs > _minmax->second))) {
      log_msg("calibrated rate {} {} to display range [{}, {}]", type, static_cast<void *>(this),
              -abs, abs);
      _minmax->first  = -abs;
      _minmax->second = abs;

      if (calibrated_count < 5) {
        calibrated_count += 1;
      } else {
        calibrated = true;
      }
    }
  }
};

class FileRate3D : public Rate3D {
  void init() { this->type = "file"; }

 public:
  InStream<num> &in;

  FileRate3D(InStream<num> &_in) : Rate3D(""), in(_in) { init(); };

  using Rate3D::compute;

  void compute() final { FATAL_ERROR("FileRate3D needs time parameter for compute()"); }

  void compute(int t, Array3<num> &output) override { output.load(in, t); }
};

class FloatDiffRate3D : public Rate3D {
  num_func3D input1;
  num_func3D input2;

  static auto make_num_func(Array3<num> *a) {
    ASSERT_ALWAYS(a->dimensions() == prm.dims);
    return [a](int i, int j, int k) -> num { return a->operator()(i, j, k); };
  }

  static auto make_num_func(Rate3D *r) {
    return [r](int i, int j, int k) -> num { return r->rate(i, j, k); };
  }

  static auto make_num_func(Myocardium *t) {
    return [t](int i, int j, int k) -> num { return t->getMyocyte(i, j, k)->getu(); };
  }

 public:
  FloatDiffRate3D(num_func3D f1, num_func3D f2, string out_file, int z_begin = 0, int z_end = prm.Nz)
      : Rate3D(out_file), input1(std::move(f1)), input2(std::move(f2)) {
    type = "floatdiff";
    set_z_start_end(z_begin, z_end);
  }

  template <typename U, typename V>
  FloatDiffRate3D(U *r1, V *r2, string out_filename, int z_begin = 0, int z_end = prm.Nz)
      : Rate3D(out_filename) {
    input1 = make_num_func(r1);
    input2 = make_num_func(r2);
    type   = "floatdiff";
    set_z_start_end(z_begin, z_end);
  }

  using Rate3D::compute;
  void compute(int t, Array3<num> &output) final {
    for (int k = _z_start; k < _z_end; k++) {
      for (int j = 0; j < prm.Ny; j++) {
        for (int i = 0; i < prm.Nx; i++) {
          output(i, j, k) = input1(i, j, k) - input2(i, j, k);
        }
      }
    }
  }
};

class RateDebug3D : public Rate3D {
 public:
  RateDebug3D(string out_filename = "") : Rate3D(out_filename) { type = "debug"; }

  using Rate3D::compute;
  void compute(int t, Array3<num> &output) override {}
};