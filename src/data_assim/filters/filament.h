#pragma once

#include "data_assim/filters/hilbert.h"

namespace Filament {
  void compute_phase(CircularArray<num>& in, CircularArray<num>& phase, num mean);
  CircularArray<num> compute_phase(CircularArray<num>& in, num mean);

  /*
   * Checks if there is a phase singularity at point `pos`.
   *
   * Returns  0 if `pos` is no phase singularity
   * Returns -1 for negative charged phase singularity
   * Returns  1 for positive charged phase singularity
   */
  int is_filament(const Array3<num>& phase, IdxVec pos, num cutoff = 0.99);

  /*
   * Compute filaments from phase with `is_filament()`, store result in a CircularArray (0 for no phase singularity, -1 and 1 for phase singularity)
   */
  void compute_filaments(const CircularArray<num>& phase, CircularArray<num>& out, num cutoff);
  CircularArray<num> compute_filaments(const CircularArray<num>& phase, num cutoff);
}  // namespace Filament

/* Implementation */
namespace Filament {

  /* Implementation */
  void compute_phase(CircularArray<num>& in, CircularArray<num>& phase, const num mean) {

    std::transform(in.begin(), in.end(), in.begin(),
                   [&mean](const num& x) -> num { return x - mean; });

    Hilbert::CircularArrayHolder hilbert(in, &phase);
    hilbert.apply();
  }

  /* Implementation */
  CircularArray<num> compute_phase(CircularArray<num>& in, const num mean) {
    CircularArray<num> phase(in.dimensions(), in.buffer_multiplier());
    compute_phase(in, phase, mean);
    return phase;
  }


  /* Implementation */
  int is_filament(const Array3<num>& phase, IdxVec pos, const num cutoff) {

    const IdxVec u1{1, 0, 0};
    const IdxVec u2{0, 1, 0};
    const IdxVec u3{0, 0, 1};

    const IdxVec u12{1, 1, 0};
    const IdxVec u13{1, 0, 1};
    const IdxVec u23{0, 1, 1};

    // Ignore points on the boundary
    if ((pos[0] <= 1 || pos[0] >= prm.Nx - 2) || (pos[1] <= 1 || pos[1] >= prm.Ny - 2) ||
        (pos[2] <= 1 || pos[2] >= prm.Nz - 2)) {
      return 0;
    }

    std::array<num, 4> path_X = {{
        phase(pos + u3) - phase(pos),
        phase(pos + u23) - phase(pos + u3),
        phase(pos + u2) - phase(pos + u23),
        phase(pos) - phase(pos + u2),
    }};

    std::array<num, 4> path_Y = {{
        phase(pos + u3) - phase(pos),
        phase(pos + u13) - phase(pos + u3),
        phase(pos + u1) - phase(pos + u13),
        phase(pos) - phase(pos + u1),
    }};

    std::array<num, 4> path_Z = {{
        phase(pos + u2) - phase(pos),
        phase(pos + u12) - phase(pos + u2),
        phase(pos + u1) - phase(pos + u12),
        phase(pos) - phase(pos + u1),
    }};

    auto step_fnc = [](auto& arr) {
      for (auto& i : arr) {
        if (i > M_PI) {
          i -= 2 * M_PI;
        } else if (i < -M_PI) {
          i += 2 * M_PI;
        }
      }
    };

    step_fnc(path_X);
    step_fnc(path_Y);
    step_fnc(path_Z);

    int result = 0;

    auto check_path = [&result, &cutoff](const auto& path) {
      const num sum = Array::sum(path) / (static_cast<num>(2) * M_PI);

      if (sum > cutoff) {
        result = 1;
      } else if (sum < -cutoff) {
        result = -1;
      }
    };

    check_path(path_X);
    if (result) return result;
    check_path(path_Y);
    if (result) return result;
    check_path(path_Z);

    return result;
  }


  /* Implementation */
  void compute_filaments(const CircularArray<num>& phase,
                         CircularArray<num>& out,
                         const num cutoff) {
    for (long t = 0; t < phase.Nt(); t++) {
      for (const auto& pos : phase.positions) {
        out(pos, t) = Filament::is_filament(phase[t], pos, cutoff);
      }
    }
  }

  /* Implementation */
  CircularArray<num> compute_filaments(const CircularArray<num>& phase, const num cutoff) {
    CircularArray<num> filaments(phase.dimensions(), phase.buffer_multiplier());
    compute_filaments(phase, filaments, cutoff);
    return filaments;
  }
}