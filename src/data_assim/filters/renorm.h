#pragma once

#include "data_assim/rate.h"

class Renorm {
 protected:
  Array3<MinMax> minmax_db;
  bool update_mm_db = true;

 public:
  explicit Renorm(IdxVec dims) : minmax_db(dims) {}

  void reset() { minmax_db.Load({0, 0}); }

  void apply(Array3<num> &in, Array3<num> &out) {

    ASSERT_DEBUG(minmax_db.dimensions() == in.dimensions());
    ASSERT_DEBUG(in.dimensions() == out.dimensions());

    for (auto &pos : in.positions) {
      const num x  = in(pos);
      auto &minmax = minmax_db(pos);

      if (update_mm_db) {
        if (x < minmax.first) minmax.first = x;
        if (x > minmax.second) minmax.second = x;
      }

      out(pos) = renorm_value(x, minmax);
    }
  }

  void apply(Array3<num> &inout) { apply(inout, inout); }

  void set_MinMaxDB(const Array3<MinMax> &mm) {
    minmax_db    = mm;
    update_mm_db = false;
  }

  Array3<MinMax> get_MinMaxDB() { return minmax_db; }

  void saveMinMaxDB(string prefix, RateType rt) {
    string fn = fmt::format("{}_{}_minmax.dat", prefix, DeformationMetrics::Ratetype(rt));
    minmax_db.save(fn);
  }

  bool loadMinMaxDB(string prefix, RateType rt) {
    string fn = fmt::format("{}_{}_minmax.dat", prefix, DeformationMetrics::Ratetype(rt));
    if (io::fileExists(fn)) {

      Array3<MinMax> mm(minmax_db.dimensions());
      mm.load(fn, 0);

      set_MinMaxDB(mm);
      return true;
    } else {
      return false;
    }
  }

  template <typename T>
  static T renorm_value(const T &v, const MinMax &minmax) {
    return renorm_value(v, minmax.first, minmax.second);
  }

  template <typename T>
  static T renorm_value(const T &v, const T &min, const T &max) {
    T factor = _renorm_factor(min, max);
    return _renorm_value(v, min, factor);
  }

 private:
  template <typename T>
  static T _renorm_factor(const T &min, const T &max) {
    const num factor = (max - min) / (static_cast<T>(2));
    if (std::abs(factor) < 1e-6) return 1;
    return factor;
  }

  template <typename T>
  static T _renorm_value(const T &v, const T &min, const T &factor) {
    return (v - min) / factor - 1;
  }
};