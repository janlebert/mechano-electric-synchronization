#pragma once

#include <random>
#include <algorithm>

#include "helper/helper.h"
#include "helper/arrays.h"

#ifndef RNG_SEED  // Default seed for Mersenne Twister 19937 rng
#define RNG_SEED std::random_device{}(), std::random_device{}(), std::random_device{}()
#endif

class NoiseAdder {
 protected:
  std::array<std::normal_distribution<num>, 3> dists;

 public:
  std::mt19937 rng;

  NoiseAdder(Vec3 mean, Vec3 stddev, std::seed_seq seed = {RNG_SEED}) : rng(seed) {

    set_params(mean, stddev);

    set_seed(seed);
  }

  NoiseAdder(num stddev) : NoiseAdder(Vec3(0, 0, 0), Vec3(stddev, stddev, stddev)){};

  NoiseAdder(Vec3 stddev) : NoiseAdder(Vec3(0, 0, 0), stddev){};

  NoiseAdder() : NoiseAdder(1){};

  void set_params(Vec3 mean, Vec3 stddev) {
    dists = {{std::normal_distribution<num>(mean[0], stddev[0]),
              std::normal_distribution<num>(mean[1], stddev[1]),
              std::normal_distribution<num>(mean[2], stddev[2])}};
  }

  void set_params(num stddev) { set_params(Vec3(0, 0, 0), Vec3(stddev, stddev, stddev)); }

  void set_params(Vec3 stddev) { set_params(Vec3(0, 0, 0), stddev); }

  void set_seed(std::seed_seq &seed) {
    rng.seed(seed);

    // log rng seed
    std::stringstream ss;
    seed.param(std::ostream_iterator<unsigned int>(ss, "u, "));
    string seed_str = ss.str();
    seed_str        = seed_str.substr(0, seed_str.length() - 2);
    log_msg("NoiseAdder rng seed {}", seed_str);
  }

  void apply(Array3<num> &inout) { apply(inout, inout); }

  void apply(Array3<num> &in, Array3<num> &out) {
    for (auto &pos : in.positions) {
      out(pos) = in(pos) + random_number();
    }
  }

  void apply(Array3<Vec3> &inout) { apply(inout, inout); }

  void apply(Array3<Vec3> &in, Array3<Vec3> &out) {
    for (auto &pos : in.positions) {
      out(pos) = in(pos) + random_vector();
    }
  }

  num random_number() { return dists[0](rng); }

  Vec3 random_vector() { return {dists[0](rng), dists[1](rng), dists[2](rng)}; }

  // returns a uniformly distributed vector on the unit sphere
  Vec3 random_unit_vector() {
    // https://stats.stackexchange.com/questions/7977/ :
    // To get a sample uniformly distributed on an n-sphere (surface)
    // 1) generate X from n-dimensional standard normal distribution
    // 2) divide each component of X by the Euclidean norm of X
    // See also Harman, R. & Lacko, V. On decompositional algorithms for uniform sampling from nn-spheres and nn-balls Journal of Multivariate Analysis, 2010

    ASSERT_ALWAYS(dists[0].mean() == 0);
    ASSERT_ALWAYS(dists[0].stddev() == 1);

    Vec3 v{dists[0](rng), dists[0](rng), dists[0](rng)};

    auto l = v.length();
    if (l < 1e-5) {
      return random_unit_vector();
    }

    return v / l;
  }
};