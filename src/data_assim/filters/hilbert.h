#pragma once

#include "helper/arrays.h"
#include "data_assim/filters/fft.h"

/* forward declarations */
namespace Hilbert {

  // Generate forward and backward fftw plans to compute analytic signal
  inline fftw::plans createPlans(
      const int *n, num *in, fftw::stdcomplex *kspace, int howmany = 1, int stride = 1);

  // Computes discrete-time analytic signal of a real signal using Hilbert transformation
  //
  // The real time part of `signal` is the original data
  // The imaginary part of `signal` is the Hilbert Transform
  // `phase` (if provided) contains the phase
  //
  // Marple, S.L. Computing the discrete-time analytic signal via FFT, IEEE Transactions on Signal Processing, Vol. 47, No. 9, September 1999, pp.2600-2603
  //
  // plans should be generated with createPlans()
  // kspace is the resulting analytical signal used in createPlans()
  void analyticSignal(const fftw::plans &plans,
                      fftw::stdcomplex *kspace,
                      size_t N,
                      num *phase = nullptr,
                      int stride = 1);

  // type T is a container of value type fftw::stdcomplex, e.g. std::vector<fftw::stdcomplex>, std::array<fftw::stdcomplex, N> …
  template <typename T>
  void analyticSignal(const fftw::plans &plans, T &signal, num *phase = nullptr);

  // most basic version, iterators begin and end over analytical signal (kspace)
  template <typename It>
  void analyticSignal(const fftw::plans &plans,
                      const It &begin,
                      const It &end,
                      num *phase = nullptr,
                      int stride = 1);

  // special CircularArray version
  // t >=0: compute analytic signal and phase only for one time point
  inline void analyticSignal(const fftw::plans &plans,
                             CircularArray<num> &in,
                             CircularArray<fftw::stdcomplex> &signal,
                             CircularArray<num> *phase = nullptr,
                             long t                    = -1);

  class CircularArrayHolder {
   protected:
    fftw::plans plans;
    std::array<int, 1> N = {};

   public:
    CircularArray<fftw::stdcomplex> analytic_signal;
    CircularArray<num> &in;
    CircularArray<num> *phase = nullptr;

    CircularArrayHolder(CircularArray<num> &in, CircularArray<num> *phase = nullptr)
        : analytic_signal(in.dimensions(), in.buffer_multiplier()), in(in), phase(phase) {
      if (phase) {
        ASSERT_ALWAYS(in.dimensions() == phase->dimensions());
      }

      N[0]              = in.Nt();
      const int howmany = in.Nx() * in.Ny() * in.Nz();

      plans = createPlans(N.data(), in.data(), analytic_signal.data(), howmany, howmany);
    }

    ~CircularArrayHolder() {
      fftw::destroy_plan(plans.first);
      fftw::destroy_plan(plans.second);
    }

    void apply(long t = -1) { analyticSignal(plans, in, analytic_signal, phase, t); };
    void apply(CircularArray<num> &in, CircularArray<num> *phase = nullptr, long t = -1) {
      analyticSignal(plans, in, analytic_signal, phase, t);
    };
  };

  namespace detail {
    // phase = imag(log(analytic_signal))
    inline num compute_phase(const fftw::stdcomplex &c) {
      return (c == fftw::stdcomplex(0, 0)) ? 0 : std::log(c).imag();
    }

    // function generator to normalize analytical signal
    inline auto normalize_signal(const num Nt) {
      return [Nt](const fftw::stdcomplex &c) -> fftw::stdcomplex { return c / Nt; };
    }

    template <typename It>
    void analyticSignalMask(It first, It last);
  }  // namespace detail
}  // namespace Hilbert


/* Implementation */
namespace Hilbert {
  namespace detail {
    template <typename It>
    void analyticSignalMask(It first, It last) {
      static_assert(
          std::is_same<typename It::iterator_category, std::random_access_iterator_tag>::value,
          "iterator has to be an random access iterator");

      const auto N = static_cast<long>(last - first);

      long half = N / 2l;    // N even
      if (2l * half != N) {  // N odd
        half = (N + 1) / 2l;
      }

      for (long i = 1; i <= half - 1; i++) {
        first[i] *= 2;
      }

      for (long i = half + 1; i < N; i++) {
        first[i] = 0;
      }
    }
  }  // namespace detail

  fftw::plans createPlans(const int *n, num *in, fftw::stdcomplex *kspace, int howmany, int stride) {
    // TODO: make sure memory is aligned to make use of SSE etc., see http://www.fftw.org/fftw3_doc/New_002darray-Execute-Functions.html

    ASSERT_ALWAYS(n != nullptr);
    ASSERT_ALWAYS(in != nullptr);
    ASSERT_ALWAYS(kspace != nullptr);
    ASSERT_ALWAYS(howmany > 0);
    ASSERT_ALWAYS(stride > 0);

    fftw::init_threads();

    const int rank = 1;
    const int dist = 1;

    fftw::complex *out = reinterpret_cast<fftw::complex *>(kspace);
    const int *inembed = n, *onembed = n;

    /* clang-format off */
    return {
        fftw::plan_many_dft_r2c(rank, n, howmany, in, inembed, stride, dist, out, onembed, stride, dist, FFTW_ESTIMATE | FFTW_UNALIGNED),
        fftw::plan_many_dft(rank, n, howmany, out, onembed, stride, dist, out, onembed, stride, dist, FFTW_BACKWARD, FFTW_ESTIMATE | FFTW_UNALIGNED)
    };
    /* clang-format on */
  }


  template <typename T>
  void analyticSignal(const fftw::plans &plans, T &kspace, num *phase) {
    static_assert(std::is_same<typename T::value_type, fftw::stdcomplex>::value,
                  "signal has to be a container of value type fftw::stdcomplex");

    auto N = kspace.size();  // C++17: use std::size()
    analyticSignal(plans, kspace.data(), N, phase);
  }

  void analyticSignal(
      const fftw::plans &plans, fftw::stdcomplex *kspace, size_t N, num *phase, int stride) {
    const auto begin = RawIterator<fftw::stdcomplex>(kspace);
    const auto end   = RawIterator<fftw::stdcomplex>(kspace + N);

    return analyticSignal(plans, begin, end, phase, stride);
  }

  template <typename It>
  void analyticSignal(
      const fftw::plans &plans, const It &begin, const It &end, num *phase, int stride) {
    static_assert(std::is_same<typename It::value_type, fftw::stdcomplex>::value,
                  "iterators have to go over fftw::stdcomplex type data (kspace/analytical signal)");

    const auto N = std::distance(begin, end);
    ASSERT_DEBUG(N > 0);
    const num Nt = N / stride;
    ASSERT_DEBUG(N % stride == 0);  // size should be a multiple of stride

    std::fill(begin, end, fftw::stdcomplex(0, 0));


    fftw::execute(plans.first);  // Forward

    using strideItr = StrideIterator<It>;
    for (int i = 0; i < stride; i++) {
      detail::analyticSignalMask(strideItr(begin + i, stride), strideItr(end, stride));
    }

    fftw::execute(plans.second);  // Backward

    std::transform(begin, end, begin, detail::normalize_signal(Nt));
    if (phase) {
      std::transform(begin, end, RawIterator<num>(phase), detail::compute_phase);
    }
  }

  void analyticSignal(const fftw::plans &plans,
                      CircularArray<num> &in,
                      CircularArray<fftw::stdcomplex> &signal,
                      CircularArray<num> *phase,
                      long t) {
    using strideItr = StrideIterator<CircularArray<fftw::stdcomplex>::iterator>;

    signal.set_t_end(in.get_t_end());
    if (phase) phase->set_t_end(in.get_t_end());
    signal.fill(fftw::stdcomplex(0, 0));
    fftw::complex *signal_ptr = reinterpret_cast<fftw::complex *>(signal.data());


    /* Forward */
    fftw::execute_dft_r2c(plans.first, in.data(), signal_ptr);

    auto stride      = signal.Nx() * signal.Ny() * signal.Nz();
    const auto begin = signal.begin();
    const auto end   = signal.end();
    for (int i = 0; i < stride; i++) {
      detail::analyticSignalMask(strideItr(begin + i, stride), strideItr(end, stride));
    }


    /* Backward */
    fftw::execute_dft(plans.second, signal_ptr, signal_ptr);

    if (t >= 0) {  // compute analytic signal and phase only for one time point
      std::transform(signal.begin(t), signal.end(t), signal.begin(t),
                     detail::normalize_signal(signal.Nt()));
      if (phase) {
        std::transform(signal.begin(t), signal.end(t), phase->begin(t), detail::compute_phase);
      }
    } else {  // compute analytic signal and phase
      std::transform(signal.begin(), signal.end(), signal.begin(),
                     detail::normalize_signal(signal.Nt()));
      if (phase) {
        std::transform(signal.begin(), signal.end(), phase->begin(), detail::compute_phase);
      }
    }
  }
}  // namespace Hilbert