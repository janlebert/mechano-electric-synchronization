#pragma once

#include <complex>
#include <fftw3.h>
#include "num.h"
#include "helper/threading.h"

/*
 * FFTW wrapper, which uses floats or double dependings on the compiletime setting for `num`.
 * also provides a global fftw thread initialization method.
 */
namespace fftw {

#define ALIAS_TEMPLATE_FUNCTION(highLevelF, lowLevelF)                                        \
  template <typename... Args>                                                                 \
  inline auto highLevelF(Args &&... args)->decltype(lowLevelF(std::forward<Args>(args)...)) { \
    return lowLevelF(std::forward<Args>(args)...);                                            \
  }

#ifdef NUM_SINGLE
#define FFTW_FCT(name) fftwf_##name
#else
#define FFTW_FCT(name) fftw_##name
#endif

#define DEFINE_FFTW_FCT(name) ALIAS_TEMPLATE_FUNCTION(name, FFTW_FCT(name))


  using stdcomplex = std::complex<num>;
  using complex    = FFTW_FCT(complex);
  using plan       = FFTW_FCT(plan);
  using plans      = std::pair<plan, plan>;

  DEFINE_FFTW_FCT(plan_dft)
  DEFINE_FFTW_FCT(plan_dft_1d)
  DEFINE_FFTW_FCT(plan_dft_r2c)
  DEFINE_FFTW_FCT(plan_dft_r2c_1d)
  DEFINE_FFTW_FCT(plan_dft_c2r)
  DEFINE_FFTW_FCT(plan_dft_c2r_1d)
  DEFINE_FFTW_FCT(plan_many_dft)
  DEFINE_FFTW_FCT(plan_many_dft_r2c)
  DEFINE_FFTW_FCT(execute)
  DEFINE_FFTW_FCT(execute_dft)
  DEFINE_FFTW_FCT(execute_dft_r2c)
  DEFINE_FFTW_FCT(destroy_plan)

  // Initialize fftw threads if they were not initialized previously
  void init_threads();

  namespace detail {
    // Global singleton object to init threads if fftw is used and clean them up on shutdown
    class ThreadOwner {
     public:
      ThreadOwner() {
#ifdef NUM_SINGLE
        fftwf_init_threads();
        fftwf_plan_with_nthreads(get_thread_count());
#else
        fftw_init_threads();
        fftw_plan_with_nthreads(get_thread_count());
#endif
      }

      ~ThreadOwner() {
#ifdef NUM_SINGLE
        fftwf_cleanup_threads();
#else
        fftw_cleanup_threads();
#endif
      }
    };

    static std::shared_ptr<ThreadOwner> ThreadOwnerObj;
  }  // namespace detail

  // Initialize fftw threads if they were not initialized previously
  void init_threads() {
    if (!fftw::detail::ThreadOwnerObj) {
      fftw::detail::ThreadOwnerObj = make_shared<fftw::detail::ThreadOwner>();
    }
  }
}  // namespace fftw
