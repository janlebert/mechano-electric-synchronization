#pragma once

extern "C" {
#include "ipol/gaussian_conv_deriche.h"
}

#include "helper/helper.h"
#include "helper/arrays.h"

class DericheFilter {
 private:
  std::vector<num> buffer;

  num sigma;
  const int K   = 3;
  const num tol = 1e-3;
  deriche_coeffs c;

  void allocate_buffer(std::vector<int> dimensions) {
    unsigned longest_dimension_length = *(std::max_element(dimensions.begin(), dimensions.end()));
    unsigned min_buffer_size          = 2 * longest_dimension_length + 1;

    if (buffer.size() < min_buffer_size) {
      buffer.resize(min_buffer_size);
    }
  }

  void apply(const Array2<num> &src, Array2<num> &dest, bool allocated) {
    if (!allocated) {
      ASSERT_DEBUG(src.dimensions() == dest.dimensions());
      allocate_buffer({src.Nx(), src.Ny()});
    }

    // should be equivalent to code below
    //deriche_gaussian_conv_image(c, dest, buffer.data(), src, src.Nx(), src.Ny(), 1);

    num *_dest      = dest;
    const num *_src = src;
    // src(i, j) == _src[i + j * nx]

    for (int x = 0; x < src.Nx(); ++x) {
      deriche_gaussian_conv(c, dest + x, buffer.data(), dest + x, src.Ny(), src.Nx());
    }

    for (int y = 0; y < src.Ny(); ++y) {
      deriche_gaussian_conv(c, _dest, buffer.data(), _src, src.Nx(), 1);
      _dest += src.Nx();
      _src += src.Nx();
    }
  }

  void apply(const Array3<num> &src, Array3<num> &dest, bool allocated) {
    if (!allocated) {
      ASSERT_DEBUG(src.dimensions() == dest.dimensions());
      allocate_buffer({src.Nx(), src.Ny(), src.Nz()});
    }

    num *_dest      = dest;
    const num *_src = src;
    for (int z = 0; z < src.Nx() * src.Ny(); ++z) {  // _src[z*src.Nx()*src.Ny()] == src(0,0,z)
      deriche_gaussian_conv(c, _dest + z, buffer.data(), _src + z, src.Nz(), src.Nx() * src.Ny());
    }

    for (int z = 0; z < src.Nz(); ++z) {
      Array2<num> dest2 = dest[z];  // dest2(i, j) == dest(i, j, z)); dest[x] == dest2;
      this->apply(dest2, dest2, true);
    }
  }

 public:
  DericheFilter(num sigma) : buffer(1000), sigma(sigma) { deriche_precomp(&c, sigma, K, tol); }

  num get_sigma() { return sigma; }

  void set_sigma(num s) {
    sigma = s;
    deriche_precomp(&c, s, K, tol);
  }

  void apply(const Array2<num> &src, Array2<num> &dest) { this->apply(src, dest, false); }

  Array2<num> apply(const Array2<num> &src) {
    Array2<num> dest(src.dimensions());
    this->apply(src, dest, false);
    return dest;
  }

  void apply(const Array3<num> &src, Array3<num> &dest) { this->apply(src, dest, false); }

  Array3<num> apply(const Array3<num> &src) {
    Array3<num> dest(src.dimensions());
    this->apply(src, dest, false);
    return dest;
  }

  void apply(const Array4<num> &src, Array4<num> &dest) {
    ASSERT_DEBUG(src.dimensions() == dest.dimensions());

    allocate_buffer({src.Nx(), src.Ny(), src.Nz(), src.Nt()});

    num *_dest      = dest;
    const num *_src = src;
    for (int t = 0; t < src.Nx() * src.Ny() * src.Nz(); ++t) {
      deriche_gaussian_conv(c, _dest + t, buffer.data(), _src + t, src.Nt(), src.Nx() * src.Ny() * src.Nz());
    }

    for (int t = 0; t < src.Nt(); ++t) {
      Array3<num> dest3 = dest[t];
      this->apply(dest3, dest3, true);
    }
  }

  Array4<num> apply(const Array4<num> &src) {
    Array4<num> dest(src.dimensions());
    this->apply(src, dest);
    return dest;
  }

  void apply(CircularArray<num> &src, CircularArray<num> &dest) {
    Array4<num> src4(src.dimensions(), src.data());
    Array4<num> dest4(dest.dimensions(), dest.data());

    this->apply(src4, dest4);
  }
};