#pragma once

#include "data_assim/volume_change.h"


std::shared_ptr<Rate3D> make_rate(RateType rt,
                                  Array3<Vec3>& pos3D,
                                  SimulationDataStreams* simDataStream) {

  string prefix = ""s;
  if (simDataStream != nullptr) {
    prefix = simDataStream->prefix();
  }

  auto make_fn = [&prefix](RateType rt) -> string {
    if (prefix.empty()) {
      return ""s;
    } else {
      return fmt::format("{}_{}.dat", prefix, DeformationMetrics::Ratetype(rt));
    }
  };

  switch (rt) {
    case RateType::Volumechange:
      return make_shared<Volumechange3D>(&pos3D, make_fn(RateType::Volumechange));
      break;
    case RateType::File:
      ASSERT_ALWAYS(simDataStream != nullptr,
                    "RateType::File selected but simDataStream == nullptr");

      return make_shared<FileRate3D>(simDataStream->E);
      break;
  }

  return {};
}


std::shared_ptr<Rate3D> make_rate(RateType rt,
                                  Myocardium& tissue,
                                  string prefix = "") {

  ASSERT_ALWAYS(rt != RateType::File,
                "RateType::File not supported in make_rate() with Myocardium& tissue parameter");

  auto make_fn = [&prefix](RateType rt) -> string {
    if (prefix.empty()) {
      return ""s;
    } else {
      return fmt::format("{}_{}.dat", prefix, DeformationMetrics::Ratetype(rt));
    }
  };

  switch (rt) {
    case RateType::Volumechange:
      return make_shared<Volumechange3D>(&tissue, make_fn(RateType::Volumechange));
      break;
    case RateType::File:  // to ignore warning
      break;
  }

  return {};
}