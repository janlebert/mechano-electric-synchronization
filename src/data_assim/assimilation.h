#pragma once

#include "parameters.h"
#include "helper.h"

extern Parameters prm;

class DataAssimSensors {
 public:
  using sensors_t = std::vector<std::unordered_set<IdxVec>>;

  sensors_t sensors;

  IdxVec sensor_size;
  IdxVec sensor_space;

  DataAssimSensors(IdxVec _sensor_size, IdxVec _sensor_space) {
    resetSensors(_sensor_size, _sensor_space);
  }

  using iterator       = sensors_t::iterator;
  using const_iterator = sensors_t::const_iterator;
  iterator begin() { return sensors.begin(); };
  iterator end() { return sensors.end(); };
  const_iterator begin() const { return sensors.begin(); };
  const_iterator end() const { return sensors.end(); };

  void resetSensors(IdxVec _sensor_size, IdxVec _sensor_space) {
    sensor_size  = _sensor_size;
    sensor_space = _sensor_space;
    sensors_t().swap(sensors);  // clear sensors


    auto sensor_idx_start = [this](int n, int i) -> int {
      return n * (sensor_size[i] + sensor_space[i]);
    };

    auto sensor_idx_end = [this](int n, int i) -> int {
      return sensor_size[i] + n * (sensor_size[i] + sensor_space[i]);
    };


    // construct sensors according to sensor_size and sensor_space starting from 0,0,0 until there are no more pixels
    std::unordered_set<IdxVec> all_pos;
    int n, m, l;
    bool in_x, in_y, in_z;
    for (n = 0, in_x = true; in_x; n++) {
      for (m = 0, in_y = true; in_y; m++) {
        for (l = 0, in_z = true; in_z; l++) {
          std::unordered_set<IdxVec> sensor;

          for (int i = sensor_idx_start(n, 0); i < sensor_idx_end(n, 0); i++) {
            for (int j = sensor_idx_start(m, 1); j < sensor_idx_end(m, 1); j++) {
              for (int h = sensor_idx_start(l, 2); h < sensor_idx_end(l, 2); h++) {
                IdxVec pos = clamp_pos({i, j, h});

                if (all_pos.insert(pos).second) {
                  // If pixel was not previously used (due to clipping), add to sensor
                  sensor.insert(pos);
                }

                if (h > prm.Nz) in_z = false;
                if (j > prm.Ny) in_y = false;
                if (i > prm.Nx) in_x = false;
              }
            }
          }

          if ((!sensor.empty()) &&
              (sensor.size() > sensor_size[0] * sensor_size[2])) {  // avoid very small sensors
            sensors.push_back(sensor);
          }
        }
      }
    }
  }
};


class DataAssim {
 public:
  DataAssimSensors sensors;

  RateDebug3D debug_assim;
  bool enable_debug_assim = false;
  bool assimilated        = false;

  DataAssim()
      : sensors(prm.sensor_size, prm.sensor_spacing),
        debug_assim(prm.filename_prefix + "_da_debug.dat") {

#ifdef WRITEOUT_EVERYTHING
    enable_debug_assim = true;

    // debug image with all pixels belonging to sensor set to 1
    RateDebug3D sensor_img(prm.da_filename_prefix + "_sensors.dat");
    sensor_img.rate.Load(0.);
    for (auto &sensor : sensors) {  // for debuging
      for (auto &idx : sensor) {
        sensor_img.rate(idx) += 1;
      }
    }

    sensor_img.write();
#endif
  }

  void resetSensors(IdxVec sensor_size, IdxVec sensor_space) {
    sensors.resetSensors(sensor_size, sensor_space);
  }

  void apply_sync_renorm(Myocardium &tissue, Array3<num> &assim, num scale = 1) {
    if (enable_debug_assim) debug_assim.rate.Load(0.);

    if (scale <= 0) {
      assimilated = false;
      return;
    } else {
      assimilated = true;
    }

    for (const auto &sensor : sensors) {
      num G = 0;
      for (auto &pos : sensor) {
        const num dG = -assim(pos) - tissue.getMyocyte(pos)->getu();
        G += dG;
      }
      G /= sensor.size();
      G *= scale;

      if (G < 0) {
        G *= prm.assim_scale_negative;
      }

      for (auto &pos : sensor) {
        const num d = prm.da_k * G;
        const num u = d + tissue.getMyocyte(pos)->getu();
        tissue.getMyocyte(pos)->writeu(clamp(u));

        if (enable_debug_assim) debug_assim.rate(pos) += d;
      }
    }

    if (enable_debug_assim) log_msg("debug assim : {}", debug_assim.calc_minmax());
  }
};
