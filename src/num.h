#pragma once

#include <cstdlib>
#include <cmath>
#include <string>
#include <vector>
#include <array>
#include <fstream>
#include <iostream>
#include <memory>

// forward declaration
template <typename T, typename D>
std::ostream& operator<<(std::ostream& out, const std::pair<T, D>& p);

#ifdef NUM_SINGLE
using num = float;
#else
using num = double;
#endif

using std::abs;
using std::array;
using std::sqrt;
using std::string;
using std::vector;

using std::cout;
using std::endl;
using std::ios;
using std::ofstream;

using std::make_shared;
using std::make_unique;

using namespace std::string_literals;

/* implementation details */

// format helper for std::pair<>
template <typename T, typename D>
std::ostream& operator<<(std::ostream& out, const std::pair<T, D>& p) {
  return out << p.first << '\t' << p.second;
}


#include "helper/logging.h"