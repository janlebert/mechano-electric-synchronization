#pragma once

#include <fstream>
#include <stdexcept>

#include "parameters.h"
#include "io.h"

extern Parameters prm;

// Centralized logging

template <typename... Args>
inline void log_msg(const char *fmt, Args &&... args) {
  log_msg(prm.logstream(), prm.t, fmt, std::forward<Args>(args)...);
  prm.logstream().flush();
}

template <typename... Args>
inline void log_msg(const string &fmt, Args &&... args) {
  log_msg(fmt.c_str(), std::forward<Args>(args)...);
}

// Various small common helper functions

template <typename T, typename D, typename F>
inline T clamp(const T &x, const D &min, const F &max) {
  if (x < min)
    return min;
  else if (x > max)
    return max;

  return x;
}

template <typename T, typename D>
inline T clamp(const T &x, const D &max) {
  return clamp(x, 0, max);
}

template <typename T>
inline T clamp(const T &u) {
  return clamp(u, 0, 1);
}

inline IdxVec clamp_pos(IdxVec v, const IdxVec &min, const IdxVec &max) {
  for (int i = 0; i < 3; i++) {
    v[i] = clamp(v[i], min[i], max[i]);
  }
  return v;
}

inline IdxVec clamp_pos(IdxVec a) {
  IdxVec max_size(prm.Nx - 1, prm.Ny - 1, prm.Nz - 1);
  return clamp_pos(a, {0, 0, 0}, max_size);
}

// Generic math helper functions for containers (std::vector, std::array, ArrayN)
namespace Array {
  // ValueType<T> gets the type of an element in a container
  template <typename T>
  using ValueType = typename T::value_type;

  template <typename T>
  auto min(const T &a) {
    return *std::min_element(std::cbegin(a), std::cend(a));
  }

  template <typename T>
  auto max(const T &a) {
    return *std::max_element(std::cbegin(a), std::cend(a));
  }

  template <typename T>
  auto sum(const T &a) {
    return std::accumulate(std::cbegin(a), std::cend(a), ValueType<T>{});
  }

  template <typename T>
  auto mean(const T &a) {
    return sum(a) / static_cast<ValueType<T>>(a.size());
  }

  template <typename T>
  auto median(const T &a) {
    std::vector<ValueType<T>> copy(a.size());
    std::copy(a.cbegin(), a.cend(), copy.begin());
    auto n = a.size() / 2;
    std::nth_element(copy.begin(), copy.begin() + n, copy.end());
    return copy[n];
  }

  template <typename T>
  auto median_inplace(T &a) {
    auto n = a.size() / 2;
    std::nth_element(a.begin(), a.begin() + n, a.end());
    return a[n];
  }

  /*
   * Square sum over the elements in the container
   */
  template <typename T>
  auto sqsum(const T &a) {
    return std::inner_product(std::cbegin(a), std::cend(a), std::cbegin(a), ValueType<T>{});
  }

  /*
   * Returns the mean and sample standard deviation
   * For BesselCorrection = true (n - 1) instead of n is used
   */
  template <typename T>
  std::pair<ValueType<T>, ValueType<T>> mean_stdev(const T &a, bool BesselCorrection = false) {
    auto mean = Array::mean(a);

    std::vector<ValueType<T>> diff(a.size());
    std::transform(std::cbegin(a), std::cend(a), diff.begin(), [mean](auto x) { return x - mean; });

    const double norm = BesselCorrection ? a.size() - 1 : a.size();
    auto stdev        = std::sqrt(sqsum(diff) / norm);

    return {mean, stdev};
  }

  /*
   * Computes sample covariance
   */
  template <typename T>
  ValueType<T> covariance(const T &a, const T &b) {
    ASSERT_ALWAYS(a.size() == b.size());
    const num N = a.size();

    // https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Covariance
    auto product = std::inner_product(a.begin(), a.end(), b.begin(), ValueType<T>{});
    return (product - sum(a) * sum(b) / N) / (N - 1);
  }

  /*
   * Computes Pearson correlation coefficient over two 1D data sets
   */
  template <typename T>
  ValueType<T> pearsoncoeff(const T &a, const T &b) {
    ASSERT_ALWAYS(a.size() == b.size());

    auto mean_a = mean(a);
    auto mean_b = mean(b);

    std::vector<ValueType<T>> diff_a(a.size());
    std::vector<ValueType<T>> diff_b(b.size());
    std::transform(std::cbegin(a), std::cend(a), diff_a.begin(),
                   [mean_a](auto x) { return x - mean_a; });
    std::transform(std::cbegin(b), std::cend(b), diff_b.begin(),
                   [mean_b](auto x) { return x - mean_b; });

    auto product = std::inner_product(diff_a.begin(), diff_a.end(), diff_b.begin(), ValueType<T>{});

    auto norm = std::sqrt(sqsum(diff_a) * sqsum(diff_b));

    if (norm < 1e-8) return 0;

    return product / norm;
  }

  /*
   * Structural similarity (SSIM) index
   * Used for measuring the similarity between two images
   */
  template <typename T, typename D>
  ValueType<T> SSIM(const T &a, const D &b) {
    static_assert(std::is_same<ValueType<T>, ValueType<D>>::value,
                  "arguments have to be containers of the same type");

    ASSERT_ALWAYS(a.size() == b.size());
    const num N = a.size();

    auto mean_a = mean(a);
    auto mean_b = mean(b);

    std::vector<ValueType<T>> diff_a(a.size());
    std::vector<ValueType<D>> diff_b(b.size());
    std::transform(std::cbegin(a), std::cend(a), diff_a.begin(),
                   [mean_a](auto x) { return x - mean_a; });
    std::transform(std::cbegin(b), std::cend(b), diff_b.begin(),
                   [mean_b](auto x) { return x - mean_b; });

    auto var_a = sqsum(diff_a);
    auto var_b = sqsum(diff_b);

    auto covar =
        std::inner_product(diff_a.begin(), diff_a.end(), diff_b.begin(), ValueType<T>{}) / (N - 1);

    // Dynamic range, e.g. 255 for 8-bit grayscale images or 1 for floating point
    // and assume for now that other have 8-bit dynamic range
    constexpr int L = std::is_floating_point<ValueType<T>>::value ? 1 : 255;

    constexpr float k1 = 0.01f;
    constexpr float k2 = 0.03f;
    constexpr float c1 = k1 * L * k1 * L;
    constexpr float c2 = k2 * L * k2 * L;

    auto numerator   = (2 * mean_a * mean_b + c1) * (2 * covar + c2);
    auto denominator = (mean_a * mean_a + mean_b * mean_b + c1) * (var_a + var_b + c2);

    return numerator / denominator;
  }

  // Returns the minimum and maximum values in the container
  template <typename T>
  std::pair<ValueType<T>, ValueType<T>> minmax(const T &a) {
    auto its = std::minmax_element(std::cbegin(a), std::cend(a));
    return {*(its.first), *(its.second)};
  }

  template <typename T>
  std::pair<ValueType<T>, ValueType<T>> percentile_minmax(const T &a, const num percentile) {
    ASSERT_DEBUG(percentile >= 0 && percentile < 1);

    std::vector<ValueType<T>> copy(a.size());
    std::copy(a.cbegin(), a.cend(), copy.begin());
    std::sort(copy.begin(), copy.end());

    ValueType<T> min = copy.at(percentile * copy.size());
    ValueType<T> max = copy.at((1 - percentile) * (copy.size() - 1));

    if (min > max) std::swap(min, max);

    return {min, max};
  }

  // Use std::less, std::greater, etc. as ComparisonOperator
  template <typename ComparisonOperator, typename T>
  void threshold(const T &in, T &out, ValueType<T> val, ComparisonOperator comp) {
    ASSERT_DEBUG(in.size() == out.size());

    auto lambda = [&comp, &val](const ValueType<T> &v) -> ValueType<T> {
      if (!comp(v, val)) {
        return static_cast<ValueType<T>>(0);
      } else {
        return v;
      }
    };

    std::transform(in.cbegin(), in.cend(), out.begin(), lambda);
  }

  template <typename ComparisonOperator, typename T>
  void threshold(T &inout, ValueType<T> val, ComparisonOperator comp) {
    threshold(inout, inout, val, comp);
  }
}  // namespace Array