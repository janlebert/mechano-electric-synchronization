#pragma once

#include "num.h"

#include "fmt/core.h"

// General logging function without requiring parameter.h, see helper.h for simplified function
void log_msg(std::ostream& out, long t, const string& msg);

template <typename... Args>
void log_msg(std::ostream& out, long t, const char* fmt, Args&&... args) {
  const string action = fmt::format(fmt, std::forward<Args>(args)...);

  log_msg(out, t, action);
}

// Assertion/Error handling, see macros at the end of file
namespace ErrorHandling {
  struct source_location {
    const char* file_name;
    unsigned line_number;
  };

  struct assertAbortHandler {

    [[noreturn]] static void handle(const source_location& loc,
                                    const char* expression,
                                    const string& msg) noexcept;
  };

  struct assertWarningHandler {

    static void handle(const source_location& loc,
                       const char* expression,
                       const string& msg) noexcept;
  };


  struct messageAbortHandler {

    [[noreturn]] static void handle(const source_location& loc, const string& msg) noexcept;
  };

  struct messageWarningHandler {

    static void handle(const source_location& loc, const string& msg) noexcept;
  };
}  // namespace ErrorHandling

// ignore variadic macro warnings
#ifdef __clang__
#pragma clang diagnostic ignored "-Wgnu-zero-variadic-macro-arguments"
#elif defined(__GNUC__)
// unfortunately applies to the rest of the file, see https://stackoverflow.com/questions/35587137/
#pragma GCC system_header
#endif





// ASSERT_ALWAYS(expr, msg, msg_params…): assertion macro with optional fmt-formatted message
#define ASSERT_ALWAYS(...) EXPAND(DO_ASSERT(ErrorHandling::assertAbortHandler, __VA_ARGS__))
// same as ASSERT_ALWAYS(), but only warn instead of std::abort()
#define CHECK_ALWAYS(...) EXPAND(DO_ASSERT(ErrorHandling::assertWarningHandler, __VA_ARGS__))

// same as ASSERT_ALWAYS()/CHECK_ALWAYS(), but only for DEBUG builds
#ifdef NDEBUG
#define ASSERT_DEBUG(...)
#define CHECK_DEBUG(...)
#else
#define ASSERT_DEBUG(...) EXPAND(DO_ASSERT(ErrorHandling::assertAbortHandler, __VA_ARGS__))
#define CHECK_DEBUG(...) EXPAND(DO_ASSERT(ErrorHandling::assertWarningHandler, __VA_ARGS__))
#endif

// FATAL_ERROR(msg, msg_params…) exit with fmt-formatted message
#define FATAL_ERROR(...) EXPAND(DO_ERROR_MSG(ErrorHandling::messageAbortHandler, __VA_ARGS__))
// same as FATAL_ERROR(), but only warn instead of std::abort()
#define WARN_ERROR(...) EXPAND(DO_ERROR_MSG(ErrorHandling::messageWarningHandler, __VA_ARGS__))



/* macros implementation */

#define CUR_SOURCE_LOCATION \
  ErrorHandling::source_location { __FILE__, static_cast<unsigned>(__LINE__) }

#define DO_ASSERT(Handle, Expr, ...) \
  (LIKELY(Expr) ? void(0)            \
                : Handle::handle(CUR_SOURCE_LOCATION, #Expr, fmt::format("" __VA_ARGS__)))

#define DO_ERROR_MSG(Handle, ...) Handle::handle(CUR_SOURCE_LOCATION, fmt::format("" __VA_ARGS__))


// MSVC variadic macro fix https://stackoverflow.com/questions/5134523
#define EXPAND(x) x

#ifdef __GNUC__
#define LIKELY(Expr) __builtin_expect(static_cast<bool>(Expr), true)
#else
#define LIKELY(Expr) static_cast<bool>(Expr)
#endif