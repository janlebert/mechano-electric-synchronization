#pragma once

#include <algorithm>

#include "INIReader.h"

#include "vectors.h"

class ParameterHandler {
 private:
  string current_section = "";
  std::unique_ptr<INIReader> reader;

  std::shared_ptr<std::ostream> _logstream;

  template <typename T>
  void logparam(const string &key, const T &r, const T &default_value, string comment = "");

 protected:
  void init(string str, string logfilename = "", bool load_as_file = true);

  void enter_section(string section);

  void leave_section();

  string get(string key, string default_value, string comment = "");

  long get_integer(string key, long default_value, string comment = "");

  num get_real(string key, double default_value, string comment = "");

  bool get_boolean(string key, bool default_value, string comment = "");

  // floating-point 2D vector, e.g. "(1.1, 2.2)"
  Vec2 get_Vec2(string key, Vec2 default_value, string comment = "");

  // floating-point 3D vector, e.g. "(1.1, 2.2, 3.3)"
  Vec3 get_Vec3(string key, Vec3 default_value, string comment = "");

  // integer 3D vector, e.g. "(1, 2, 3)"
  IdxVec get_IdxVec(string key, IdxVec default_value, string comment = "");

 public:
  ParameterHandler() : _logstream(&std::cout, [](std::ostream *) {}){};

  std::ostream &logstream() const;

  std::shared_ptr<std::ostream> logstream_ptr() const;

  void set_logstream(std::shared_ptr<std::ostream> logstream);

  void turn_off_logging();

  void turn_on_logging(std::shared_ptr<std::ostream> logstream = {});
};