#pragma once

#include <algorithm>
#include <atomic>
#include <thread>
#include <mutex>
#include <sstream>

#include "cpp11-on-multicore/sema.h"
#include "cpp11-on-multicore/autoresetevent.h"
#include "cpp11-on-multicore/rwlock.h"
#include "num.h"
#include "helper.h"

inline unsigned get_thread_count() {  // number of threads to be created for TimeStep
  unsigned thread_count = std::thread::hardware_concurrency();

  // overwrite thread count using this environment variable, also used by OGE/SGE
  char const *nslots = std::getenv("NSLOTS");

  if (nslots != nullptr) {
    std::stringstream(nslots) >> thread_count;
  }

  // sanity check
  ASSERT_ALWAYS(thread_count > 0 && thread_count < 64, "Thread count {} is not valid!", thread_count);

  return thread_count;
}

template <typename T>
using Iterator = typename T::iterator;

template <typename T>
using Chunk = typename std::pair<Iterator<std::vector<T>>, Iterator<std::vector<T>>>;

template <typename T>
using ChunkVec = typename std::vector<Chunk<T>>;

// Chunk a container into equal sized pieces (except for the last one)
template <typename T>
ChunkVec<T> make_chunks(std::vector<T> &container, const std::ptrdiff_t n) {
  ASSERT_ALWAYS(n > 0, "n hast to be larger than 0 in make_chunks!");

  using diff_t = std::ptrdiff_t;
  using It     = Iterator<std::vector<T>>;

  const It range_from = std::begin(container);
  const It range_to   = std::end(container);

  const diff_t total   = std::distance(range_from, range_to);
  const diff_t portion = total / n;

  ChunkVec<T> chunks(n);

  It portion_end = range_from;

  std::generate(begin(chunks), end(chunks), [&portion_end, portion]() {
    It portion_start = portion_end;

    portion_end += portion;
    return std::make_pair(portion_start, portion_end);
  });

  chunks.back().second = range_to;

  return chunks;
}

// Spinning thread barrier to synchronize threads
class ThreadBarrier {
  const unsigned count;
  std::atomic<unsigned> spaces;
  std::atomic<std::size_t> generation;

 public:
  explicit ThreadBarrier(unsigned count_) : count(count_), spaces(count), generation(0) {}

  void wait() {
    // __sync_synchronize();  // Prevent compiler and memory reordering, should not be necessary

    const std::size_t my_generation = generation;

    if (!--spaces) {
      spaces = count;
      ++generation;
    } else {
      while (generation == my_generation)
        ;
      //std::this_thread::yield();
    }
  }
};

// Spinlock wich is usable with std::lock_guard, keeps threads spinng (inefficient way to wait in terms of
// power consumption and can have thread starvation on HT processors; but is very fast compared to mutex)
class SpinLock {
  std::atomic_flag flag = ATOMIC_FLAG_INIT;

 public:
  void lock() noexcept {
    while (flag.test_and_set(std::memory_order_acquire))
      ;
  }
  void unlock() noexcept { flag.clear(std::memory_order_release); }
  bool try_lock() noexcept { return !flag.test_and_set(std::memory_order_acquire); }
};