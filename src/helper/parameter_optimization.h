#pragma once

#include "num.h"

namespace ParameterOptimization {
  namespace detail {
    template <typename Func, std::size_t N, std::size_t... index>
    decltype(auto) invoke(Func&& func, const array<double, N>& args, std::index_sequence<index...>) {
      /*
       * Helper function to invoke arbitrary function by unpacking an array of parameter as arguments
       */
      return func(args[index]...);
    }
  }  // namespace detail

  /*
   * The resulting pack of parameters and the minimum value for which `f` has its global minimum
   */
  using Result = std::pair<vector<double>, double>;

  template <std::size_t N, typename Func>
  Result find_min_grid(Func f,
                       const array<double, N>& lower_bounds,
                       const array<double, N>& upper_bounds,
                       const array<bool, N>& is_integer_variable,
                       size_t maxcalls) {
    /*
     * Does a grid search to find the global minimum of f(x_0, x_1, x_2, ...) by calling `f` a
     * maximum of `max_calls` times. The lower resp. higher bounds determine the rage for the resp.
     * function parameter x_i. `is_integer_variable` specifies if x_i is inter or double typed.
     */

    array<double, N> steps;

    auto sqrtn = [](double x, double n) { return std::pow(x, 1. / n); };

    const double its_per_var = sqrtn(maxcalls, N);

    for (int i = 0; i < lower_bounds.size(); i++) {
      num stepsize = (upper_bounds[i] - lower_bounds[i]) / (its_per_var - 1);
      if (is_integer_variable[i]) {
        stepsize = std::round(stepsize);
      }

      steps[i] = stepsize;
    }

    auto iterate = [&lower_bounds, &upper_bounds, &steps](array<double, N>& x) {
      for (size_t i = 0; i < N; i++) {
        if (i > 0) {
          x[i - 1] = lower_bounds[i - 1];
        }

        if (x[i] + steps[i] <= upper_bounds[i] + 0.1 * steps[i]) {
          x[i] = clamp(x[i] + steps[i], lower_bounds[i], upper_bounds[i]);
          break;
        }
      }
    };

    array<double, N> x = lower_bounds;
    array<double, N> argmin;
    double min = std::numeric_limits<double>::max();

    for (size_t i = 0; i < maxcalls; i++) {
      const double r = detail::invoke(f, x, std::make_index_sequence<N>{});

      if (r < min) {
        min    = r;
        argmin = x;
      }

      iterate(x);
    }

    return {vector<double>(argmin.begin(), argmin.end()), min};
  }
}  // namespace ParameterOptimization