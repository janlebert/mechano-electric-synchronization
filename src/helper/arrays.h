#pragma once

#include <iostream>
#include <algorithm>

#include "helper/vectors.h"
#include "helper/iterators.h"
#include "helper/io.h"

/*
 * Column-major order multidimensional array classes
 */

// Base class, common functionality for the specific dimensional specializations
template <typename T, unsigned N>
class ArrayN {
 protected:
  T *_data             = nullptr;
  size_t _size;

  // The array may act as a view on a data pointer, in this case we don't deallocate the data on destruction.
  // This is unsafe, because the owner may deallocate the data we have a view on.
  // This is most frequently used with operator[] and Set(), should be rewritten to internally use a shared_ptr.
  bool _data_allocated = false;

  explicit ArrayN(size_t m_size, T *v = nullptr) {
    _size = m_size;
    if (v != nullptr) { // act as a view on the provided data pointer
      _data = v;
    } else {
      _data_allocated = true;
      _data           = new T[_size];
      ASSERT_ALWAYS(_data != nullptr, "allocation of Array failed!");

      std::fill(begin(), end(), T{});
    }
  }

 public:
  ~ArrayN() {
    if (_data_allocated) {
      delete[] _data;
    }
  }

  // Copy swap idiom
  friend void swap(ArrayN<T, N> &first, ArrayN<T, N> &second) {
    using std::swap;

    swap(first._data, second._data);
    swap(first._data_allocated, second._data_allocated);
    swap(first._size, second._size);
  }

  size_t size() const { return _size; }

  T &operator()(size_t i) {
    ASSERT_DEBUG(i < _size);
    return _data[i];
  }

  T operator()(size_t i) const {
    ASSERT_DEBUG(i < _size);
    return _data[i];
  }

  operator T *() { return _data; }
  T *data() { return _data; };
  operator const T *() const { return _data; }
  const T *data() const { return _data; }

  // Set everything to one value
  void Load(T v) { std::fill(begin(), end(), v); }

  // Copy data
  void Load(T *v) {
    ASSERT_ALWAYS(v != nullptr);
    std::copy(iterator(v), iterator(v + this->size()), begin());
  }

  // Copy data
  void Load(const T *v) {
    ASSERT_ALWAYS(v != nullptr);
    std::copy(const_iterator(v), const_iterator(v + this->size()), begin());
  }

  // Act as a view on a data pointer
  // WARNING: does not own the data, may lead to segfault if owner deletes v
  void Set(T *v) {
    if (v != nullptr) {
      if (_data_allocated && _data != nullptr) {
        delete[] _data;
      }

      _data           = v;
      _data_allocated = false;
    }
  }

  bool operator==(const ArrayN<T, N> &o) const { return std::equal(cbegin(), cend(), o.cbegin()); }
  bool operator!=(const ArrayN<T, N> &o) const { return !operator==(o); }
  void operator*=(const T &v) {
    for (iterator it = begin(); it < end(); ++it) {
      *it *= v;
    }
  }
  void operator/=(const T &v) {
    for (iterator it = begin(); it < end(); ++it) {
      *it /= v;
    }
  }

  // Append array to file
  void save(const string &filepath) const {
    OutStream binaryfile(filepath);
    save(binaryfile);
  }

  void save(OutStream &out) const { out.write(this->data(), this->size()); }
  void save(std::ofstream &out) const { io::writeToFile(out, this->data(), this->size()); }

  // Load array from file
  void load(const string &filepath, long t = 0) {
    InStream<T> in(filepath);
    load(in, t);
  }

  void load(InStream<T> &in, long t = 0) {
    ASSERT_ALWAYS(t >= 0);
    ASSERT_ALWAYS(t % in.getSkip() == 0, "t = {}; in.getSkip() = {}", t, in.getSkip());

    t /= in.getSkip();
    in.read(this->data(), t * this->size(), this->size());
  }

  void load(std::ifstream &in, long t = 0) {
    ASSERT_ALWAYS(t >= 0);
    io::readFromFile(in, this->data(), t * this->size(), this->size());
  }


  // Iterators
  using value_type     = T;
  using iterator       = RawIterator<T>;
  using const_iterator = ConstRawIterator<const T>;
  iterator begin() { return iterator(_data); }
  iterator end() { return iterator(_data + _size); }
  const_iterator begin() const { return cbegin(); }
  const_iterator end() const { return cend(); }
  const_iterator cbegin() const { return const_iterator(_data); }
  const_iterator cend() const { return const_iterator(_data + _size); }
};




// 1D implementation for convenience to provide the same interface as the latter implementations
template <typename T>
class Array1 : public ArrayN<T, 1> {
  int position(const int &x) const {
    ASSERT_DEBUG((x >= 0) && (x < this->size()));
    return x;
  }

 public:
  Array1(size_t nx, T *v = nullptr) : ArrayN<T, 1>(nx, v){};

  Array1(const Array1<T> &other) : Array1(other.size()) { this->Load(other.data()); }

  template <typename D>
  Array1(const Array1<D> &other) : Array1(other.size()) {
    for (size_t i = 0; i < this->size(); i++) {
      this->_data[i] = static_cast<T>(other[i]);
    }
  }


  using ArrayN<T, 1>::operator();
  using ArrayN<T, 1>::operator==;
  using ArrayN<T, 1>::operator!=;
  using ArrayN<T, 1>::operator*=;
  using ArrayN<T, 1>::operator/=;

  T &operator[](const size_t &i) { return ArrayN<T, 1>::operator()(i); };
  T operator[](const size_t &i) const { return ArrayN<T, 1>::operator()(i); };

  size_t Nx() const { return this->size(); }
};

// 2D implementation
template <typename T>
class Array2 : public ArrayN<T, 2> {
 private:
  const Vector2<int> _dims;
  const Vector2<int> _dotdims;

  int position(const Vector2<int> &pos) const {
    ASSERT_DEBUG((pos[0] >= 0) && (pos[0] < _dims[0]) && (pos[1] >= 0) && (pos[1] < _dims[1]));
    return pos[0] + pos[1] * _dims[0];
  }

 public:
  Array2(Vector2<int> dims, T *v = nullptr) : Array2(dims[0], dims[1], v) {}

  Array2(int nx, int ny, T *v = nullptr)
      : ArrayN<T, 2>(static_cast<size_t>(nx) * ny, v), _dims({nx, ny}), _dotdims({1, nx}) {}

  Array2(Array2<T> &&other) : Array2(other.dimensions(), other.data()) {
    std::swap(this->_data_allocated, other._data_allocated);
  }

  Array2(const Array2<T> &other) : Array2(other.dimensions()) {
    ASSERT_ALWAYS(_dims == other._dims);
    this->Load(other.data());
  }

  Array2<T> &operator=(const Array2<T> &other) {
    ASSERT_ALWAYS(_dims == other._dims);
    this->Load(other.data());
    return *this;
  }

  using ArrayN<T, 2>::operator();
  using ArrayN<T, 2>::operator==;
  using ArrayN<T, 2>::operator!=;
  using ArrayN<T, 2>::operator*=;
  using ArrayN<T, 2>::operator/=;

  T &operator()(const Vector2<int> &pos) { return this->_data[position(pos)]; };
  T &operator()(int x, int y) { return operator()({x, y}); };
  T operator()(const Vector2<int> &pos) const { return this->_data[position(pos)]; };
  T operator()(int x, int y) const { return operator()({x, y}); };

  Array1<T> operator[](int y) {
    ASSERT_DEBUG(y >= 0 && y < Ny());
    return {static_cast<size_t>(_dims[0]), this->data() + y * _dims[0]};
  }

  Vector2<int> dimensions() const { return _dims; }

  int Nx() const { return _dims[0]; }
  int Ny() const { return _dims[1]; }
};

// 3D implementation
template <typename T>
class Array3 : public ArrayN<T, 3> {
 private:
  const Vector3<int> _dims;
  const Vector3<int> _dotdims;

  int position(const IdxVec &pos) const {
    ASSERT_DEBUG((pos[0] >= 0) && (pos[0] < _dims[0]) && (pos[1] >= 0) && (pos[1] < _dims[1]) &&
                 (pos[2] >= 0) && (pos[2] < _dims[2]));
    auto r = pos.dot(_dotdims);
    ASSERT_DEBUG(r < this->_size);
    return r;
  }

 public:
  Array3(Vector3<int> dims, T *v = nullptr) : Array3(dims[0], dims[1], dims[2], v) {}

  Array3(int nx, int ny, int nz, T *v = nullptr)
      : ArrayN<T, 3>(static_cast<size_t>(nx) * ny * nz, v),
        _dims({nx, ny, nz}),
        _dotdims({1, nx, nx * ny}),
        positions({nx, ny, nz}) {}

  Array3(Array3<T> &&other) : Array3(other.dimensions(), other.data()) {
    std::swap(this->_data_allocated, other._data_allocated);
  }

  Array3(const Array3<T> &other) : Array3(other.dimensions()) {
    ASSERT_ALWAYS(_dims == other._dims);
    this->Load(other.data());
  }

  Array3<T> &operator=(const Array3<T> &other) {
    ASSERT_ALWAYS(_dims == other._dims);
    this->Load(other.data());
    return *this;
  }

  using ArrayN<T, 3>::operator();
  using ArrayN<T, 3>::operator==;
  using ArrayN<T, 3>::operator!=;
  using ArrayN<T, 3>::operator*=;
  using ArrayN<T, 3>::operator/=;

  T &operator()(const IdxVec &pos) { return this->_data[position(pos)]; };
  T &operator()(int x, int y, int z) { return operator()({x, y, z}); };
  T operator()(const IdxVec &pos) const { return this->_data[position(pos)]; };
  T operator()(int x, int y, int z) const { return operator()({x, y, z}); };

  Array2<T> operator[](int z) {
    ASSERT_DEBUG(z >= 0 && z < Nz());
    return {_dims[0], _dims[1], this->data() + z * _dims[0] * _dims[1]};
  }

  Vector3<int> dimensions() const { return _dims; }

  int Nx() const { return _dims[0]; }
  int Ny() const { return _dims[1]; }
  int Nz() const { return _dims[2]; }

  using ArrayN<T, 3>::load;

  // Load one z slice into memory
  void load(InStream<T> &in, long t, int z_slice) {
    ASSERT_DEBUG(z_slice >= 0);

    t /= in.getSkip();

    const auto nxy = Nx() * Ny();
    in.read(this->data() + z_slice * nxy, t * this->size() + z_slice * nxy, nxy);
  }

  // Iterators
  Positions positions;
};

// 4D implementation
template <typename T>
class Array4 : public ArrayN<T, 4> {
 private:
  const Vector4<int> _dims;
  const Vector4<long> _dotdims;

  long position(const Vector4<int> &pos) const {
    ASSERT_DEBUG((pos[0] >= 0) && (pos[0] < _dims[0]) && (pos[1] >= 0) && (pos[1] < _dims[1]) &&
                 (pos[2] >= 0) && (pos[2] < _dims[2]) && (pos[3] >= 0) && (pos[3] < _dims[3]),
                 "pos {}, dims {}", pos, _dims);

    auto r = _dotdims.inner_product(pos);
    ASSERT_DEBUG(r < this->_size);
    return r;
  }

 public:
  Array4(Vector3<int> dims, int t, T *v = nullptr) : Array4(dims[0], dims[1], dims[2], t, v) {}

  Array4(Vector4<int> dims, T *v = nullptr) : Array4(dims[0], dims[1], dims[2], dims[3], v) {}

  Array4(int nx, int ny, int nz, int nt, T *v = nullptr)
      : ArrayN<T, 4>(static_cast<size_t>(nx) * ny * nz * nt, v),
        _dims({nx, ny, nz, nt}),
        _dotdims({1, nx, nx * ny, nx * ny * nz}),
        positions({nx, ny, nz}) {}

  Array4(Array4<T> &&other) : Array4(other.dimensions(), other.data()) {
    std::swap(this->_data_allocated, other._data_allocated);
  }

  Array4(const Array4<T> &other) : Array4(other.dimensions()) {
    ASSERT_ALWAYS(_dims == other._dims);
    this->Load(other.data());
  }

  Array4<T> &operator=(const Array4<T> &other) {
    ASSERT_ALWAYS(_dims == other._dims);
    this->Load(other.data());
    return *this;
  }

  using ArrayN<T, 4>::operator();
  using ArrayN<T, 4>::operator==;
  using ArrayN<T, 4>::operator!=;
  using ArrayN<T, 4>::operator*=;
  using ArrayN<T, 4>::operator/=;

  T &operator()(const Vector4<int> &pos) { return this->_data[position(pos)]; };
  T &operator()(const IdxVec &pos, int t) {
    return this->_data[position({pos[0], pos[1], pos[2], t})];
  };
  T &operator()(int x, int y, int z, int t) { return this->_data[position({x, y, z, t})]; };
  T operator()(const Vector4<int> &pos) const { return this->_data[position(pos)]; };
  T operator()(const IdxVec &pos, int t) const {
    return this->_data[position({pos[0], pos[1], pos[2], t})];
  };
  T operator()(int x, int y, int z, int t) const { return this->_data[position({x, y, z, t})]; };

  Array3<T> operator[](int t) {
    ASSERT_DEBUG(t >= 0 && t < Nt());
    return {_dims[0], _dims[1], _dims[2], this->data() + t * _dims[0] * _dims[1] * _dims[2]};
  }

  Vector4<int> dimensions() const { return _dims; }

  int Nx() const { return _dims[0]; }
  int Ny() const { return _dims[1]; }
  int Nz() const { return _dims[2]; }
  int Nt() const { return _dims[3]; }

  // Iterators
  Positions positions;
  using iterator          = RawIterator<T>;
  using timeTraceIterator = StrideIterator<iterator>;

  using ArrayN<T, 4>::begin;
  using ArrayN<T, 4>::end;

  // Iterators over time for one specific (3D) element
  timeTraceIterator begin(const IdxVec &pos) { return end(pos, 0); };
  timeTraceIterator end(const IdxVec &pos) { return end(pos, this->Nt()); }
  timeTraceIterator end(const IdxVec &pos, int t_end) {
    ASSERT_DEBUG(t_end >= 0 && t_end <= Nt());

    const Vector4<int> p {pos[0], pos[1], pos[2], t_end};
    size_t n = 0;
    if (t_end < Nt()) {
      n = position(p);
    } else { // can't use position() as it would throw out of bounds error
      n = _dotdims.inner_product(p);
    }
    const iterator it {this->data() + n};
    return {it, _dotdims[3]};
  };
};




// Forward declaration of friend functions
template <typename T>
class CircularArray;

template <typename T>
std::ostream &operator<<(std::ostream &out, const CircularArray<T> &v);

template <typename T>
class CircularArray {
 public:
  // Rule of five, constructors and copy/move operators

  CircularArray(Vector3<int> dims, int window, long buffer_multiplier = 10)
      : CircularArray(dims[0], dims[1], dims[2], window, buffer_multiplier) {}

  CircularArray(Vector4<int> dims, long buffer_multiplier = 10)
      : CircularArray(dims[0], dims[1], dims[2], dims[3], buffer_multiplier) {}

  CircularArray(int x, int y, int z, int window, long buffer_multiplier = 10);

  ~CircularArray();

  CircularArray(const CircularArray<T> &other);

  CircularArray(CircularArray<T> &&other) noexcept;

  CircularArray<T> &operator=(CircularArray<T> other);

  // Access operators
  T &operator()(IdxVec pos, long t) { return get(pos, t); }

  T &operator()(int x, int y, int z, long t) { return get({x, y, z}, t); }

  T operator()(IdxVec pos, long t) const { return get_const(pos, t); }

  T operator()(int x, int y, int z, long t) const { return get_const({x, y, z}, t); }

  Array3<T> operator[](long t);

  const Array3<T> operator[](long t) const;

  bool operator==(CircularArray<T> &a);

  bool operator!=(CircularArray<T> &a) { return !(operator==(a)); }

  T *data() const { return _head; }
  T *data(long t) const;

  friend std::ostream &operator<<<>(std::ostream &out, const CircularArray<T> &a);

  // Size and dimensions funcs
  int Nx() const { return _dims[0]; }

  int Ny() const { return _dims[1]; }

  int Nz() const { return _dims[2]; }

  int Nt() const { return _window; }

  Vector4<int> dimensions() const;

  long size() const;

  long buffer_multiplier() const { return _buffer_multiplier; }

  // Array stores data in the timeframe [t_head, t_end]
  long get_t_head() const { return _t_head; }
  long get_t_end() const { return _t_end - 1; }
  void set_t_end(long t_end) { get({0, 0, 0}, t_end); }

  void resize(int window);

  void reset() { reset(_window); }

  // Fill array with value and set default initialization value to it
  void fill(T x);

  // Return a vector containing the timetrace of a pixel, last time point to include is t_end
  Array1<T> timeTrace(IdxVec pos, long t_end) const;

  // A vector of size window following the pixel at position pos over time
  Array1<T> timeTrace(IdxVec pos) { return timeTrace(pos, _t_end - 1); }

  void loadTimeTrace(IdxVec pos, Array1<T> &trace) { loadTimeTrace(pos, _t_end - 1, trace); };
  void loadTimeTrace(IdxVec pos, long t_end, Array1<T> &trace);

  // Iterators
  using value_type        = T;
  using iterator          = RawIterator<T>;
  using const_iterator    = ConstRawIterator<const T>;
  using timeTraceIterator = StrideIterator<iterator>;

  iterator begin() { return iterator(data()); };
  iterator end() { return iterator(_end); };
  const_iterator begin() const { return cbegin(); }
  const_iterator end() const { return cend(); }
  const_iterator cbegin() const { return const_iterator(_data); }
  const_iterator cend() const { return const_iterator(_end); }

  iterator begin(long t) { return iterator(data(t)); };
  iterator end(long t);

  timeTraceIterator begin(const IdxVec &pos);
  timeTraceIterator end(const IdxVec &pos) { return end(pos, _t_end - 1); }
  timeTraceIterator end(const IdxVec &pos, long t_end);

  Positions positions;

  // Copy swap idiom
  friend void swap(CircularArray<T> &first, CircularArray<T> &second) {
    using std::swap;

    swap(first._data, second._data);
    swap(first._head, second._head);
    swap(first._end, second._end);

    swap(first._buffer_multiplier, second._buffer_multiplier);
    swap(first._init_value, second._init_value);
    swap(first._dims, second._dims);
    swap(first._dotdims, second._dotdims);
    swap(first._window, second._window);
    swap(first._array_length, second._array_length);
    swap(first._inner_length, second._inner_length);
    swap(first._total_length, second._total_length);
    swap(first._t_head, second._t_head);
    swap(first._t_end, second._t_end);
    swap(first._t_maximum, second._t_maximum);
  }

 private:
  long _buffer_multiplier;  // scaling factor how much memory to allocation on construction

  T *_data = nullptr;
  T *_head = nullptr;
  T *_end  = nullptr;

  T _init_value{};

  IdxVec _dims;
  IdxVec _dotdims;
  int _window;

  long _array_length;
  long _inner_length;
  long _total_length;

  long _t_head;
  long _t_end;
  long _t_maximum;

  long position(IdxVec pos, long t) const;

  T &get(IdxVec pos, long t);

  T &get_const(IdxVec pos, long t) const;

  void sanityCheckT(long t) const;

  void sanityCheckPointer(T *p) const;

  void alloc_data(int x, int y, int z, int window);

  void reset(long t_end);

  void updatePointersAndCounters(T *head, long t_end);

  void move_arrays(long t_end);
};

template <typename T>
std::ostream &operator<<(std::ostream &out, CircularArray<T> &a) {
  for (long t = a.get_t_head(); t <= a.get_t_end(); t++) {
    out << "\n";
    for (auto it = a.begin(t); it < a.end(t); it++) {
      out << *it << " ";
    }
  }
  out << "\n";
  return out;
}