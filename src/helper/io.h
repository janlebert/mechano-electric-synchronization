#pragma once

#include "vectors.h"

/*
 * File helper functions not dependent on parameter.h
 */

namespace io {

  inline bool fileExists(const string &filename) {
    std::ifstream infile(filename);
    return infile.good();
  }

  inline void deleteFile(const string &filename) { std::remove(filename.c_str()); }

  inline void deleteFiles(const vector<string> &filenames) {
    for (auto &s : filenames) {
      deleteFile(s);
    }
  }

  inline long getFileSize(std::ifstream &in) {
    in.seekg(0, std::ios::end);
    auto pos_end = in.tellg();
    in.seekg(0, std::ios::beg);
    auto pos_begin = in.tellg();

    return pos_end - pos_begin;
  }

  template <typename T>
  T readFromFile(std::ifstream &in, std::size_t position) {
    T tmp;
    in.seekg(position * sizeof(T), std::ios::beg);
    in.read(reinterpret_cast<char *>(&tmp), sizeof(T));
    return tmp;
  }

  template <typename T>
  void readFromFile(std::ifstream &in, T *out, std::size_t start, std::size_t size) {
    in.seekg(start * sizeof(T), std::ios::beg);
    in.read(reinterpret_cast<char *>(out), size * sizeof(T));
  }

  template <typename T>
  void writeToFile(std::ofstream &out, const T &val) {
    out.write(reinterpret_cast<const char *>(&val), sizeof(val));
  }

  template <typename T>
  void writeToFile(std::ofstream &out, const T *data, std::size_t size) {
    out.write(reinterpret_cast<const char *>(data), size * sizeof(T));
  }
}  // namespace io


/**
 * Basic input filestream class, customized for our internal memery layout and common needs
 */
template <typename T>
class InStream {
 private:
  string filepath;
  IdxVec dotdims;
  int skip_factor = 1;
  size_t nxyz     = 1;

  void openFileIfExists(const string &filepath, char *buffer = nullptr, std::size_t N = 0) {
    ASSERT_ALWAYS(io::fileExists(filepath), "File {} not found!", filepath);

    in.close();
    if (buffer != nullptr) {
      in.rdbuf()->pubsetbuf(buffer, N);
    }
    in.open(filepath, std::ios::in | std::ios::binary);
    in.exceptions(std::ifstream::eofbit | std::ifstream::failbit | std::ifstream::badbit);
  }

 public:
  std::ifstream in;

  InStream(string filepath, IdxVec dims = {1, 1, 1}, int tw = 1)
      : filepath(filepath), dotdims({1, dims[0], dims[0] * dims[1]}), skip_factor(tw) {
    nxyz = static_cast<size_t>(dims[0]) * dims[1] * dims[2];

    if (!filepath.empty()) {
      openFileIfExists(filepath);
    }
  }

  string getFilePath() { return filepath; }
  void setFilePath(const string &fp) {
    filepath = fp;
    openFileIfExists(filepath);
  }

  void setSkip(int tw) { skip_factor = tw; }
  int getSkip() { return skip_factor; }

  bool good() { return in.good(); }

  void clear() {
    in.clear();

    if (in.good()) {
      in.seekg(0, ios::beg);
    }
  }

  T read(size_t position) { return io::readFromFile<T>(in, position); }

  void read(T *out, size_t start, size_t size) { return io::readFromFile<T>(in, out, start, size); }

  T read(IdxVec pos, long t) {
    ASSERT_DEBUG(dotdims != IdxVec(1, 1, 1));
    ASSERT_DEBUG(skip_factor > 0);

    auto p = pos.dot(dotdims) + (t / skip_factor) * nxyz;
    return read(p);
  }

  T read(int x, int y, int z, long t) { return read({x, y, z}, t); }

  long maxNt() {
    ASSERT_ALWAYS(dotdims != IdxVec(1, 1, 1));
    ASSERT_ALWAYS(skip_factor > 0);

    auto fsize = io::getFileSize(in);

    long Nt   = fsize / (sizeof(T) * nxyz);
    auto rest = fsize % (sizeof(T) * nxyz);

    CHECK_ALWAYS(rest == 0, "filesize of {} is not an multiple of {}! rest = {}", filepath, nxyz,
                 rest);

    return skip_factor * Nt;
  }
};


/**
 * Output IO datastream class, complement of InStream
 */
class OutStream {
 public:
  std::ofstream out;

  explicit OutStream(const string &filename, bool delete_existing = false) {
    if (delete_existing) io::deleteFile(filename);
    out.open(filename, ios::out | ios::binary | ios::app);
  }

  template <typename T>
  void write(const T &val) {
    io::writeToFile(out, val);
  }

  template <typename T>
  void write(const T *data, size_t size) {
    io::writeToFile(out, data, size);
  }
};


/**
 * Most often, we require a InStream<Vec3> file (position), a InStream<num>
 * (excitatory variable) and perhaps a second InStream<num> (deformation rate)
 */
class SimulationDataStreams {
 private:
  string common_prefix(string a, string b) const {
    if (a.size() > b.size()) std::swap(a, b);
    return std::string(a.begin(), std::mismatch(a.begin(), a.end(), b.begin()).first);
  }

  string _prefix = "";

 public:
  InStream<Vec3> pos;
  InStream<num> u;
  InStream<num> E;
  InStream<array<Vec3, 3>> fiberorientation;

  SimulationDataStreams(const string &filename_u,
                        const string &filename_pos,
                        IdxVec dims       = {1, 1, 1},
                        int tw            = 1,
                        string filename_e = "")
      : pos(filename_pos, dims, tw),
        u(filename_u, dims, tw),
        E(filename_e, dims, tw),
        fiberorientation("", dims) {

    // Find prefix
    _prefix = common_prefix(filename_u, filename_pos);
    if ((_prefix.length() > 1) && (_prefix.substr(_prefix.length() - 1) == "_"s)) {
      _prefix = _prefix.substr(0, _prefix.length() - 1);
    } else {
      _prefix = ""s;
    }

    string fibers_fn = _prefix + "_fiberorientation.dat";
    if (io::fileExists(fibers_fn)) {
      fiberorientation.setFilePath(fibers_fn);
    }
  }

  string prefix() const { return _prefix; }

  void clearEOF() {
    pos.clear();
    u.clear();
    E.clear();
  }

  void setSkip(int tw) {
    pos.setSkip(tw);
    u.setSkip(tw);
    E.setSkip(tw);
  }

  long maxNt() { return pos.maxNt(); }
};