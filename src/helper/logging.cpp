#include "logging.h"

#include "fmt/time.h"
#include "fmt/ostream.h"

#ifdef BACKWARD_ENABLED
#include "backward-cpp/backward.hpp"
#endif

namespace {
#ifdef BACKWARD_ENABLED
  void print_stacktrace() {
    using namespace backward;
    StackTrace st;
    st.load_here(32);

    TraceResolver tr;
    tr.load_stacktrace(st);

    std::vector<ResolvedTrace> traces;
    traces.reserve(st.size());

    size_t trace_start_idx = 0;

    for (size_t i = 0; i < st.size(); ++i) {
      traces.emplace_back(tr.resolve(st[i]));

      auto fct = traces.back().object_function;

      if (fct.rfind("ErrorHandling::", 0) == 0) {
        trace_start_idx = i + 1;
      }
    }

    fmt::print(std::cerr, "Stack trace (most recent call last):\n");
    for (size_t i = traces.size() - 1; i >= trace_start_idx; i--) {
      fmt::print(std::cerr, "#{} {:<80} {}\n", i - trace_start_idx + 1, traces[i].object_filename,
                 traces[i].object_function);
    }
  }
#else
  void print_stacktrace() {}
#endif
}  // namespace




void log_msg(std::ostream &out, long t, const string &msg) {

  std::time_t c_time = std::time(nullptr);
  std::tm tm         = *std::localtime(&c_time);

  if (t >= 0) {
    const int padding = 60 - static_cast<int>(std::log10(t + 1));
    fmt::print(out, "; {:%Y-%m-%d %H:%M:%S} — t = {}   {:>{}}\n", tm, t, msg, padding);
  } else {
    const int padding = 60;
    fmt::print(out, "; {:%Y-%m-%d %H:%M:%S}            {:>{}}\n", tm, msg, padding);
  }
}

namespace ErrorHandling {
  [[noreturn]] void assertAbortHandler::handle(const source_location &loc,
                                               const char *expression,
                                               const string &msg) noexcept {

    if (!msg.empty()) {
      fmt::print(std::cerr, "\nERROR: Assertion \"{}\" ({}) failed in {}:{}\n", expression, msg,
                 loc.file_name, loc.line_number);
    } else {
      fmt::print(std::cerr, "\nERROR: Assertion \"{}\" failed in {}:{}\n", expression, loc.file_name,
                 loc.line_number);
    }

    print_stacktrace();

    std::cout.flush();
    std::cerr.flush();

    std::abort();
  }

  void assertWarningHandler::handle(const source_location &loc,
                                    const char *expression,
                                    const string &msg) noexcept {

    string fmt;
    if (!msg.empty()) {
      fmt = fmt::format("WARNING: Assertion \"{}\" ({}) failed in {}:{}\n", expression, msg,
                        loc.file_name, loc.line_number);
    } else {
      fmt = fmt::format("WARNING: Assertion \"{}\" failed in {}:{}\n", expression, loc.file_name,
                        loc.line_number);
    }

    log_msg(std::cerr, -1, fmt);

    print_stacktrace();
  }


  [[noreturn]] void messageAbortHandler::handle(const source_location &loc,
                                                const string &msg) noexcept {

    fmt::print(std::cerr, "\nERROR: \"{}\" from {}:{}\n", msg, loc.file_name, loc.line_number);

    print_stacktrace();

    std::cout.flush();
    std::cerr.flush();

    std::abort();
  }


  void messageWarningHandler::handle(const source_location &loc, const string &msg) noexcept {
    string fmt = fmt::format("WARNING: \"{}\" from {}:{}\n", msg, loc.file_name, loc.line_number);

    print_stacktrace();

    log_msg(std::cerr, -1, fmt);
  }

}  // namespace ErrorHandling
