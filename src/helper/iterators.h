#pragma once

#include <iterator>
#include <cassert>
#include <cstddef>

#include "vectors.h"

using std::ptrdiff_t;

// Random-access iterator wrapping any raw data, to be used for custom container classes
template <typename T>
class RawIterator {
 public:
  // public typedefs for STL algorithms
  using value_type        = T;
  using reference         = T&;
  using difference_type   = ptrdiff_t;
  using pointer           = T*;
  using iterator_category = std::random_access_iterator_tag;
  using self              = RawIterator<T>;

  // constructors
  explicit RawIterator(pointer ptr = nullptr) : m_ptr(ptr) {}

  RawIterator(const self& rawIterator) = default;

  ~RawIterator() = default;

  // operators
  self& operator=(const self& rawIterator) = default;

  self& operator=(pointer ptr) {
    m_ptr = ptr;
    return *this;
  }

  explicit operator bool() const { return m_ptr != nullptr; }

  self& operator++() {
    ++m_ptr;
    return *this;
  }
  self operator++(int) {
    self tmp = *this;
    ++m_ptr;
    return tmp;
  }
  self& operator+=(const ptrdiff_t& x) {
    m_ptr += x;
    return *this;
  }
  self& operator--() {
    --m_ptr;
    return *this;
  }
  self operator--(int) {
    self tmp = *this;
    --m_ptr;
    return tmp;
  }
  self& operator-=(const ptrdiff_t& x) {
    m_ptr -= x;
    return *this;
  }

  reference operator[](const difference_type n) { return m_ptr[n]; }
  reference operator*() const { return *m_ptr; }
  pointer operator->() { return m_ptr; }

  // friend operators
  friend bool operator==(const self& x, const self& y) { return x.m_ptr == y.m_ptr; }
  friend bool operator!=(const self& x, const self& y) { return x.m_ptr != y.m_ptr; }

  friend bool operator<(const self& x, const self& y) { return x.m_ptr < y.m_ptr; }
  friend bool operator<=(const self& x, const self& y) { return x.m_ptr <= y.m_ptr; }
  friend bool operator>(const self& x, const self& y) { return x.m_ptr > y.m_ptr; }
  friend bool operator>=(const self& x, const self& y) { return x.m_ptr >= y.m_ptr; }

  friend difference_type operator-(const self& x, const self& y) {
    return std::distance(y.m_ptr, x.m_ptr);
  }

  friend self operator+(const self& x, difference_type y) { return self(x.m_ptr + y); }

  friend self operator+(difference_type x, const self& y) { return y + x; }

  friend self operator-(const self& x, difference_type y) { return self(x.m_ptr - y); }

 protected:
  T* m_ptr;
};

// Random-access iterator wrapping any raw data, to be used for custom container classes
template <typename T>
class ConstRawIterator {
 public:
  // public typedefs for STL algorithms
  using value_type        = T;
  using reference         = T&;
  using difference_type   = ptrdiff_t;
  using pointer           = T*;
  using iterator_category = std::random_access_iterator_tag;
  using self              = ConstRawIterator<T>;

  // constructors
  explicit ConstRawIterator(pointer ptr = nullptr) : m_ptr(ptr) {}

  ConstRawIterator(const self& rawIterator) = default;

  ~ConstRawIterator() = default;

  // operators
  self& operator=(const self& rawIterator) = default;

  self& operator=(pointer ptr) {
    m_ptr = ptr;
    return *this;
  }

  explicit operator bool() const { return m_ptr != nullptr; }

  self& operator++() {
    ++m_ptr;
    return *this;
  }
  self operator++(int) {
    self tmp = *this;
    ++m_ptr;
    return tmp;
  }
  self& operator+=(const ptrdiff_t& x) {
    m_ptr += x;
    return *this;
  }
  self& operator--() {
    --m_ptr;
    return *this;
  }
  self operator--(int) {
    self tmp = *this;
    --m_ptr;
    return tmp;
  }
  self& operator-=(const ptrdiff_t& x) {
    m_ptr -= x;
    return *this;
  }

  value_type operator[](const difference_type n) const { return m_ptr[n]; }
  value_type operator*() const { return *m_ptr; }
  pointer operator->() { return m_ptr; }

  // friend operators
  friend bool operator==(const self& x, const self& y) { return x.m_ptr == y.m_ptr; }
  friend bool operator!=(const self& x, const self& y) { return x.m_ptr != y.m_ptr; }

  friend bool operator<(const self& x, const self& y) { return x.m_ptr < y.m_ptr; }
  friend bool operator<=(const self& x, const self& y) { return x.m_ptr <= y.m_ptr; }
  friend bool operator>(const self& x, const self& y) { return x.m_ptr > y.m_ptr; }
  friend bool operator>=(const self& x, const self& y) { return x.m_ptr >= y.m_ptr; }

  friend difference_type operator-(const self& x, const self& y) {
    return std::distance(y.m_ptr, x.m_ptr);
  }

  friend self operator+(const self& x, difference_type y) { return self(x.m_ptr + y); }

  friend self operator+(difference_type x, const self& y) { return y + x; }

  friend self operator-(const self& x, difference_type y) { return self(x.m_ptr - y); }

 protected:
  T* m_ptr;
};

template <class itr_t>
class StrideIterator {
 public:
  // public typedefs for STL algorithms
  using value_type        = typename std::iterator_traits<itr_t>::value_type;
  using reference         = typename std::iterator_traits<itr_t>::reference;
  using difference_type   = typename std::iterator_traits<itr_t>::difference_type;
  using pointer           = typename std::iterator_traits<itr_t>::pointer;
  using iterator_category = std::random_access_iterator_tag;
  using self              = StrideIterator;

  // constructors
  StrideIterator() : m(nullptr), step(0){};
  StrideIterator(const self& x) : m(x.m), step(x.step) {}
  StrideIterator(itr_t x, difference_type n) : m(x), step(n) { assert(n != 0); }

  // operators
  self& operator++() {
    m += step;
    return *this;
  }
  self operator++(int) {
    self tmp = *this;
    m += step;
    return tmp;
  }
  self& operator+=(const difference_type x) {
    m += (x * step);
    return *this;
  }
  self& operator--() {
    m -= step;
    return *this;
  }
  self operator--(int) {
    self tmp = *this;
    m -= step;
    return tmp;
  }
  self& operator-=(const difference_type x) {
    m -= x * step;
    return *this;
  }
  reference operator[](const difference_type n) { return m[n * step]; }
  reference operator*() const { return *m; }

  // friend operators
  friend bool operator==(const self& x, const self& y) {
    assert(x.step == y.step);
    return x.m == y.m;
  }
  friend bool operator!=(const self& x, const self& y) {
    assert(x.step == y.step);
    return x.m != y.m;
  }
  friend bool operator<(const self& x, const self& y) {
    assert(x.step == y.step);
    return x.m < y.m;
  }
  friend bool operator<=(const self& x, const self& y) {
    assert(x.step == y.step);
    return x.m <= y.m;
  }
  friend bool operator>(const self& x, const self& y) {
    assert(x.step == y.step);
    return x.m > y.m;
  }
  friend bool operator>=(const self& x, const self& y) {
    assert(x.step == y.step);
    return x.m >= y.m;
  }
  friend difference_type operator-(const self& x, const self& y) {
    assert(x.step == y.step);
    return (x.m - y.m) / x.step;
  }

  friend self operator+(const self& x, difference_type y) {
    return self(x.m + (y * x.step), x.step);
  }

  friend self operator+(difference_type x, const self& y) { return y + x; }

  friend self operator-(const self& x, difference_type y) {
    return self(x.m - (y * x.step), x.step);
  }

 private:
  itr_t m;
  difference_type step;
};

class IdxVecIterator {
  const IdxVec dims;
  IdxVec pos;

  void next() {
    if (pos[0] < dims[0]) {
      ++pos[0];
    } else if (pos[1] < dims[1]) {
      ++pos[1];
      pos[0] = 0;
    } else if (pos[2] < dims[2]) {
      ++pos[2];
      pos[1] = 0;
      pos[0] = 0;
    } else if (pos == dims) {
      pos = dims + IdxVec(1, 1, 1);
    } else {
      FATAL_ERROR("IdxVecIterator is in invalid state, this point should never be reached!");
    }
  }

 protected:
  IdxVecIterator(IdxVec dims, IdxVec pos) : dims(dims - IdxVec(1, 1, 1)), pos(pos) {
    assert(dims != IdxVec(0, 0, 0));
  }

 public:
  // public typedefs for STL algorithms
  using value_type        = IdxVec;
  using reference         = const IdxVec&;
  using difference_type   = ptrdiff_t;
  using pointer           = IdxVec*;
  using iterator_category = std::forward_iterator_tag;
  using self              = IdxVecIterator;

  static self begin(IdxVec dims) { return self(dims, IdxVec(0, 0, 0)); }

  static self end(IdxVec dims) { return self(dims, dims); }

  reference operator*() const { return pos; }

  self& operator++() {
    next();
    return *this;
  }

  self operator++(int) {
    next();
    return *this;
  }

  bool operator!=(const self& other) const {
    assert(dims == other.dims);
    return pos != other.pos;
  }

  bool operator==(const self& other) const {
    assert(dims == other.dims);
    return pos == other.pos;
  }

  friend difference_type operator-(const self& x, const self& y) {
    const auto nx = x.dims[0];
    const auto ny = x.dims[1];
    const Vector3<int> dotdims{1, nx, nx * ny};

    return x.pos.dot(dotdims) - y.pos.dot(dotdims);
  }
};

class Positions {
  IdxVec dims;

 public:
  using value_type = IdxVec;
  using iterator   = IdxVecIterator;

  Positions(IdxVec dims) : dims(dims) {}

  IdxVecIterator begin() const { return IdxVecIterator::begin(dims); }
  IdxVecIterator end() const { return IdxVecIterator::end(dims); }

  int size() const { return dims.element_product(); }
};

template <typename T, typename Func>
class PosFuncIterator {
  Func func;
  IdxVecIterator itr;

 public:
  // public typedefs for STL algorithms
  using value_type        = T;
  using reference         = const T&;
  using difference_type   = IdxVecIterator::difference_type;
  using pointer           = T*;
  using iterator_category = std::forward_iterator_tag;
  using self              = PosFuncIterator;

 protected:
  PosFuncIterator(Func func, IdxVecIterator itr) : func(func), itr(itr) {}

 public:
  static self begin(Func func, IdxVec dims) { return self(func, IdxVecIterator::begin(dims)); }

  static self end(Func func, IdxVec dims) { return self(func, IdxVecIterator::end(dims)); }

  value_type operator*() const { return func(*itr); }

  self& operator++() {
    itr++;
    return *this;
  }

  self operator++(int) {
    self tmp = *this;
    itr++;
    return tmp;
  }

  bool operator!=(const self& other) const { return itr != other.itr; }

  bool operator==(const self& other) const { return itr == other.itr; }

  friend difference_type operator-(const self& x, const self& y) {
    return std::distance(y.itr, x.itr);
  }
};

template <typename T, typename Func>
class PosFuncHolder {
  IdxVec dims;
  Func func;

 public:
  using value_type = T;
  using iterator   = PosFuncIterator<T, Func>;

  PosFuncHolder(IdxVec dims, Func func) : dims(dims), func(func) {}

  iterator begin() const { return PosFuncIterator<T, Func>::begin(func, dims); }
  iterator end() const { return PosFuncIterator<T, Func>::end(func, dims); }

  int size() const { return dims.element_product(); }
};