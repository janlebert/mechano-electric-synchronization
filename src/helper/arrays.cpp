#include "arrays.h"

#include <algorithm>
#include <complex>

template <typename T>
CircularArray<T>::CircularArray(int x, int y, int z, int window, long buffer_multiplier)
    : positions({x, y, z}), _buffer_multiplier(buffer_multiplier) {

  ASSERT_ALWAYS(x > 0 && y > 0 && z > 0 && window > 0);

  static_assert(std::is_trivially_copyable<T>::value, "Requires trivially copyable type for memcpy");

  // Check if allocated size would be larger than 4 GB, if so reduce buffer_multiplier
  constexpr unsigned long long max_buffer_size = 4 * 1024 * 1024 * 1024ull;
  while ((_buffer_multiplier > 2) &&
         (x * y * z * window * _buffer_multiplier * sizeof(T) > max_buffer_size)) {
    log_msg(std::cerr, 0,
            "CircularArray {}: array size is larger than 4GB, reducing buffer_multiplier {} to {}",
            static_cast<void *>(this), _buffer_multiplier, std::max(_buffer_multiplier - 1l, 2l));
    _buffer_multiplier = std::max(_buffer_multiplier - 1l, 2l);
  }

  this->alloc_data(x, y, z, window);
  std::fill_n(_head, _total_length, _init_value);  // set to zero
}

template <typename T>
CircularArray<T>::~CircularArray() {
  delete[] _data;
}
template <typename T>
CircularArray<T>::CircularArray(const CircularArray<T> &other)
    : positions(other._dims),
      _buffer_multiplier(other._buffer_multiplier),
      _init_value(other._init_value) {

  this->alloc_data(other._dims[0], other._dims[1], other._dims[2], other._window);
  std::copy(other._head, other._head + _total_length, _head);

  _t_head    = other._t_head;
  _t_end     = other._t_end;
  _t_maximum = other._t_maximum;
}

template <typename T>
CircularArray<T>::CircularArray(CircularArray<T> &&other) noexcept : CircularArray<T>(1, 1, 1, 1) {
  swap(*this, other);
}

template <typename T>
CircularArray<T> &CircularArray<T>::operator=(CircularArray<T> other) {
  swap(*this, other);
  return *this;
}

template <typename T>
void CircularArray<T>::alloc_data(int x, int y, int z, int window) {
  // Initialize variables
  _dims         = {x, y, z};
  _dotdims      = {1, x, x * y};
  _window       = window;
  _array_length = _dims.element_product();
  _inner_length = window * _array_length;
  _total_length = _buffer_multiplier * _inner_length;

  // Allocate data
  if (_data) delete[] _data;
  _data = new T[_total_length];
  ASSERT_ALWAYS(_data, "CircularArray: data allocation failed!");


  updatePointersAndCounters(_data, _window);
}

template <typename T>
void CircularArray<T>::sanityCheckT(long t) const {
  ASSERT_ALWAYS(t < _t_end,
                "CircularArray range error, t = {} is newer than the newest frame stored at t = {}!",
                t, _t_end - 1);

  ASSERT_ALWAYS(t >= _t_head,
                "CircularArray range error, t = {} is older than the oldest frame stored at t = {}!",
                t, _t_head);
}

template <typename T>
void CircularArray<T>::sanityCheckPointer(T *p) const {
  ASSERT_DEBUG(p >= _head && p <= _end);
}

template <typename T>
void CircularArray<T>::updatePointersAndCounters(T *head, long t_end) {
  _head = head;
  ASSERT_DEBUG(_head >= _data);
  _end = _head + _inner_length;
  ASSERT_DEBUG(_end <= _data + _total_length);

  if (t_end < _window) {
    t_end = _window;
  }

  _t_end  = t_end;
  _t_head = _t_end - _window;

  if (head == _data) {
    _t_maximum = _t_head + _buffer_multiplier * _window;
  }
}

template <typename T>
void CircularArray<T>::reset(long t_end) {
  updatePointersAndCounters(_data, t_end);

  std::fill_n(_data, _total_length, _init_value);
}

template <typename T>
void CircularArray<T>::move_arrays(long t_end) {
  if (t_end < _t_end + _window) {

    auto items_to_copy = (_window - (t_end - _t_end)) * _array_length;
    ASSERT_DEBUG(items_to_copy > 0 && items_to_copy < _total_length);

    std::memmove(_data, _end - items_to_copy, items_to_copy * sizeof(T));

    std::fill_n(_data + items_to_copy, _total_length - items_to_copy, _init_value);

    updatePointersAndCounters(_data, t_end);
  } else {
    reset(t_end);
  }
}

template <typename T>
long CircularArray<T>::position(IdxVec pos, long t) const {
  sanityCheckT(t);

#ifndef NDEBUG
  for (int i = 0; i < 3; i++) {
    ASSERT_DEBUG(pos[i] >= 0 && pos[i] < _dims[i],
                 "Out of range: CircularArray pos {} is not inside dimensions {}!", pos, _dims);
  }
#endif

  return (t - _t_head) * _array_length + pos.dot(_dotdims);
}

template <typename T>
T &CircularArray<T>::get(IdxVec pos, long t) {
  if (t >= _t_end) {
    if (t < _t_maximum - 1) {
      updatePointersAndCounters(_head + (t + 1 - _t_end) * _array_length, t + 1);
    } else {
      move_arrays(t + 1);
    }
  }

  auto p = position(pos, t);

  ASSERT_DEBUG((p < _inner_length) && (_head + p <= _data + _total_length) && (_head + p >= _data),
               "CircularArray: illegal memory location!");

  return _head[p];
}

template <typename T>
T &CircularArray<T>::get_const(IdxVec pos, long t) const {
  sanityCheckT(t);

  auto p = position(pos, t);

  ASSERT_DEBUG((p < _inner_length) && (_head + p <= _data + _total_length) && (_head + p >= _data),
               "CircularArray: illegal memory location!");

  return _head[p];
}

template <typename T>
Vector4<int> CircularArray<T>::dimensions() const {
  return {this->Nx(), this->Ny(), this->Nz(), this->Nt()};
}

template <typename T>
long CircularArray<T>::size() const {
  return _dims.element_product() * static_cast<long>(this->Nt());
}

template <typename T>
void CircularArray<T>::resize(int window) {
  alloc_data(_dims[0], _dims[1], _dims[2], window);
  std::fill_n(_head, _total_length, _init_value);  // set to zero
}

template <typename T>
void CircularArray<T>::fill(T x) {
  _init_value = x;
  reset(_t_end);
}

template <typename T>
Array1<T> CircularArray<T>::timeTrace(IdxVec pos, long t_end) const {
  sanityCheckT(t_end);

  //  if (t_end < _window - 1) {
  //    t_end = _window - 1;
  //  }

  Array1<T> v(t_end + 1 - _t_head);
  for (int i = 0; i < v.size(); i++) {  // include element at t_end
    v(i) = get_const(pos, _t_head + i);
  }

  return v;
}

template <typename T>
void CircularArray<T>::loadTimeTrace(IdxVec pos, long t_end, Array1<T> &trace) {
  sanityCheckT(t_end);
  ASSERT_DEBUG(trace.size() == t_end + 1 - _t_head);

  //  if (t_end < _window - 1) {
  //    t_end = _window - 1;
  //  }

  for (int i = trace.size(); i > 0; i--) {  // include element at t_end
    get_const(pos, t_end - trace.size() + i) = trace(i - 1);
  }
}

template <typename T>
Array3<T> CircularArray<T>::operator[](long t) {
  set_t_end(t);

  return Array3<T>(_dims[0], _dims[1], _dims[2], data(t));
}

template <typename T>
const Array3<T> CircularArray<T>::operator[](long t) const {
  sanityCheckT(t);

  return Array3<T>(_dims[0], _dims[1], _dims[2], data(t));
}

template <typename T>
bool CircularArray<T>::operator==(CircularArray<T> &a) {
  return dimensions() == a.dimensions() && get_t_end() == a.get_t_end() &&
         get_t_head() == a.get_t_head() && std::equal(begin(), end(), a.begin());
}

template <typename T>
T *CircularArray<T>::data(long t) const {
  sanityCheckT(t);

  T *p = _head + (t - _t_head) * _array_length;
  sanityCheckPointer(p);
  return p;
}

template <typename T>
typename CircularArray<T>::iterator CircularArray<T>::end(long t) {
  auto p = std::min(data(t) + _array_length, _end);
  sanityCheckPointer(p);
  return iterator(p);
}

template <typename T>
typename CircularArray<T>::timeTraceIterator CircularArray<T>::begin(const IdxVec &pos) {
  auto p = data() + position(pos, _t_head);
  sanityCheckPointer(p);
  return StrideIterator<iterator>(iterator(p), _array_length);
}

template <typename T>
typename CircularArray<T>::timeTraceIterator CircularArray<T>::end(const IdxVec &pos, long t_end) {
  sanityCheckT(t_end);
  auto p = data() + position(pos, t_end);
  sanityCheckPointer(p);
  return StrideIterator<iterator>(iterator(p + _array_length), _array_length);
}

// Explicit instantiation of the template classes, types have to be listed here for the
// code to be compiled for it.
template class CircularArray<bool>;
template class CircularArray<num>;
template class CircularArray<Vec3>;
template class CircularArray<std::complex<num>>;