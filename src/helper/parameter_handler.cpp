#include "parameter_handler.h"

namespace {
  // /dev/null ostream
  class NullStream : public std::ostream {
    class NullBuffer : public std::streambuf {
     public:
      int overflow(int c) override { return c; }
    } m_nb;

   public:
    NullStream() : std::ostream(&m_nb) {}
  };

  std::string trim(const std::string &s) {
    auto is_space = [](int c) { return std::isspace(c); };
    auto front    = std::find_if_not(s.begin(), s.end(), is_space);
    auto back     = std::find_if_not(s.rbegin(), s.rend(), is_space).base();
    return (back <= front ? std::string() : std::string(front, back));
  }

  std::vector<std::string> split(const std::string &s, char seperator) {
    std::vector<std::string> result;
    size_t pos = 0, prev_pos = 0;
    while ((pos = s.find(seperator, pos)) != std::string::npos) {
      result.push_back(s.substr(prev_pos, pos - prev_pos));
      prev_pos = ++pos;
    }
    result.push_back(s.substr(prev_pos, pos - prev_pos));  // last word

    return result;
  }

  std::vector<std::string> parse_str_vector(std::string input) {
    string r = trim(input);
    ASSERT_ALWAYS(!r.empty() && (r.front() == '(') && (r.back() == ')'),
                  "Parsing of parameter value '{}' failed", input);
    r.erase(0, 1);
    r.pop_back();

    return split(r, ',');
  }
}  // namespace

template <typename T>
void ParameterHandler::logparam(const string &key,
                                const T &r,
                                const T &default_value,
                                string comment) {
  const bool changed_value = (r != default_value);
  const bool has_comment   = !comment.empty();

  const auto padding_pair = 35ul;

  string l = fmt::format("{:<12} = {}", key, r);
  if (l.size() < padding_pair) {
    l += fmt::format("{:<{}}", "", padding_pair - l.size());
  }

  if (changed_value && has_comment) {
    l = fmt::format("{} ; default = {} \t# {}", l, default_value, comment);
  } else if (changed_value) {
    l = fmt::format("{} ; default = {}", l, default_value);
  } else if (has_comment) {
    l = fmt::format("{} ; {}", l, comment);
  }

  logstream() << l << std::endl;
}

void ParameterHandler::init(string str, string logfilename, bool load_str_as_file) {
  if (!logfilename.empty()) {
    _logstream = make_shared<std::ofstream>(logfilename.c_str(), std::ios::out);
  } else {
#ifdef _WIN32  // Set output to UTF8 on Windows
    std::system("chcp 65001 > nul");
#endif
  }

  logstream() << std::boolalpha;

  reader = std::make_unique<INIReader>(str, load_str_as_file);

  if (load_str_as_file) {
    if (reader->ParseError() < 0) {
      FATAL_ERROR(
          "Could not load parameter file '{}'! If you want to test this program you should copy a "
          "params.ini file from the case_studies directory to the current directory",
          str);
    } else if (reader->ParseError() > 0) {
      FATAL_ERROR("Parse error on line {} of parameter file '{}'", reader->ParseError(), str);
    }

    log_msg(logstream(), -1, "Loaded parameter file '{}':\n\n\n", str);

  } else {

    ASSERT_ALWAYS(reader->ParseError() == 0,
                  "error parsing parameters from raw string on line {} of:\n\n{}\n\n",
                  reader->ParseError(), str);

    log_msg(logstream(), -1, "Loading parameters from raw string:\n\n\n");
  }
}

void ParameterHandler::enter_section(string section) {
  current_section = section;
  logstream() << "[" << section << "]" << std::endl;
}

void ParameterHandler::leave_section() {
  current_section = "";
  logstream() << std::endl << std::endl;
}

string ParameterHandler::get(string key, string default_value, string comment) {
  auto r = reader->Get(current_section, key, default_value);
  this->logparam(key, r, default_value, comment);
  return r;
}

long ParameterHandler::get_integer(string key, long default_value, string comment) {
  auto r = reader->GetInteger(current_section, key, default_value);
  this->logparam(key, r, default_value, comment);
  return r;
}

num ParameterHandler::get_real(string key, double default_value, string comment) {
  auto r = reader->GetReal(current_section, key, default_value);
  this->logparam(key, r, default_value, comment);
  return r;
}

bool ParameterHandler::get_boolean(string key, bool default_value, string comment) {
  auto r = reader->GetBoolean(current_section, key, default_value);
  this->logparam(key, r, default_value, comment);
  return r;
}

Vec3 ParameterHandler::get_Vec3(string key, Vec3 default_value, string comment) {
  string r = reader->Get(current_section, key, default_value.to_string());

  auto v = parse_str_vector(r);
  ASSERT_ALWAYS(v.size() == 3, "Parsing of parameter {} failed (value: '{}')!", key, r);
  Vec3 result(std::stod(v[0]), std::stod(v[1]), std::stod(v[2]));

  this->logparam(key, result, default_value, comment);
  return result;
}

Vec2 ParameterHandler::get_Vec2(string key, Vec2 default_value, string comment) {
  string r = reader->Get(current_section, key, default_value.to_string());

  auto v = parse_str_vector(r);
  ASSERT_ALWAYS(v.size() == 2, "Parsing of parameter {} failed (value: '{}')!", key, r);
  Vec2 result(std::stod(v[0]), std::stod(v[1]));

  this->logparam(key, result, default_value, comment);
  return result;
}

IdxVec ParameterHandler::get_IdxVec(string key, IdxVec default_value, string comment) {
  string r = reader->Get(current_section, key, default_value.to_string());

  auto v = parse_str_vector(r);
  ASSERT_ALWAYS(v.size() == 3, "Parsing of parameter {} failed (value: '{}')!", key, r);
  IdxVec result(std::stoi(v[0]), std::stoi(v[1]), std::stoi(v[2]));

  this->logparam(key, result, default_value, comment);
  return result;
}

std::ostream &ParameterHandler::logstream() const {
  return *_logstream;
}

std::shared_ptr<std::ostream> ParameterHandler::logstream_ptr() const {
  return _logstream;
}

void ParameterHandler::set_logstream(std::shared_ptr<std::ostream> logstream) {
  _logstream = logstream;
}


void ParameterHandler::turn_off_logging() {
  _logstream = make_shared<NullStream>();
}

void ParameterHandler::turn_on_logging(std::shared_ptr<std::ostream> logstream) {
  if (logstream) {
    _logstream = logstream;
  } else {
    _logstream = {&std::cout, [](std::ostream *) {}};
  }
}
