#pragma once

#include "rowview.h"
#include "filters/noiseadder.h"
#include "filters/deriche.h"
#include "filters/renorm.h"

class RowView3D : public RowView {
 protected:
  bool draw_fiberorientation;
  bool draw_colormap;
  Array3<array<Vec3, 3>> fsn_orientations;

  static void rotate3D_default(const Vec3& translate) {
    glTranslate(translate);
    glRotatef(-60, 1, 0, 0);
    glRotatef(-135, 0, 0, 1);
  }

  static void unrotate3D_default(const Vec3& translate) {
    glTranslate(translate);
    glRotatef(135, 0, 0, 1);
    glRotatef(60, 1, 0, 0);
  }

 public:
  RowView3D(std::shared_ptr<SimulationDataStreams> _simDataStream,
            RateType rt,
            string label,
            bool _draw_fiberorientation = abs(prm.fiber_zmax_angle) > 1e-2,
            bool draw_colormap          = true)
      : RowView(std::move(_simDataStream), rt, label),
        draw_fiberorientation(_draw_fiberorientation),
        draw_colormap(draw_colormap),
        fsn_orientations(prm.dims) {

    rate->set_z_start_end(0, prm.Nz);

    height_px_factor += 0.2 * (!label.empty()) + 0.1 * draw_colormap;

    if (simData && simData->fiberorientation.good()) {
      fsn_orientations.load(simData->fiberorientation);
    } else {
      draw_fiberorientation = false;
    }

    //rate->set_minmax_fixed(0.33);
  }

  void display(long int t, Vec3 offset, bool play) override {
    if (play) {
      readBinaryFileElectro(t);
      readBinaryFileMechanics(t);
    }

    if (compute_rate) {
      if (rt != RateType::File) {
        if (play) {
          rate->compute();
        }
      } else {
        rate->compute(t);
      }

      if (t % 3 * prm.tw == 0) {
        recalibrateColormap(false);
      }
    }

    if (!label.empty()) {
      offset -= {0, 20, 0};
      drawLabel(Vec3(offset[0], offset[1] + 23, 0), label);
    }

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(0, 0, -(prm.Nz - 1));
    glDisable(GL_CULL_FACE);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_MULTISAMPLE);  // alternative: GL_MULTISAMPLE_ARB
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    auto zero = Vec3(0, 0, 0);

    Vec3 left_offset  = left_cell_pos(offset) + Vec3(prm.Nx / 3.f, 0, 0);
    Vec3 right_offset = right_cell_pos(offset) - Vec3(prm.Nx / 3.f, 0, 0);

    glPushMatrix();
    rotate3D_default(left_offset);
    draw3DMeshBorder(zero, 3, 2);
    drawExcitation3D(zero);
    draw3DMeshTopOuterBorder(zero);
    glPopMatrix();

    if (draw_colormap) {
      static GLuint ecm_tex = colormap_as_texture(excitation_color, true, {0.5, 1});
      drawColormap(ecm_tex, {0, 1}, left_offset - Vec3(0, 5 + prm.Ny / 2.f, 0), {70, 5}, 8);
    }

    draw_text(left_offset + Vec3(0, prm.Ny / 2 + 10, 0), "Electrical Activity", 9.f);

    glPushMatrix();
    rotate3D_default(right_offset);
    draw3DMeshBorder(zero, 3, 2);
    drawRate3D(rate->rate, zero, mechanics_color, rate->minmax());
    draw3DMeshTopOuterBorder(zero);
    if (draw_fiberorientation) {
      drawZFiberOrientation({prm.spacing * prm.Ny - 10, -20, prm.spacing * (-prm.Nz / 2.f)}, 8);
    }
    glPopMatrix();

    if (draw_colormap) {
      static GLuint mcm_tex = colormap_as_texture(mechanics_color, true);
      drawColormap(mcm_tex, rate->minmax(), right_offset - Vec3(0, 5 + prm.Ny / 2.f, 0), {70, 5}, 8);
    }

    draw_text(right_offset + Vec3(0, prm.Ny / 2 + 10, 0), "Mechanical Deformation", 9.f);
    draw_text(right_offset + Vec3(0, prm.Ny / 2, 0), "Contraction  ", 8.f,
              mechanics_color(0.1, {0, 1}, false), "normal", FONS_ALIGN_RIGHT | FONS_ALIGN_BASELINE);
    draw_text(right_offset + Vec3(0, prm.Ny / 2, 0), "↔", 8.f, Colors::black, "normal",
              FONS_ALIGN_CENTER | FONS_ALIGN_BASELINE);
    draw_text(right_offset + Vec3(0, prm.Ny / 2, 0), "  Stretching", 8.f,
              mechanics_color(0.9, {0, 1}, false), "normal", FONS_ALIGN_LEFT | FONS_ALIGN_BASELINE);

    glEnable(GL_CULL_FACE);
    glPopMatrix();
  }

  void drawZFiberOrientation(Vec3 offset, float font_size = 4) {
    glPushMatrix();
    glTranslate(offset);

    auto fibersheet_pos = [](num z) { return prm.spacing * Vec3(0, 0, z); };

    auto color1 = Colors::gray_medium;
    auto color2 = Colors::gray_medium;

    color1[3] = 0.75;

    draw_3D_arrow(fibersheet_pos(0), fibersheet_pos(1.1f * prm.Nz), 2, {2, color2});
    for (int z = 0; z < prm.Nz; z++) {
      const Vec3 pos              = fibersheet_pos(z);
      const Vec3 fiber            = fsn_orientations(0, 0, z)[0];
      const LineStyle fiber_style = {2, color1};
      draw_3D_arrow(pos, fiber * 10, 1, fiber_style);
    }

    glPushMatrix();
    {
      unrotate3D_default(fibersheet_pos(1.2f * prm.Nz));
      draw_text({0, 0, 0}, "z", font_size - 2, color2);
    }
    glPopMatrix();

    glPushMatrix();
    {
      unrotate3D_default(fibersheet_pos(-0.4f * prm.Nz));
      draw_text({0, 0, 0}, "fiber orientation", font_size, color1);
    }
    glPopMatrix();

    glPopMatrix();
  }

  void draw3DMeshBorder(Array3<Vec3>& pos3D, Vec3 offset, int ref_xy, int ref_z, int boundary = 0);
  void draw3DMeshBorder(Vec3 offset, int ref_xy, int ref_z, int boundary = 0) {
    draw3DMeshBorder(this->pos3D, offset, ref_xy, ref_z, boundary);
  }

  void draw3DMeshTopOuterBorder(Array3<Vec3>& pos3D, Vec3 offset, int boundary = 0);
  void draw3DMeshTopOuterBorder(Vec3 offset, int boundary = 0) {
    draw3DMeshTopOuterBorder(this->pos3D, offset, boundary);
  }

  void drawRate3D(Array3<num>& a,
                  const Vec3& offset,
                  Colormap::colormap colormap,
                  Colormap::MinMax minmax) {
    drawArray3D(a, this->pos3D, offset, colormap, minmax);
  }

  void drawExcitation3D(const Vec3& offset) {
    drawArray3D(u3D, this->pos3D, offset, excitation_color, {-1, 1});
  }

  void drawArray3D(Array3<num>& a,
                   Array3<Vec3>& pos3D,
                   const Vec3& offset,
                   Colormap::colormap colormap,
                   Colormap::MinMax minmax) {
    drawArrays3D({&a}, pos3D, offset, {colormap}, {minmax});
  }

  void drawArrays3D(vector<Array3<num>*> va,
                    Array3<Vec3>& pos3D,
                    const Vec3& offset,
                    vector<Colormap::colormap> vcolormap,
                    vector<Colormap::MinMax> vminmax);
};

void RowView3D::draw3DMeshTopOuterBorder(Array3<Vec3>& pos3D, Vec3 offset, int boundary) {
  LineStyle line_style = {2.0f, meshcolor};

  const auto position = [this, &offset, &pos3D](IdxVec pos) -> Vec3 {
    return h_inv * pos3D(clamp_pos(pos)) + offset;
  };

  const int z_max = Nz - 1 - boundary;
  const int y_max = Ny - 1 - boundary;
  const int x_max = Nx - 1 - boundary;

  for (int y : std::array<int, 2>{0, y_max}) {
    for (int x = Nx - 1 - boundary; x > boundary; x -= 1) {
      Vec3 from = position({x, y, z_max});
      Vec3 to   = position({x - 1, y, z_max});
      draw_line(from, to, line_style);
    }
  }

  for (int x : std::array<int, 2>{0, x_max}) {
    for (int y = Ny - 1 - boundary; y > boundary; y -= 1) {
      Vec3 from = position({x, y, z_max});
      Vec3 to   = position({x, y - 1, z_max});
      draw_line(from, to, line_style);
    }
  }

  for (auto& p : std::array<std::pair<int, int>, 3>{
           {{prm.Nx - 1, prm.Ny - 1}, {0, prm.Ny - 1}, {prm.Nx - 1, 0}}}) {
    for (int z = 0; z < prm.Nz - 1; z++) {
      Vec3 from = position({p.first, p.second, z});
      Vec3 to   = position({p.first, p.second, z + 1});
      draw_line(from, to, line_style);
    }
  }
}

void RowView3D::draw3DMeshBorder(
    Array3<Vec3>& pos3D, Vec3 offset, int ref_xy, int ref_z, int boundary) {

  const auto line_style = [this](bool is_border) -> LineStyle {
    if (is_border) {
      return {2.0f, meshcolor};
    } else {
      auto c = meshcolor;
      c[3]   = 0.3;
      return {0.3f, c};
    };
  };

  const auto position = [this, &offset, &pos3D](IdxVec pos) -> Vec3 {
    return h_inv * pos3D(clamp_pos(pos)) + offset;
  };

  const auto draw_mesh_cell = [&line_style, &position](const std::array<IdxVec, 4>& xyz,
                                                       const std::array<bool, 4>& is_border) {
    std::array<Vec3, 4> pos = {
        {position(xyz[0]), position(xyz[1]), position(xyz[2]), position(xyz[3])}};

    draw_line(pos[0], pos[1], line_style(is_border[0]));
    draw_line(pos[1], pos[3], line_style(is_border[1]));
    draw_line(pos[3], pos[2], line_style(is_border[2]));
    draw_line(pos[2], pos[0], line_style(is_border[3]));
  };

  const int z_max = Nz - 1 - boundary;
  const int y_max = Ny - 1 - boundary;
  const int x_max = Nx - 1 - boundary;

  // Top, bottom
  for (int z : std::array<int, 1>{0 /*, z_max*/}) {
    for (int y = y_max; y > boundary; y -= ref_xy) {
      for (int x = x_max; x > boundary; x -= ref_xy) {
        // clang-format off
        std::array<IdxVec, 4> xyz = {{{x,          y,          z},
                                      {x - ref_xy, y,          z},
                                      {x,          y - ref_xy, z},
                                      {x - ref_xy, y - ref_xy, z}}};

        std::array<bool, 4> is_border = {{y == y_max,
                                          x <= ref_xy,
                                          y <= ref_xy,
                                          x == x_max}};

        draw_mesh_cell(xyz, is_border);
        // clang-format on
      }
    }
  }

  // Mesh along y side
  for (int y : std::array<int, 2>{0, y_max}) {
    for (int z = z_max; z > boundary; z -= ref_z) {
      for (int x = x_max; x > boundary; x -= ref_xy) {
        // clang-format off
        std::array<IdxVec, 4> xyz = {{{x,          y, z},
                                      {x - ref_xy, y, z},
                                      {x,          y, z - ref_z},
                                      {x - ref_xy, y, z - ref_z}}};

        std::array<bool, 4> is_border = {{z == z_max,
                                          x <= ref_xy,
                                          z <= ref_z,
                                          x == x_max}};

        draw_mesh_cell(xyz, is_border);
        // clang-format on
      }
    }
  }

  // Mesh along x side
  for (int x : std::array<int, 2>{0, x_max}) {
    for (int y = y_max; y > boundary; y -= ref_xy) {
      for (int z = z_max; z > boundary; z -= ref_z) {
        // clang-format off
        std::array<IdxVec, 4> xyz = {{{x, y,          z},
                                      {x, y - ref_xy, z},
                                      {x, y,          z - ref_z},
                                      {x, y - ref_xy, z - ref_z}}};

        std::array<bool, 4> is_border = {{z == z_max,
                                          y <= ref_xy,
                                          z <= ref_z,
                                          y == y_max}};

        draw_mesh_cell(xyz, is_border);
        // clang-format on
      }
    }
  }
}

void RowView3D::drawArrays3D(vector<Array3<num>*> va,
                             Array3<Vec3>& pos3D,
                             const Vec3& offset,
                             vector<Colormap::colormap> vcolormap,
                             vector<Colormap::MinMax> vminmax) {

  const bool alpha_transfer_fct = true;

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glTranslate(offset);

  std::array<Vec3, 8> positions;
  std::array<Color, 8> colors;

  const auto position = [this, &pos3D](IdxVec pos) -> Vec3 { return h_inv * pos3D(pos); };
  const auto color    = [&](IdxVec idx) -> Color {
    if (va.size() == 1) {  // hotpath
      num val = va[0]->operator()(idx);
      return vcolormap[0](val, vminmax[0], alpha_transfer_fct);
    }

    Color result{0, 0, 0, 0};
    num max_alpha = 0;

    for (auto i = 0; i < va.size(); i++) {
      num val = va[i]->operator()(idx);
      Color c = vcolormap[i](val, vminmax[i], alpha_transfer_fct);

      result += c;
      if (c[3] > max_alpha) max_alpha = c[3];
    }

    result    = result / va.size();
    result[3] = max_alpha;


    return result;
  };

  const auto colored_vertex_point = [&colors, &positions](int i) {
    glColor4fv(colors[i]);
    glVertex3nv(positions[i]);
  };

  for (int x = 0; x < Nx - 1; x++) {
    for (int y = 0; y < Ny - 1; y++) {
      for (int z = 0; z < Nz - 1; z++) {
        // clang-format off
        std::array<IdxVec, 8> xyz = {{{x,     y,     z},
                                      {x + 1, y,     z},
                                      {x,     y,     z + 1},
                                      {x + 1, y,     z + 1},
                                      {x,     y + 1, z},
                                      {x + 1, y + 1, z},
                                      {x,     y + 1, z + 1},
                                      {x + 1, y + 1, z + 1}}};
        // clang-format on

        for (int i = 0; i < 8; i++) {
          positions[i] = position(xyz[i]);
          colors[i]    = color(xyz[i]);
        }

        glBegin(GL_QUADS);

        /*
         *    6---7
         *   /|  /|
         *  2---3 |
         *  | 4-|-5
         *  |/  |/
         *  0---1
         *
         *  Sides:
         *  2---3  3---7  7---6  6---2
         *  |   |  |   |  |   |  |   |
         *  |   |  |   |  |   |  |   |
         *  0---1  1---5  5---4  4---0
         *
         *  Bottom/Top
         *  0---1  6---7
         *  |   |  |   |
         *  |   |  |   |
         *  4---5  2---3
         *
         */

        // Bottom
        colored_vertex_point(4);
        colored_vertex_point(5);
        colored_vertex_point(1);
        colored_vertex_point(0);

        // Sides
        colored_vertex_point(0);
        colored_vertex_point(1);
        colored_vertex_point(3);
        colored_vertex_point(2);

        colored_vertex_point(1);
        colored_vertex_point(5);
        colored_vertex_point(7);
        colored_vertex_point(3);

        colored_vertex_point(5);
        colored_vertex_point(4);
        colored_vertex_point(6);
        colored_vertex_point(7);

        colored_vertex_point(4);
        colored_vertex_point(0);
        colored_vertex_point(2);
        colored_vertex_point(6);

        // Top
        colored_vertex_point(2);
        colored_vertex_point(3);
        colored_vertex_point(7);
        colored_vertex_point(6);

        glEnd();
      }
    }
  }

  glPopMatrix();
}

class RowView3D_DA : public RowView3D {
 protected:
  std::shared_ptr<SimulationDataStreams> daDataStream;

  Array3<Vec3> pos3D_noised;
  NoiseAdder noiseAdder;
  std::shared_ptr<Rate3D> rate_noised;
  DericheFilter filter;
  RateDebug3D rate_filtered;
  Renorm renorm;
  RateDebug3D rate_renorm;

  Array3<num> u3D_da;
  Array3<Vec3> pos3D_da;
  std::shared_ptr<Rate3D> rate_da;

  Array3<Vec3> pos3D_reference;
  std::unique_ptr<FloatDiffRate3D> u_diff;

 public:
  RowView3D_DA(std::shared_ptr<SimulationDataStreams> simDataStream,
               std::shared_ptr<SimulationDataStreams> daDataStream,
               RateType rt,
               string label)
      : RowView3D(std::move(simDataStream), rt, label),
        daDataStream(std::move(daDataStream)),
        pos3D_noised(prm.dims),
        noiseAdder(prm.da_noise_stdev * prm.spacing),
        filter(prm.da_smoothing_sigma),
        rate_filtered(""),
        renorm(prm.dims),
        rate_renorm(""),
        u3D_da(prm.dims),
        pos3D_da(prm.dims),
        pos3D_reference(prm.dims) {

    height_px_factor = 1.5;

    rate_noised = make_rate(rt, pos3D_noised, simDataStream.get());
    rate_da     = make_rate(rt, pos3D_da, daDataStream.get());

    u_diff = make_unique<FloatDiffRate3D>(&u3D, &u3D_da, "");

    // Calculate undeformed mesh grid positions
    Array3<Vec3> padded_positions(prm.dims_padded);

    const Vec3 centering = static_cast<Vec3>(prm.dims_padded) / static_cast<num>(2);
    for (auto& pos : prm.positions_padded) {
      Vec3 position{prm.Lx * (pos[0] / static_cast<num>(prm.Lx)),
                    prm.Ly * (pos[1] / static_cast<num>(prm.Ly)),
                    prm.Lz * (pos[2] / static_cast<num>(prm.Lz))};

      position -= centering;
      position *= prm.spacing;

      padded_positions(pos) = position;
    }

    IdxVec padding = prm.dims_padded - prm.dims;
    for (auto& pos : prm.positions) {
      IdxVec padded_pos    = pos + padding;
      pos3D_reference(pos) = padded_positions(padded_pos);
    }

    rate_da->set_minmax_fixed(rate.get());
  }

  void readBinaryFileElectro(long int frame) override {
    u3D.load(simData->u, frame);
    u3D_da.load(daDataStream->u, frame);
  }

  void readBinaryFileMechanics(long int frame) override {
    pos3D.load(simData->pos, frame);
    pos3D_da.load(daDataStream->pos, frame);
  }

  void recalibrateColormap(bool forced) override {
    rate->calibrate_minmax(forced, calibrate_factor);
    rate_da->calibrate_minmax(forced, calibrate_factor);
  }

  void clearEOF() override {
    simData->clearEOF();
    daDataStream->clearEOF();
  }

  void display(long int t, Vec3 offset, bool play) override {
    if (play) {
      readBinaryFileElectro(t);
      readBinaryFileMechanics(t);
      noiseAdder.apply(pos3D, pos3D_noised);
    }

    if (compute_rate && play) {
      if (rt != RateType::File) {
        rate->compute();
        rate_noised->compute();
        rate_da->compute();
      } else {
        rate->compute(t);
        rate_noised->compute(t);
        rate_da->compute(t);
      }

      if (t % prm.tw_da * prm.tw == 0) {
        filter.apply(rate_noised->rate, rate_filtered.rate);
        renorm.apply(rate_filtered.rate, rate_renorm.rate);
      }

      u_diff->compute();

      if (t < std::min(prm.t_D_start / 10, 250l)) {
        recalibrateColormap(false);
      }
    }



    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(0, 0, -(prm.Nz - 1));
    glScalef(0.74, 0.74, 0.74);

    glDisable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    auto zero = Vec3(0, 0, 0);

    Vec3 left_offset  = left_cell_pos(offset) - Vec3(0.4*Nx, 0, 0);
    Vec3 right_offset = right_cell_pos(offset) + Vec3(0.4*Nx, 0, 0);

    glPushMatrix();
    rotate3D_default(left_offset);
    draw3DMeshBorder(zero, 3, 2);
    drawExcitation3D(zero);
    draw3DMeshTopOuterBorder(zero);
    if (draw_fiberorientation) {
      drawZFiberOrientation({0, prm.spacing * prm.Ny, -prm.spacing * 0.75f * prm.Nz}, 8);
    }
    glPopMatrix();

    glPushMatrix();
    rotate3D_default(offset);
    draw3DMeshBorder(zero, 3, 2);
    drawRate3D(rate->rate, zero, mechanics_color, rate->minmax());
    draw3DMeshTopOuterBorder(zero);
    glPopMatrix();

    glPushMatrix();
    rotate3D_default(right_offset);
    draw3DMeshBorder(zero, 3, 2);
    drawRate3D(rate_renorm.rate, zero, mechanics_color, {-1, 1});
    draw3DMeshTopOuterBorder(zero);
    glPopMatrix();

    static GLuint ecm_tex      = colormap_as_texture(excitation_color, true, {0.5, 1});
    static GLuint mcm_tex      = colormap_as_texture(mechanics_color, true);
    static GLuint ecm_full_tex = colormap_as_texture(excitation_color, true);
    const Vec2 colormap_size{70, 5};
    const Vec3 colormap_offset{0, -(prm.Ny / 2.f + 2.f), 0};
    if (draw_colormap) {
      drawColormap(ecm_tex, {0, 1}, left_offset + colormap_offset, colormap_size, 8);
      drawColormap(mcm_tex, rate->minmax(), offset + colormap_offset, colormap_size, 8);
      drawColormap(mcm_tex, {-1, 1}, right_offset + colormap_offset, colormap_size, 8);
    }

    const float font_size  = 10;
    const Color font_color = Colors::black;
    const Vec3 text_offset{0, prm.Ny / 2.f + 2.f, 0};
    draw_text(left_offset + text_offset, "Source Electrical Activity", font_size, font_color);
    draw_text(offset + text_offset, "Source Mechanical Deformation", font_size, font_color);
    draw_text(right_offset + text_offset, "Synchronization Input", font_size, font_color);

    /* next row */
    offset -= Vec3(0, prm.Ny + 3 * Viewer::row_padding, 0);
    left_offset  = left_cell_pos(offset) - Vec3(0.4*Nx, 0, 0);
    right_offset = right_cell_pos(offset) + Vec3(0.4*Nx, 0, 0);

    glPushMatrix();
    rotate3D_default(left_offset);
    draw3DMeshBorder(pos3D_da, zero, 3, 2);
    drawArray3D(u3D_da, pos3D_da, zero, excitation_color, {-1, 1});
    draw3DMeshTopOuterBorder(pos3D_da, zero);
    glPopMatrix();

    glPushMatrix();
    rotate3D_default(offset);
    draw3DMeshBorder(pos3D_da, zero, 3, 2);
    drawArray3D(rate_da->rate, pos3D_da, zero, mechanics_color, rate_da->minmax());
    draw3DMeshTopOuterBorder(pos3D_da, zero);
    glPopMatrix();

    glPushMatrix();
    rotate3D_default(right_cell_pos(offset) + Vec3(40, 0, 0));
    draw3DMeshBorder(pos3D_da, zero, 3, 2);
    drawArray3D(u_diff->rate, pos3D_da, zero, excitation_color, {-1, 1});
    draw3DMeshTopOuterBorder(pos3D_da, zero);
    glPopMatrix();

    if (draw_colormap) {
      drawColormap(ecm_tex, {0, 1}, left_offset + colormap_offset, colormap_size, 8);
      drawColormap(mcm_tex, rate_da->minmax(), offset + colormap_offset, colormap_size, 8);
      drawColormap(ecm_full_tex, {-1, 1}, right_offset + colormap_offset, colormap_size, 8);
    }

    draw_text(left_offset + text_offset, "Reconstructed Electrical Activity", font_size, font_color);
    draw_text(offset + text_offset, "Mechanical Deformation", font_size, font_color);
    draw_text(right_offset + text_offset, u8"Δ Electrical Activity", font_size, font_color);

    const Vec3 syn_text_pos{14, -3 * prm.Ny / 2.f - 6.f, 0};
    float syn_text_fonts = 12;
    draw_text(syn_text_pos, "Coupling ", syn_text_fonts, Colors::black, "normal",
              FONS_ALIGN_RIGHT | FONS_ALIGN_BASELINE);
    if (t > prm.Nt_assim_on && t < prm.Nt_assim_off) {
      draw_text(syn_text_pos, "on", syn_text_fonts, Colors::green, "italic",
                FONS_ALIGN_LEFT | FONS_ALIGN_BASELINE);
    } else {
      draw_text(syn_text_pos, "off", syn_text_fonts, Colors::red, "italic",
                FONS_ALIGN_LEFT | FONS_ALIGN_BASELINE);
    }


    glEnable(GL_CULL_FACE);
    glPopMatrix();
  }
};
