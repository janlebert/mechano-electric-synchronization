#pragma once

#ifdef _WIN32  // Windows 32 and 64 bit
#include <windows.h>
#endif

#include <GLFW/glfw3.h>

/* Includes which may depend on Viewer class are included later */
#include "parameters.h"
#include "rowview.h"

/* Forward declarations */
class VideoRecorder;
class GuiWindow;

/* Global variables */
Parameters prm;
std::vector<std::shared_ptr<RowView>> rows;
std::vector<std::shared_ptr<GuiWindow>> guis;

// Classes definitions
class Viewer {
 public:
  static bool instance_exists;  // singelton class

  static long int t;
  static long int t_end;

  static GLFWwindow *window;
  static VideoRecorder recorder;

  /* user mouse controls */
  static int mouse_button;
  static Vector3<float> rotate;
  static Vector3<float> viewpoint;

  /* parameters for visualization */
  static int window_width;
  static double zoom;
  static float background;
  static Vec3 rows_offset;
  static int row_padding;  // spacing between rows
  static int msaa_samples;

  /* runtime settings */
  static bool play;
  static bool loop;
  static bool show_current_time;
  static bool makesnapshots;
  static bool makemovie;
  static bool exit_on_eof;

  const string default_title = "Elastic Reaction-Diffusion Model [Mass-Spring Particle System] with Fiber Orientation";

  explicit Viewer(bool record_and_exit = false, string title = "");

  ~Viewer();

  GLFWwindow *init_glfw(string title);

  void init_opengl(GLFWwindow *window, bool record_and_exit, string title);

  virtual void run();

  void display(Vec3 offset = Vec3(0, 0, 0));

  void setup_viewport();

  static std::pair<float, float> get_total_rows_width_height();

  static void reshape_callback(GLFWwindow *window, int w, int h);

  static void cursor_position_callback(GLFWwindow *window, double xpos, double ypos);

  static void scroll_callback(GLFWwindow *window, double xoffset, double yoffset);

  static void mouse_button_callback(GLFWwindow *window, int button, int action, int mods);

  static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods);

  static void set_frameskip(int frameskip);

  static void exit_viewer(int return_code);

 private:
  // Singelton class, should never be copied. Delete move assignment and copy constructors
  Viewer(const Viewer &other) = delete;
  void operator=(const Viewer &other) = delete;
};

/* Includes which may depend on Viewer class (currently only gui.h) */
#include "helper/videorecorder.h"
#include "helper/gl_helpers.h"
#include "rowview_3D.h"
#include "rowview_live.h"
#include "rowview_filaments.h"
#include "gui.h"

/* static class member initialization */

bool Viewer::instance_exists = false;

long int Viewer::t     = 0;
long int Viewer::t_end = 0;

GLFWwindow *Viewer::window     = nullptr;
VideoRecorder Viewer::recorder = VideoRecorder();

/* mouse controls */
int Viewer::mouse_button         = -1;
Vector3<float> Viewer::rotate    = {0, 0, 0};
Vector3<float> Viewer::viewpoint = {0, 0, 0};

/* parameters for visualization */
int Viewer::window_width = 0;
double Viewer::zoom      = 1;
float Viewer::background = 1;
Vec3 Viewer::rows_offset = {0, 0, 0};
int Viewer::row_padding  = 20;  // spacing between rows
int Viewer::msaa_samples = 8;

bool Viewer::play              = true;
bool Viewer::loop              = false;
bool Viewer::show_current_time = true;
bool Viewer::makesnapshots     = false;
bool Viewer::makemovie         = false;
bool Viewer::exit_on_eof       = false;

/****************************************************/
/*               Implementation                     */
/****************************************************/

Viewer::Viewer(bool record_and_exit, string title) {

  ASSERT_ALWAYS(!instance_exists, "only one viewer instance should exist at one time");

  instance_exists = true;

  recorder.videotitle = title;
  if (title.empty()) title = default_title;

  window = init_glfw(title);

  ImGuiConnector::Init(window);

  init_opengl(window, record_and_exit, title);
}

std::pair<float, float> Viewer::get_total_rows_width_height() {
  float height_factor = 0;
  for (auto &r : rows) {
    height_factor += r->height_px_factor;
  }

  const float w = prm.Nx * 3.f + 3 * RowView::cells_padding;
  const float h = prm.Ny * height_factor + row_padding * (height_factor + 3);

  return {w, h};
}

GLFWwindow *Viewer::init_glfw(string title) {

  glfwSetErrorCallback(glfw_error_callback);

  ASSERT_ALWAYS(glfwInit(), "GLFW initialization failed");

  glfwWindowHint(GLFW_SAMPLES, msaa_samples);

  if (window_width == 0) {
    auto mode    = glfwGetVideoMode(glfwGetPrimaryMonitor());
    window_width = 0.7 * mode->width;
  }

  auto wh    = get_total_rows_width_height();
  int height = window_width * wh.second / wh.first;

  window = glfwCreateWindow(window_width, height, title.c_str(), nullptr, nullptr);
  if (!window) exit_viewer(EXIT_FAILURE);

  // force window size, even if longer than monitor display height
  glfwSetWindowSize(window, window_width, height);

  glfwSetWindowSizeCallback(window, reshape_callback);
  glfwSetKeyCallback(window, key_callback);
  glfwSetCursorPosCallback(window, cursor_position_callback);
  glfwSetScrollCallback(window, scroll_callback);
  glfwSetMouseButtonCallback(window, mouse_button_callback);

  glfwMakeContextCurrent(window);
  glfwSwapInterval(1);  // wait until the current frame has been drawn before drawing the next one

  int w, h;
  glfwGetWindowSize(window, &w, &h);
  reshape_callback(window, w, h);

  log_msg("[viewer] OpenGL context version: {}", glGetString(GL_VERSION));
  log_msg("[viewer] OpenGL context renderer: {}", glGetString(GL_RENDERER));

  GLint samples, samplebuffers;
  glGetIntegerv(GL_SAMPLES, &samples);
  glGetIntegerv(GL_SAMPLE_BUFFERS, &samplebuffers);
  log_msg("[viewer] OpenGL MSAA samples: {}; sample buffers: {}", samples, samplebuffers);

  return window;
}

void Viewer::init_opengl(GLFWwindow *window, bool record_and_exit, string title) {
  glShadeModel(GL_SMOOTH);
  glClearColor(background, background, background, 0);  // background color
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  //glEnable(GL_LINE_SMOOTH);
  //glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
  glEnable(GL_MULTISAMPLE);  // alternative: GL_MULTISAMPLE_ARB
  glEnable(GL_CULL_FACE);

  fons_init();

  if (makesnapshots) {
    exit_on_eof = true;
  }

  if (record_and_exit) {
    makemovie   = true;
    exit_on_eof = true;
    recorder.start_recording(prm.movie_filename, prm.fps);
  }
}

void Viewer::setup_viewport() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  // drawing
  glLoadIdentity();

  glTranslatef(viewpoint[0], viewpoint[1], viewpoint[2]);
  glRotatef(rotate[0], 1, 0, 0);
  glRotatef(rotate[1], 0, 1, 0);
  glRotatef(rotate[2], 0, 0, 1);
}

void Viewer::run() {
  while (!glfwWindowShouldClose(window)) {
    glfwPollEvents();
    setup_viewport();
    display(rows_offset);
    glfwSwapBuffers(window);

    if (loop && (t > 6000)) {
      t = 1000;
    }

    if (t_end && (t > t_end)) {
      exit_viewer(EXIT_SUCCESS);
    }

    if (t % 10 * prm.tw * prm.frameskip == 0) {
      for (auto &row : rows) {
        row->recalibrateColormap(false);
      }
    }

    if (play) {
      if (makesnapshots) {
        snapshot(t);
      }
      if (makemovie) {
        recorder.add_frame();
      }

      t += prm.tw * prm.frameskip;
    }
  }

  exit_viewer(EXIT_SUCCESS);
}

void Viewer::display(Vec3 offset) {

  // Draw current timestamp at the top
  if (show_current_time) {
    Vec3 text_pos(offset[0], offset[1] - 15, 0);
    draw_text(text_pos, fmt::format("t = {}", t), 9.f + rows.size(), Colors::black, "normal",
              FONS_ALIGN_CENTER | FONS_ALIGN_BOTTOM);
  }

  offset -= Vec3(0, prm.Ny / 2 + 2 * Viewer::row_padding, 0);

  try {
    Vec3 offset_row = offset;
    for (auto &r : rows) {
      r->display(t, offset_row, play);
      offset_row -= r->height_px_factor * Vec3(0, prm.Ny + row_padding, 0);
    }
  } catch (std::ifstream::failure &) {
    if (exit_on_eof) {
      log_msg("[viewer] Exiting because of EOF");
      exit_viewer(EXIT_SUCCESS);
    }
    log_msg("[viewer] EOF, restarting viewer");

    for (auto &r : rows) {
      r->clearEOF();
      r->frameskipChanged(prm.frameskip, prm.frameskip);
    }
    t = 0;
  }

  ImGuiConnector::NewFrame();
  for (auto &gui : guis) {
    gui->display(t, offset);
  }
  ImGuiConnector::Render();
}

void Viewer::reshape_callback(GLFWwindow *window, int w, int h) {
  log_msg("[viewer] reshaped to {}x{}, zoom {}", w, h, zoom);

  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  auto wh            = get_total_rows_width_height();
  GLdouble halfwidth = wh.first / 2 * zoom;
  GLdouble height    = wh.second * zoom;

  glOrtho(-halfwidth, halfwidth, -height, 0, -1000, 1000);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void Viewer::cursor_position_callback(GLFWwindow *window, double xpos, double ypos) {
  static double old_xpos = xpos;
  static double old_ypos = ypos;

  double dx = xpos - old_xpos;
  double dy = ypos - old_ypos;

  if (mouse_button == GLFW_MOUSE_BUTTON_LEFT) {
    rotate[0] += dy * 0.2f;
    rotate[1] += dx * 0.2f;

    log_msg("[viewer] new rotate: {}", rotate);
  } else if (mouse_button == GLFW_MOUSE_BUTTON_RIGHT) {
    viewpoint[0] += dx * 0.2f;
    viewpoint[1] += -dy * 0.2f;

    log_msg("[viewer] new viewpoint: {}", viewpoint);
  }

  old_xpos = xpos;
  old_ypos = ypos;
}

void Viewer::scroll_callback(GLFWwindow *window, double xoffset, double yoffset) {

  if (ImGuiConnector::WantsScrollInput(window, xoffset, yoffset)) {
    // imgui wants the input, don't dispatch it to our app
    return;
  }

  if (yoffset > 0) {
    zoom /= 1.02;
  } else if (yoffset < 0) {
    zoom *= 1.02;
  }

  int width, height;
  glfwGetWindowSize(window, &width, &height);
  reshape_callback(window, width, height);
}

void Viewer::mouse_button_callback(GLFWwindow *window, int button, int action, int mods) {

  if (ImGuiConnector::WantsMouseInput(window, button, action, mods)) {
    // imgui wants the input, don't dispatch it to our app
    return;
  }

  if (action == GLFW_PRESS) {
    mouse_button = button;
  } else if (action == GLFW_RELEASE) {
    mouse_button = -1;
  }
}

void Viewer::key_callback(GLFWwindow *window, int key, int scancode, int action, int mods) {

  if (ImGuiConnector::WantsKeybourdInput(window, key, scancode, action, mods)) {
    // imgui wants the input, don't dispatch it to our app
    return;
  }

  if (key == GLFW_KEY_SPACE && action == GLFW_PRESS) {
    play = !play;
    if (play) {
      log_msg("[viewer] playing!");
    } else {
      log_msg("[viewer] paused!");
    }

  } else if ((key == GLFW_KEY_UP || key == GLFW_KEY_DOWN) && action != GLFW_RELEASE) {
    // Zoom in/out

    if (key == GLFW_KEY_UP)
      zoom -= 0.01;
    else
      zoom += 0.01;
    log_msg("[viewer] zoom = {}", zoom);

    int width, height;
    glfwGetWindowSize(window, &width, &height);
    reshape_callback(window, width, height);

  } else if (key == GLFW_KEY_LEFT && action != GLFW_RELEASE) {
    t -= prm.tw * prm.frameskip;
    log_msg("[viewer] t -= {}", prm.tw * prm.frameskip);
    if (t < 0) t = 0;
  } else if (key == GLFW_KEY_RIGHT && action != GLFW_RELEASE) {
    t += prm.tw * prm.frameskip;
    log_msg("[viewer] t += {}", prm.tw * prm.frameskip);

  } else if (key == GLFW_KEY_1 && action == GLFW_PRESS) {
    set_frameskip(1);

  } else if (key == GLFW_KEY_2 && action == GLFW_PRESS) {
    set_frameskip(2);

  } else if (key == GLFW_KEY_3 && action == GLFW_PRESS) {
    set_frameskip(3);

  } else if (key == GLFW_KEY_4 && action == GLFW_PRESS) {
    set_frameskip(5);

  } else if (key == GLFW_KEY_5 && action == GLFW_PRESS) {
    set_frameskip(8);

  } else if (key == GLFW_KEY_6 && action == GLFW_PRESS) {
    set_frameskip(15);

  } else if (key == GLFW_KEY_7 && action == GLFW_PRESS) {
    set_frameskip(20);

  } else if (key == GLFW_KEY_8 && action == GLFW_PRESS) {
    set_frameskip(40);

  } else if (key == GLFW_KEY_9 && action == GLFW_PRESS) {
    set_frameskip(60);

  } else if (key == GLFW_KEY_S) {
    if (action == GLFW_PRESS) {
      makesnapshots = true;
      log_msg("[viewer] Creating screenshots from now on for every frame until S button is released");

      if (!play) {
        snapshot(t);
      }
    } else if (action == GLFW_RELEASE) {
      makesnapshots = false;
      log_msg("[viewer] Stopped creating screenshots");
    }

  } else if (key == GLFW_KEY_M && action == GLFW_PRESS) {
    if (makemovie) {
      makemovie = false;
      recorder.stop_recording();
    } else {
      makemovie = true;
      recorder.start_recording(prm.movie_filename, prm.fps);
    }

  } else if (key == GLFW_KEY_L && action == GLFW_PRESS) {
    loop = !loop;
    log_msg("[viewer] looping is now set to {}", loop);

  } else if (key == GLFW_KEY_R && action == GLFW_PRESS) {
    for (auto &row : rows) {
      row->recalibrateColormap(true);
    }
    log_msg("[viewer] colormap recalibrated");

  } else if (key == GLFW_KEY_B && action == GLFW_PRESS) {
    t = 0;
    log_msg("[viewer] setting t = 0");

  } else if (key == GLFW_KEY_G && action == GLFW_PRESS) {
    // Hide all guis

    for (auto &gui : guis) {
      gui->show_window = !gui->show_window;
    }

  } else if (key == GLFW_KEY_I && action == GLFW_PRESS) {
    // Display GuiViewerControl gui (create if needed)

    auto get_guiviewer = []() -> GuiWindow * {
      for (auto &gui : guis) {
        if (GuiWindow *g = dynamic_cast<GuiViewerControl *>(gui.get())) {
          return g;
        }
      }
      return nullptr;
    };

    GuiWindow *gui = get_guiviewer();

    if (gui == nullptr) {
      std::shared_ptr<GuiWindow> g = make_shared<GuiViewerControl>(true);
      guis.emplace_back(g);

    } else {
      gui->show_window = !gui->show_window;
    }

  } else if (key == GLFW_KEY_F && action == GLFW_PRESS) {
    // Toggle Fullscreen

    static bool is_windowed = true;
    if (is_windowed) {  // go fullscreen
      GLFWmonitor *monitor     = glfwGetPrimaryMonitor();
      const GLFWvidmode *vmode = glfwGetVideoMode(monitor);
      glfwSetWindowMonitor(window, monitor, 0, 0, vmode->width, vmode->height, vmode->refreshRate);

      is_windowed = false;
    } else {  // quit fullscreen
      auto wh    = get_total_rows_width_height();
      int height = window_width * wh.second / wh.first;
      glfwSetWindowMonitor(window, nullptr, 0, 0, window_width, height, GLFW_DONT_CARE);

      is_windowed = true;
    }

  } else if (key == GLFW_KEY_Q || key == GLFW_KEY_ESCAPE) {

    log_msg("[viewer] ESC pressed, quitting");
    exit_viewer(EXIT_SUCCESS);
  }
}

void Viewer::set_frameskip(int frameskip) {
  int oldvalue  = prm.frameskip;
  prm.frameskip = frameskip;

  for (auto &r : rows) {
    r->frameskipChanged(oldvalue, prm.frameskip);
  }
  log_msg("[viewer] frameskip parameter changed from {} to {}", oldvalue, prm.frameskip);
}

void Viewer::exit_viewer(int return_code) {
  {
    fons_shutdown();

    rows.clear();

    ImGuiConnector::Shutdown();
    guis.clear();

    GLFWwindow *window = glfwGetCurrentContext();
    if (!window) glfwDestroyWindow(window);
    glfwTerminate();
  }

  if (return_code >= 0) std::exit(return_code);
}

Viewer::~Viewer() {
  exit_viewer(-1);
}