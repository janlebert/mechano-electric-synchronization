#pragma once

/*
 * Viewer for computed phase filaments
 */

#include "CLI/CLI.hpp"

#include "rowview_3D.h"

class RowView3DFilaments : public RowView3D {
 public:
  RowView3DFilaments(std::shared_ptr<SimulationDataStreams> _simDataStream)
      : RowView3D(_simDataStream, RateType::File, "") {
    rate->set_minmax_fixed({-1, 1});

    auto cmap_filaments = [](num x, const MinMax &minmax, bool use_transfer_fct) -> Color {
      x = (x - minmax.first) / (minmax.second - minmax.first);

      if (x < 0.49f) {
        return Colors::gray_light;
      } else if (x > 0.51f) {
        return Colors::gray_light;
      } else {
        return {0, 0, 0, 0};
      }
    };
    this->mechanics_color = cmap_filaments;

    draw_fiberorientation = false;

    height_px_factor = 0.9;
  };

  void display(long int t, Vec3 offset, bool play) override {

    if (play) {
      readBinaryFileElectro(t);
      readBinaryFileMechanics(t);
    }

    if (compute_rate) {
      rate->compute(t);
    }

    if (!label.empty()) {
      offset -= {0, 20, 0};
      drawLabel(Vec3(offset[0], offset[1] + 23, 0), label);
    }

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(0, 0, -(prm.Nz - 1));
    glDisable(GL_CULL_FACE);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_MULTISAMPLE);  // alternative: GL_MULTISAMPLE_ARB
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    auto zero = Vec3(0, 0, 0);

    Vec3 left_offset  = left_cell_pos(offset) + Vec3(prm.Nx / 3.f, 0, 0);
    Vec3 right_offset = right_cell_pos(offset) - Vec3(prm.Nx / 3.f, 0, 0);

    glPushMatrix();
    rotate3D_default(left_offset);
    draw3DMeshBorder(zero, 3, 2);
    drawExcitation3D(zero);
    draw3DMeshTopOuterBorder(zero);
    glPopMatrix();

    if (draw_colormap) {
      static GLuint ecm_tex = colormap_as_texture(excitation_color, true, {0.5, 1});
      drawColormap(ecm_tex, {0, 1}, left_offset - Vec3(0, 5 + prm.Ny / 2.f, 0), {70, 5}, 8);
    }

    draw_text(left_offset + Vec3(0, prm.Ny / 2 + 10, 0), "Electrical Activity", 9.f, Colors::black);

    glPushMatrix();
    rotate3D_default(right_offset);
    draw3DMeshBorder(zero, 3, 2);
    draw3DFilaments({&(rate->rate)}, pos3D, zero, {mechanics_color}, {rate->minmax()});
    drawExcitation3D(zero);
    draw3DMeshTopOuterBorder(zero);
    if (draw_fiberorientation) {
      drawZFiberOrientation({prm.spacing * prm.Ny - 10, -20, prm.spacing * (-prm.Nz / 2.f)}, 8);
    }
    glPopMatrix();

    draw_text(right_offset + Vec3(0, prm.Ny / 2 + 10, 0), "Filaments", 9.f, Colors::black);

    glEnable(GL_CULL_FACE);
    glPopMatrix();
  }

  void draw3DFilaments(vector<Array3<num> *> va,
                       Array3<Vec3> &pos3D,
                       const Vec3 &offset,
                       vector<Colormap::colormap> vcolormap,
                       vector<Colormap::MinMax> vminmax) {

    const bool alpha_transfer_fct = false;

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslate(offset);

    std::array<Vec3, 8> positions;
    std::array<Color, 8> colors;

    const auto position = [this, &pos3D](const IdxVec &pos) -> Vec3 { return h_inv * pos3D(pos); };
    const auto color    = [&](const IdxVec &idx) -> Color {
      if (va.size() == 1) {  // hotpath
        num val = va[0]->operator()(idx);
        return vcolormap[0](val, vminmax[0], alpha_transfer_fct);
      }

      Color result{0, 0, 0, 0};
      int count = 0;

      for (auto i = 0; i < va.size(); i++) {
        num val = va[i]->operator()(idx);
        Color c = vcolormap[i](val, vminmax[i], alpha_transfer_fct);

        if (std::abs(val) > 0.1) {
          result += c;
          count++;
        }
      }

      result = result / count;

      return result;
    };

    const auto colored_vertex_point = [&colors, &positions](int i) {
      glColor4fv(colors[i]);
      glVertex3nv(positions[i]);
    };

    const auto is_val_set = [&va](const IdxVec &pos) -> bool {
      for (const auto &v : va) {
        if (std::abs(v->operator()(pos)) > 0.1) {
          return true;
        }
      }
      return false;
    };

    for (int x = 0; x < Nx - 1; x++) {
      for (int y = 0; y < Ny - 1; y++) {
        for (int z = 0; z < Nz - 1; z++) {
          if (is_val_set({x, y, z})) {
            // clang-format off
            std::array<IdxVec, 8> xyz = {{{x,     y,     z},
                                          {x + 1, y,     z},
                                          {x,     y,     z + 1},
                                          {x + 1, y,     z + 1},
                                          {x,     y + 1, z},
                                          {x + 1, y + 1, z},
                                          {x,     y + 1, z + 1},
                                          {x + 1, y + 1, z + 1}}};
            // clang-format on

            for (int i = 0; i < 8; i++) {
              positions[i] = position(xyz[i]);
              colors[i]    = color(xyz[0]);
            }

            glBegin(GL_QUADS);

            /*
           *    6---7
           *   /|  /|
           *  2---3 |
           *  | 4-|-5
           *  |/  |/
           *  0---1
           *
           *  Sides:
           *  2---3  3---7  7---6  6---2
           *  |   |  |   |  |   |  |   |
           *  |   |  |   |  |   |  |   |
           *  0---1  1---5  5---4  4---0
           *
           *  Bottom/Top
           *  0---1  6---7
           *  |   |  |   |
           *  |   |  |   |
           *  4---5  2---3
           *
           */

            // Bottom
            colored_vertex_point(4);
            colored_vertex_point(5);
            colored_vertex_point(1);
            colored_vertex_point(0);

            // Sides
            colored_vertex_point(0);
            colored_vertex_point(1);
            colored_vertex_point(3);
            colored_vertex_point(2);

            colored_vertex_point(1);
            colored_vertex_point(5);
            colored_vertex_point(7);
            colored_vertex_point(3);

            colored_vertex_point(5);
            colored_vertex_point(4);
            colored_vertex_point(6);
            colored_vertex_point(7);

            colored_vertex_point(4);
            colored_vertex_point(0);
            colored_vertex_point(2);
            colored_vertex_point(6);

            // Top
            colored_vertex_point(2);
            colored_vertex_point(3);
            colored_vertex_point(7);
            colored_vertex_point(6);

            glEnd();
          }
        }
      }
    }

    glPopMatrix();
  }
};

class RowView3DFilamentsDA : public RowView3DFilaments {
 public:
  std::shared_ptr<Rate3D> rate_sync;
  std::shared_ptr<SimulationDataStreams> simData_sync;

  Array3<num> u3D_sync;
  Array3<Vec3> pos3D_sync;
  Colormap::colormap mechanics_color_sync = Colormap::IDL::RdBu::colormap;

  RowView3DFilamentsDA(std::shared_ptr<SimulationDataStreams> _simData_src,
                       std::shared_ptr<SimulationDataStreams> _simData_sync)
      : RowView3DFilaments(_simData_src),
        simData_sync(_simData_sync),
        u3D_sync(prm.dims),
        pos3D_sync(prm.dims) {
    ASSERT_ALWAYS(simData_sync);

    rate->set_minmax_fixed({-1, 1});

    rate_sync = make_rate(rt, pos3D, simData_sync.get());
    rate_sync->set_minmax_fixed({-1, 1});

    auto cmap_filaments = [](num x, const MinMax &minmax, bool use_transfer_fct) -> Color {
      x = (x - minmax.first) / (minmax.second - minmax.first);

      if (x < 0.49f) {
        return Colors::gray_light;
      } else if (x > 0.51f) {
        return Colors::gray_light;
      } else {
        return {0, 0, 0, 0};
      }
    };
    this->mechanics_color = cmap_filaments;

    auto cmap_filaments_sync = [](num x, const MinMax &minmax, bool use_transfer_fct) -> Color {
      x = (x - minmax.first) / (minmax.second - minmax.first);

      if (x < 0.49f) {
        return Colors::black;
      } else if (x > 0.51f) {
        return Colors::black;
      } else {
        return {0, 0, 0, 0};
      }
    };
    this->mechanics_color_sync = cmap_filaments_sync;

    Viewer::zoom     = 1.37279;
    height_px_factor = 0.53;
  };

  void display(long int t, Vec3 offset, bool play) override {

    if (play) {
      u3D.load(simData->u, t);
      pos3D.load(simData->pos, t);

      u3D_sync.load(simData_sync->u, t);
      pos3D_sync.load(simData_sync->pos, t);
    }

    if (compute_rate) {
      rate->compute(t);
      rate_sync->compute(t);
    }

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(0, 0, -(prm.Nz - 1));
    glDisable(GL_CULL_FACE);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_MULTISAMPLE);  // alternative: GL_MULTISAMPLE_ARB
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    auto zero = Vec3(0, 0, 0);

    Vec3 left_offset   = left_cell_pos(offset) - Vec3(0.4*Nx, 0, 0);
    Vec3 center_offset = offset;
    Vec3 right_offset  = right_cell_pos(offset) + Vec3(0.4*Nx, 0, 0);

    glPushMatrix();
    rotate3D_default(left_offset);
    draw3DMeshBorder(zero, 3, 2);
    drawExcitation3D(zero);
    draw3DMeshTopOuterBorder(zero);
    glPopMatrix();

    if (draw_colormap) {
      static GLuint ecm_tex = colormap_as_texture(excitation_color, true, {0.5, 1});
      drawColormap(ecm_tex, {0, 1}, left_offset - Vec3(0, 5 + prm.Ny / 2.f, 0), {70, 5}, 8);
    }

    draw_text(left_offset + Vec3(0, prm.Ny / 2 + 5, 0), "Original", 11.f, Colors::black);

    glPushMatrix();
    rotate3D_default(center_offset);
    draw3DMeshBorder(pos3D_sync, zero, 3, 2);
    drawArray3D(u3D_sync, pos3D_sync, zero, excitation_color, {-1, 1});
    draw3DMeshTopOuterBorder(pos3D_sync, zero);
    glPopMatrix();

    draw_text(center_offset + Vec3(0, prm.Ny / 2 + 5, 0), "Reconstruction", 11.f, Colors::black);

    if (draw_colormap) {
      static GLuint ecm_tex = colormap_as_texture(excitation_color, true, {0.5, 1});
      drawColormap(ecm_tex, {0, 1}, center_offset - Vec3(0, 5 + prm.Ny / 2.f, 0), {70, 5}, 8);
    }


    glPushMatrix();
    rotate3D_default(right_offset);
    draw3DMeshBorder(zero, 3, 2);
    draw3DFilaments({&(rate_sync->rate), &(rate->rate)}, pos3D, zero,
                    {mechanics_color_sync, mechanics_color}, {rate_sync->minmax(), rate->minmax()});
    draw3DMeshTopOuterBorder(zero);
    if (draw_fiberorientation) {
      drawZFiberOrientation({prm.spacing * prm.Ny - 10, -20, prm.spacing * (-prm.Nz / 2.f)}, 8);
    }
    glPopMatrix();

    draw_text(right_offset + Vec3(0, prm.Ny / 2 + 5, 0), "Filaments", 11.f, Colors::black);

    draw_text(right_offset - Vec3(-15, prm.Ny / 2 + 7, 0), "Reconstruction", 8.f, Colors::black,
              "normal", FONS_ALIGN_LEFT | FONS_ALIGN_BASELINE);
    draw_text(right_offset - Vec3(+15, prm.Ny / 2 + 7, 0), "Original", 8.f, Colors::gray_light,
              "normal", FONS_ALIGN_RIGHT | FONS_ALIGN_BASELINE);

    glEnable(GL_CULL_FACE);
    glPopMatrix();
  }
};
