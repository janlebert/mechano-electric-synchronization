#include "colormap.h"
#include "colormap_data.h"
#include "helper.h"

/*
 * Collection of selected colormaps, colormap functions extracted from https://github.com/kbinani/glsl-colormap
 */
namespace Colormap {

  float transferfct(num x, bool use_transfer_fct) {
    const auto transfer = [](const num &v) -> float {
      return clamp<float>(std::abs(v) * 3 * (1 + std::abs(v)));
    };
    return use_transfer_fct ? transfer(x - static_cast<num>(0.5)) : 1;
  }


  // See https://github.com/kbinani/glsl-colormap#idl
  namespace IDL {
    namespace PRGn {
      float colormap_red(num x) {
        if (x < 0.09963276982307434) {
          return 5.56064615384614E+02 * x + 6.34200000000000E+01;
        } else if (x < 0.4070911109447479) {
          return ((-1.64134573262743E+03 * x + 1.26126075878571E+03) * x + 8.30228593437549E+01) *
                     x +
                 9.96536529647110E+01;
        } else if (x < 0.5013306438922882) {
          return 1.64123076923114E+02 * x + 1.64926153846145E+02;
        } else if (x < 0.9049346148967743) {
          return ((((-4.17783076764345E+04 * x + 1.55735420068591E+05) * x - 2.27018068370619E+05) *
                       x +
                   1.61149115838926E+05) *
                      x -
                  5.60588504546212E+04) *
                     x +
                 7.93919652573346E+03;
        } else {
          return -2.67676923076906E+02 * x + 2.68590769230752E+02;
        }
      }

      float colormap_green(num x) {
        if (x < 0.1011035144329071) {
          return 4.30627692307691E+02 * x - 1.56923076923038E-01;
        } else if (x < 0.5013851821422577) {
          return ((2.21240993583109E+02 * x - 7.23499016773187E+02) * x + 8.74292145995924E+02) * x -
                 3.78460594811949E+01;
        } else {
          return ((((-8.82260255008935E+03 * x + 3.69735516719018E+04) * x - 5.94940784200438E+04) *
                       x +
                   4.54236515662453E+04) *
                      x -
                  1.66043372157228E+04) *
                     x +
                 2.59449114260420E+03;
        }
      }

      float colormap_blue(num x) {
        if (x < 0.50031378865242) {
          return ((((1.32543265346288E+04 * x - 1.82876583834065E+04) * x + 9.17087085537958E+03) *
                       x -
                   2.45909850441496E+03) *
                      x +
                  7.42893247681885E+02) *
                     x +
                 7.26668497072812E+01;
        } else if (x < 0.609173446893692) {
          return -3.50141636141726E+02 * x + 4.22147741147797E+02;
        } else {
          return ((1.79776073728688E+03 * x - 3.80142452792079E+03) * x + 2.10214624189039E+03) * x -
                 6.74426111651015E+01;
        }
      }

      Color _colormap(num x, bool use_transfer_fct) {
        auto r = clamp(colormap_red(x) / 255.f);
        auto g = clamp(colormap_green(x) / 255.f);
        auto b = clamp(colormap_blue(x) / 255.f);
        auto a = transferfct(x, use_transfer_fct);
        return {r, g, b, a};
      }

      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct) {
        return _colormap((x - minmax.first) / (minmax.second - minmax.first), use_transfer_fct);
      }
    }  // namespace PRGn

    namespace RdBu {
      float colormap_red(float x) {
        if (x < 0.09771832105856419) {
          return 7.60263247863246E+02 * x + 1.02931623931624E+02;
        } else if (x < 0.3017162107441106) {
          return (-2.54380938558548E+02 * x + 4.29911571188803E+02) * x + 1.37642085716717E+02;
        } else if (x < 0.4014205790737471) {
          return 8.67103448276151E+01 * x + 2.18034482758611E+02;
        } else if (x < 0.5019932233215039) {
          return -6.15461538461498E+01 * x + 2.77547692307680E+02;
        } else if (x < 0.5969483882550937) {
          return -3.77588522588624E+02 * x + 4.36198819698878E+02;
        } else if (x < 0.8046060096654594) {
          return (-6.51345897546620E+02 * x + 2.09780968434337E+02) * x + 3.17674951640855E+02;
        } else {
          return -3.08431855203590E+02 * x + 3.12956742081421E+02;
        }
      }

      float colormap_green(float x) {
        if (x < 0.09881640500975222) {
          return 2.41408547008547E+02 * x + 3.50427350427364E-01;
        } else if (x < 0.5000816285610199) {
          return ((((1.98531871433258E+04 * x - 2.64108262469187E+04) * x + 1.10991785969817E+04) *
                       x -
                   1.92958444776211E+03) *
                      x +
                  8.39569642882186E+02) *
                     x -
                 4.82944517518776E+01;
        } else if (x < 0.8922355473041534) {
          return (((6.16712686949223E+03 * x - 1.59084026055125E+04) * x + 1.45172137257997E+04) *
                      x -
                  5.80944127411621E+03) *
                     x +
                 1.12477959061948E+03;
        } else {
          return -5.28313797313699E+02 * x + 5.78459299959206E+02;
        }
      }

      float colormap_blue(float x) {
        if (x < 0.1033699568661857) {
          return 1.30256410256410E+02 * x + 3.08518518518519E+01;
        } else if (x < 0.2037526071071625) {
          return 3.38458128078815E+02 * x + 9.33004926108412E+00;
        } else if (x < 0.2973267734050751) {
          return (-1.06345054944861E+02 * x + 5.93327252747168E+02) * x - 3.81852747252658E+01;
        } else if (x < 0.4029109179973602) {
          return 6.68959706959723E+02 * x - 7.00740740740798E+01;
        } else if (x < 0.5006715489526758) {
          return 4.87348695652202E+02 * x + 3.09898550724286E+00;
        } else if (x < 0.6004396902588283) {
          return -6.85799999999829E+01 * x + 2.81436666666663E+02;
        } else if (x < 0.702576607465744) {
          return -1.81331701891043E+02 * x + 3.49137263626287E+02;
        } else if (x < 0.9010407030582428) {
          return (2.06124143164576E+02 * x - 5.78166906665595E+02) * x + 5.26198653917172E+02;
        } else {
          return -7.36990769230737E+02 * x + 8.36652307692262E+02;
        }
      }

      Color _colormap(num x, bool use_transfer_fct) {
        auto r = clamp(colormap_red(x) / 255.f);
        auto g = clamp(colormap_green(x) / 255.f);
        auto b = clamp(colormap_blue(x) / 255.f);
        auto a = transferfct(x, use_transfer_fct);
        return {r, g, b, a};
      }

      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct) {
        return _colormap((x - minmax.first) / (minmax.second - minmax.first), use_transfer_fct);
      }
    }  // namespace RdBu

    namespace PiYG {
      float colormap_red(num x) {
        if (x < 0.09843078255653381) {
          return 5.57627692307694E+02 * x + 1.42135384615385E+02;
        } else if (x < 0.4093809425830841) {
          return ((-4.21748915649019E+02 * x + 1.28054196831998E+01) * x + 2.64504106766935E+02) *
                     x +
                 1.71265909327078E+02;
        } else if (x < 0.5037343473981705) {
          return -6.54538461538185E+01 * x + 2.79554615384612E+02;
        } else if (x < 0.5982368290424347) {
          return -1.66852258852308E+02 * x + 3.30632478632496E+02;
        } else {
          return ((1.82001891024969E+03 * x - 4.20447326861499E+03) * x + 2.68838861198902E+03) * x -
                 2.62418693972160E+02;
        }
      }

      float colormap_green(num x) {
        if (x < 0.101902037858963) {
          return 2.72322735042735E+02 * x + 5.21367521367536E-01;
        } else if (x < 0.5059849917888641) {
          return ((6.81035433115437E+02 * x - 1.71408042362240E+03) * x + 1.36671536460816E+03) * x -
                 9.39210546594673E+01;
        } else if (x < 0.5954320132732391) {
          return -2.72768472906136E+01 * x + 2.60800985221660E+02;
        } else {
          return ((1.09164194296742E+03 * x - 3.01508808799416E+03) * x + 2.33004497173996E+03) * x -
                 3.04306745740377E+02;
        }
      }

      float colormap_blue(num x) {
        if (x < 0.5011215507984161) {
          return (((((-3.44764954376220E+04 * x + 6.98813026428223E+04) * x - 4.87748659515380E+04) *
                        x +
                    1.31832279253005E+04) *
                       x -
                   1.26691288614273E+03) *
                      x +
                  4.73465709444135E+02) *
                     x +
                 8.21916531938477E+01;
        } else if (x < 0.5958432303492089) {
          return -3.80379731379794E+02 * x + 4.37472934472961E+02;
        } else if (x < 0.790071576833725) {
          return -7.13383710407293E+02 * x + 6.35891101055846E+02;
        } else {
          return (1.19760697610430E+03 * x - 2.36001183205723E+03) * x + 1.18928322234544E+03;
        }
      }

      Color _colormap(num x, bool use_transfer_fct) {
        auto r = clamp(colormap_red(x) / 255.f);
        auto g = clamp(colormap_green(x) / 255.f);
        auto b = clamp(colormap_blue(x) / 255.f);
        auto a = transferfct(x, use_transfer_fct);
        return {r, g, b, a};
      }

      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct) {
        return _colormap((x - minmax.first) / (minmax.second - minmax.first), use_transfer_fct);
      }
    }  // namespace PiYG

    namespace Oranges {
      float colormap_red(num x) {
        if (x < 0.4798405468463898) {
          return 255.0;
        } else if (x < 0.7629524491886985) {
          return (-3.67617638383468E+02 * x + 3.17332748024787E+02) * x + 1.85373720793787E+02;
        } else {
          return (3.68357233392831E+02 * x - 1.00617951362078E+03) * x + 7.66695019519326E+02;
        }
      }

      float colormap_green(num x) {
        if (x < 0.748539247687408) {
          return (((-8.92644295264035E+01 * x + 3.93421870424412E+02) * x - 4.73834129104390E+02) *
                      x -
                  5.60962544745416E+01) *
                     x +
                 2.43354168263028E+02;
        } else {
          return (1.06683260838348E+02 * x - 3.18020138166420E+02) * x + 2.51126712492908E+02;
        }
      }

      float colormap_blue(num x) {
        if (x < 0.76) {
          return ((7.32034492098544E+02 * x - 7.55283914444663E+02) * x - 1.53168890861198E+02) * x +
                 2.33567667053916E+02;
        } else {
          return 1.23702752385982E+01 * x - 8.09423081765692E+00;
        }
      }

      Color _colormap(num x, bool use_transfer_fct) {
        auto r = clamp(colormap_red(x) / 255.f);
        auto g = clamp(colormap_green(x) / 255.f);
        auto b = clamp(colormap_blue(x) / 255.f);
        auto a = transferfct(x, use_transfer_fct);
        return {r, g, b, a};
      }

      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct) {
        return _colormap((x - minmax.first) / (minmax.second - minmax.first), use_transfer_fct);
      }
    }  // namespace Oranges
  }    // namespace IDL


  namespace MATLAB {
    namespace HSV {
      float colormap_red(num x) {
        if (x < 0.5) {
          return -6.0 * x + 67.0 / 32.0;
        } else {
          return 6.0 * x - 79.0 / 16.0;
        }
      }

      float colormap_green(num x) {
        if (x < 0.4) {
          return 6.0 * x - 3.0 / 32.0;
        } else {
          return -6.0 * x + 79.0 / 16.0;
        }
      }

      float colormap_blue(num x) {
        if (x < 0.7) {
          return 6.0 * x - 67.0 / 32.0;
        } else {
          return -6.0 * x + 195.0 / 32.0;
        }
      }

      Color _colormap(num x, bool use_transfer_fct) {
        auto r = clamp(colormap_red(x));
        auto g = clamp(colormap_green(x));
        auto b = clamp(colormap_blue(x));
        auto a = transferfct(x, use_transfer_fct);
        return {r, g, b, a};
      }

      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct) {
        return _colormap((x - minmax.first) / (minmax.second - minmax.first), use_transfer_fct);
      }
    }  // namespace HSV

    namespace seismic {
      num colormap_f(num x) { return ((-2010.0 * x + 2502.5950459) * x - 481.763180924) / 255.0; }

      float colormap_red(num x) {
        if (x < 0.0) {
          return 3.0 / 255.0;
        } else if (x < 0.238) {
          return ((-1810.0 * x + 414.49) * x + 3.87702) / 255.0;
        } else if (x < 51611.0 / 108060.0) {
          return (344441250.0 / 323659.0 * x - 23422005.0 / 92474.0) / 255.0;
        } else if (x < 25851.0 / 34402.0) {
          return 1.0;
        } else if (x <= 1.0) {
          return (-688.04 * x + 772.02) / 255.0;
        } else {
          return 83.0 / 255.0;
        }
      }

      float colormap_green(num x) {
        if (x < 0.0) {
          return 0.0;
        } else if (x < 0.238) {
          return 0.0;
        } else if (x < 51611.0 / 108060.0) {
          return colormap_f(x);
        } else if (x < 0.739376978894039) {
          num xx = x - 51611.0 / 108060.0;
          return ((-914.74 * xx - 734.72) * xx + 255.) / 255.0;
        } else {
          return 0.0;
        }
      }

      float colormap_blue(num x) {
        if (x < 0.0) {
          return 19.0 / 255.0;
        } else if (x < 0.238) {
          num xx = x - 0.238;
          return (((1624.6 * xx + 1191.4) * xx + 1180.2) * xx + 255.0) / 255.0;
        } else if (x < 51611.0 / 108060.0) {
          return 1.0;
        } else if (x < 174.5 / 256.0) {
          return (-951.67322673866 * x + 709.532730938451) / 255.0;
        } else if (x < 0.745745353439206) {
          return (-705.250074130877 * x + 559.620050530617) / 255.0;
        } else if (x <= 1.0) {
          return ((-399.29 * x + 655.71) * x - 233.25) / 255.0;
        } else {
          return 23.0 / 255.0;
        }
      }

      Color _colormap(num x, bool use_transfer_fct) {
        auto r = colormap_red(x);
        auto g = colormap_green(x);
        auto b = colormap_blue(x);
        auto a = transferfct(x, use_transfer_fct);
        return {r, g, b, a};
      }

      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct) {
        return _colormap((x - minmax.first) / (minmax.second - minmax.first), use_transfer_fct);
      }
    }  // namespace seismic

    namespace jet {
      float colormap_red(num x) {
        if (x < 0.7) {
          return 4.0 * x - 1.5;
        } else {
          return -4.0 * x + 4.5;
        }
      }

      float colormap_green(num x) {
        if (x < 0.5) {
          return 4.0 * x - 0.5;
        } else {
          return -4.0 * x + 3.5;
        }
      }

      float colormap_blue(num x) {
        if (x < 0.3) {
          return 4.0 * x + 0.5;
        } else {
          return -4.0 * x + 2.5;
        }
      }

      Color _colormap(num x, bool use_transfer_fct) {
        auto r = clamp(colormap_red(x));
        auto g = clamp(colormap_green(x));
        auto b = clamp(colormap_blue(x));
        auto a = transferfct(x, use_transfer_fct);
        return {r, g, b, a};
      }

      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct) {
        return _colormap((x - minmax.first) / (minmax.second - minmax.first), use_transfer_fct);
      }
    }  // namespace jet
  }    // namespace MATLAB

  // See https://matplotlib.org/cmocean/
  namespace cmocean {
    namespace curl {
      Color _colormap(num x, bool use_transfer_fct) {
        const auto idx = std::lrint(clamp<num>(x, 0, 1) * (curl_rgb.size() - 1));
        auto rgb       = curl_rgb[idx];
        auto a         = transferfct(x, use_transfer_fct);

        return {rgb[0], rgb[1], rgb[2], a};
      }

      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct) {
        return _colormap((x - minmax.first) / (minmax.second - minmax.first), use_transfer_fct);
      }

      Color colormap_inv(num x, const MinMax &minmax, bool use_transfer_fct) {
        return _colormap((x - minmax.second) / (minmax.first - minmax.second), use_transfer_fct);
      }
    }  // namespace curl

    namespace delta {
      Color _colormap(num x, bool use_transfer_fct) {
        const auto idx = std::lrint(clamp<num>(x, 0, 1) * (delta_rgb.size() - 1));
        auto rgb       = delta_rgb[idx];
        auto a         = transferfct(x, use_transfer_fct);

        return {rgb[0], rgb[1], rgb[2], a};
      }

      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct) {
        return _colormap((x - minmax.first) / (minmax.second - minmax.first), use_transfer_fct);
      }

      Color colormap_inv(num x, const MinMax &minmax, bool use_transfer_fct) {
        return _colormap((x - minmax.second) / (minmax.first - minmax.second), use_transfer_fct);
      }
    }  // namespace delta

    namespace balance {
      Color _colormap(num x, bool use_transfer_fct) {
        const auto idx = std::lrint(clamp<num>(x, 0, 1) * (balance_rgb.size() - 1));
        auto rgb       = balance_rgb[idx];
        auto a         = transferfct(x, use_transfer_fct);

        return {rgb[0], rgb[1], rgb[2], a};
      }

      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct) {
        return _colormap((x - minmax.first) / (minmax.second - minmax.first), use_transfer_fct);
      }
    }  // namespace balance

    namespace phase {
      Color _colormap(num x, bool use_transfer_fct) {
        const auto idx = std::lrint(clamp<num>(x, 0, 1) * (phase_rgb.size() - 1));
        auto rgb       = phase_rgb[idx];
        auto a         = transferfct(x, use_transfer_fct);

        return {rgb[0], rgb[1], rgb[2], a};
      }

      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct) {
        return _colormap((x - minmax.first) / (minmax.second - minmax.first), use_transfer_fct);
      }
    }  // namespace phase

    namespace dense {
      Color _colormap(num x, bool use_transfer_fct) {
        const auto idx = std::lrint(clamp<num>(x, 0, 1) * (dense_rgb.size() - 1));
        auto rgb       = dense_rgb[idx];
        auto a         = transferfct(x + static_cast<num>(0.5), use_transfer_fct);

        return {rgb[0], rgb[1], rgb[2], a};
      }

      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct) {
        return _colormap(x / minmax.second, use_transfer_fct);
      }
    }  // namespace dense

    namespace algae {
      Color _colormap(num x, bool use_transfer_fct) {
        const auto idx = std::lrint(clamp<num>(x, 0, 1) * (algae_rgb.size() - 1));
        auto rgb       = algae_rgb[idx];
        auto a         = transferfct(x + static_cast<num>(0.5), use_transfer_fct);

        return {rgb[0], rgb[1], rgb[2], a};
      }

      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct) {
        return _colormap(x / minmax.second, use_transfer_fct);
      }
    }  // namespace algae
  }    // namespace cmocean
}  // namespace Colormap
