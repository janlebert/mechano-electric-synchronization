#define FONS_STATIC
#define FONTSTASH_IMPLEMENTATION    // Expands implementation
#define GLFONTSTASH_IMPLEMENTATION  // Expands implementation

#include "gl_helpers.h"

#include "fontstash/src/glfontstash.h"

#include "lodepng/lodepng.h"

void glfw_error_callback(int error, const char *description) {
  log_msg(std::cout, -1, "[viewer] [OpenGL ERROR] Code {}: {}", error, description);
}

namespace {
  FONScontext *fons = nullptr;
  int fontNormal    = FONS_INVALID;
  int fontItalic    = FONS_INVALID;
  int fontBold      = FONS_INVALID;

  void expandAtlas(FONScontext *stash) {
    int w = 0, h = 0;
    fonsGetAtlasSize(stash, &w, &h);
    if (w < h)
      w *= 2;
    else
      h *= 2;
    fonsExpandAtlas(stash, w, h);
  }

  void stashError(void *uptr, int error, int val) {
    (void)uptr;
    FONScontext *stash = (FONScontext *)uptr;
    switch (error) {
      case FONS_ATLAS_FULL:
        WARN_ERROR("fons atlas full, expanding atlas");
        expandAtlas(stash);
        break;
      case FONS_SCRATCH_FULL:
        WARN_ERROR("fons scratch full, tried to allocate {} has {}", val, FONS_SCRATCH_BUF_SIZE);
        break;
      case FONS_STATES_OVERFLOW:
        WARN_ERROR("fons states overflow");
        break;
      case FONS_STATES_UNDERFLOW:
        WARN_ERROR("fons states underflow");
        break;
    }
  }
}  // namespace


void fons_init() {
  if (fons) glfonsDelete(fons);

  fons = glfonsCreate(512, 512, FONS_ZERO_BOTTOMLEFT);

  ASSERT_ALWAYS(fons != nullptr, "fontstash creation failed");

  fonsSetErrorCallback(fons, stashError, fons);

  fontNormal = fonsAddFont(fons, "sans", VENDOR_PWD "DejaVuSerif.ttf");
  fontItalic = fonsAddFont(fons, "sans-italic", VENDOR_PWD "DejaVuSerif-Italic.ttf");
  fontBold   = fonsAddFont(fons, "sans-bold", VENDOR_PWD "DejaVuSerif-Bold.ttf");

  ASSERT_ALWAYS(fontNormal != FONS_INVALID && fontItalic != FONS_INVALID && fontBold != FONS_INVALID,
                "failure to add fonts to fontstash");
}

void fons_shutdown() {
  glfonsDelete(fons);
}

void draw_text(const Vec3 &pos,
               const string &str,
               float size,
               const Color &color,
               const string &fonttype,
               int align) {

  ASSERT_ALWAYS(fons != nullptr, "draw_text() called when fons == nulltpr. Did you fons_init() ?");

  unsigned int c = glfonsRGBA(255 * color[0], 255 * color[1], 255 * color[2], 255 * color[3]);

  int f;
  if (fonttype == "normal") {
    f = fontNormal;
  } else if (fonttype == "italic") {
    f = fontItalic;
  } else if (fonttype == "bold") {
    f = fontBold;
  } else {
    FATAL_ERROR("invalid fonttype selected for draw_text()");
  }

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glTranslatef(0, 0, pos[2]);

  float scale = 8;  // for better looking fonts at small sizes
  glScalef(1.f / scale, 1.f / scale, 1);

  fonsClearState(fons);
  fonsSetSize(fons, scale * size);
  fonsSetAlign(fons, align);
  fonsSetFont(fons, f);
  fonsSetColor(fons, c);

  fonsDrawText(fons, scale * pos[0], scale * pos[1], str.c_str(), nullptr);
  glPopMatrix();
}

Vector3<float> get_canvas_draw_pos() {
  array<float, 16> matModelView{};
  glGetFloatv(GL_MODELVIEW_MATRIX, matModelView.data());
  return {matModelView[12], matModelView[13], matModelView[14]};
}

void draw_line(const Vec3 &start, const Vec3 &end, const LineStyle &style) {
  glLineWidth(style.width);
  glBegin(GL_LINES);
  glColor4fv(style.color);
  glVertex3nv(start);
  glVertex3nv(end);
  glEnd();
}

void draw_triangle(Vec3 pos0, Color c0, Vec3 pos1, Color c1, Vec3 pos2, Color c2) {
  glBegin(GL_TRIANGLES);
  glColor4fv(c0);
  glVertex3nv(pos0);
  glColor4fv(c1);
  glVertex3nv(pos1);
  glColor4fv(c2);
  glVertex3nv(pos2);
  glEnd();
}

void draw_3D_arrow(Vec3 pos, Vec3 forward, float head_size, LineStyle linestyle) {
  const num arrowStep = 18;

  Vec3 from = pos;
  Vec3 to   = pos + forward;

  auto orthogonalBasis = [](Vec3 v) -> auto {
    Vec3 up, left;
    if (abs(v[2]) > 0.7) {
      num lenSqr = v[1] * v[1] + v[2] * v[2];
      num invLen = 1.f / sqrt(lenSqr);

      up   = {0, v[2] * invLen, -v[1] * invLen};
      left = {lenSqr * invLen, -v[0] * v[2], v[0] * v[1]};
    } else {
      num lenSqr = v[0] * v[0] + v[1] * v[1];
      num invLen = 1.f / sqrt(lenSqr);

      left = {-v[1] * invLen, v[0] * invLen, 0};
      up   = {-v[2] * left[1], v[2] * left[0], lenSqr * invLen};
    }

    return std::make_pair(up, left);
  };


  draw_line(from, to, linestyle);

  // Aux vectors to compute the arrowhead
  forward.normalize();
  Vec3 up, right;
  std::tie(up, right) = orthogonalBasis(forward);
  forward *= head_size;

  float degrees = 0;
  for (int i = 0; degrees < 360; degrees += arrowStep, ++i) {
    float scale1 = 0.5f * head_size * std::cos(degrees);
    float scale2 = 0.5f * head_size * std::sin(degrees);
    Vec3 v1      = to - forward + right * scale1 + up * scale2;

    float scale3 = 0.5f * head_size * std::cos(degrees + arrowStep);
    float scale4 = 0.5f * head_size * std::sin(degrees + arrowStep);
    Vec3 v2      = to - forward + right * scale3 + up * scale4;

    draw_line(v1, to, linestyle);
    draw_line(v1, v2, linestyle);
  }
}

GLuint load_png_as_texture(string png_filename) {
  GLuint texture;
  vector<GLubyte> background_img;
  unsigned png_width, png_height;

  unsigned error = lodepng::decode(background_img, png_width, png_height, png_filename);
  CHECK_ALWAYS(!error, "png decoder error {} for file \"{}\": {}", error, png_filename,
               lodepng_error_text(error));

  glGenTextures(1, &texture);

  // select our current texture
  glBindTexture(GL_TEXTURE_2D, texture);

  // texture should tile
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  // select modulate to mix texture with color for shading
  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, png_width, png_height, 0, GL_RGBA, GL_UNSIGNED_BYTE,
               background_img.data());

  return texture;
}

GLuint colormap_as_texture(Colormap::colormap cm, bool use_transfer_fct, Colormap::MinMax minmax) {
  ASSERT_ALWAYS(minmax.first >= 0 && minmax.second <= 1);

  constexpr int height = 1024;
  constexpr int width  = 100;

  std::array<std::array<unsigned int, height>, width> data{};

  // scale [0, height - 1] -> [mm.first, mm.second]
  auto rescale = [&minmax, &height](int x) -> num {
    const num v = x / static_cast<num>(height - 1);
    return (minmax.second - minmax.first) * v + minmax.first;
  };

  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      auto c           = cm(rescale(j), {0, 1}, use_transfer_fct) * 255;
      data.at(i).at(j) = glfonsRGBA(c[0], c[1], c[2], c[3]);
    }
  }

  GLuint texture;
  glGenTextures(1, &texture);

  // select our current texture
  glBindTexture(GL_TEXTURE_2D, texture);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
  glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, Colors::black);

  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  // select modulate to mix texture with color for shading
  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, height, width, 0, GL_RGBA, GL_UNSIGNED_BYTE,
               static_cast<GLvoid *>(data.data()));

  return texture;
}

void snapshot(long int frame) {
  GLFWwindow *window = glfwGetCurrentContext();
  int width, height;
  glfwGetWindowSize(window, &width, &height);

  string filename_prefix = "";
  string filename        = fmt::format("{}{:08d}.png", filename_prefix, frame);
  log_msg(std::cout, -1, "[viewer] saving frame {} to {}", frame, filename);


  GLenum gl_px_format            = GL_RGB;
  LodePNGColorType png_px_format = LodePNGColorType::LCT_RGB;
  const unsigned pix_byte_count  = 3;

  /* alternatively write out with alpha */
  //GLenum gl_px_format            = GL_RGBA;
  //LodePNGColorType png_px_format = LodePNGColorType::LCT_RGBA;
  //const unsigned pix_byte_count  = 4;


  vector<GLubyte> image(width * height * pix_byte_count);
  glReadPixels(0, 0, width, height, gl_px_format, GL_UNSIGNED_BYTE, image.data());

  // flip y
  for (int i = 0; i < height / 2; i++) {
    std::swap_ranges(image.begin() + i * width * pix_byte_count,
                     image.begin() + (i + 1) * width * pix_byte_count,
                     image.begin() + (height - i - 1) * width * pix_byte_count);
  }

  // Save as png
  unsigned error = lodepng::encode(filename, image, width, height, png_px_format);

  CHECK_ALWAYS(!error, "snapshot png encoder error {}: {}", error, lodepng_error_text(error));
}
