#pragma once

#ifdef _WIN32  // Windows 32 and 64 bit
#include <windows.h>

inline FILE* popen(const char* command, const char* type) {
  return _popen(command, type);
}

inline void pclose(FILE* file) {
  _pclose(file);
}
#endif

#include <stdio.h>

#include "GLFW/glfw3.h"

#include "helper.h"

class VideoRecorder {

  int width    = 0;
  int height   = 0;
  FILE* ffmpeg = nullptr;
  vector<GLubyte> buffer;

  string ffmpeg_encoder_args() {
    if (glfwExtensionSupported("GL_NVX_nvenc_interop")) {
      // if Nvidia NVENC supported, use it instead of cpu encoding
      return "-c:v h264_nvenc -preset slow -profile:v high -rc vbr_hq -qmin:v 19 -qmax:v 21 -b:v 4M -maxrate:v 10M"s;
    } else {
      // libx264 encoding preset, one of: ultrafast, superfast, veryfast, faster, fast, medium, slow, slower, veryslow
      string preset = "slow";
      // codec &  quality. Range is logarithmic 0 (lossless) to 51 (worst quality). Default is 23.
      unsigned quality = 18;
      return fmt::format("-c:v libx264 -preset {preset} -crf {quality:d}",
                         fmt::arg("preset", preset), fmt::arg("quality", quality));
    }
  }

 public:
  string videotitle = "";

  VideoRecorder() = default;

  ~VideoRecorder() { stop_recording(); }

  void start_recording(const string& filename, int fps = 15) {
    if (ffmpeg) return;  // if already recording, silently return

    ASSERT_ALWAYS(fps > 0);

    log_msg("[viewer] Starting to record movie");

    GLFWwindow* window = glfwGetCurrentContext();
    glfwGetWindowSize(window, &width, &height);

    buffer = vector<GLubyte>(width * height * 4ul);

    string cmd = fmt::format(
        "ffmpeg -f rawvideo -pix_fmt rgba -framerate {fps:d} -s {width}x{height} -i - "
        "-y -threads 0 {encoder_args} -pix_fmt yuv420p "
        "-vf \"[in]vflip,scale=trunc(iw/2)*2:trunc(ih/2)*2[out]\" "  // output colorspace and filters, ensure height and width are divisible by 2
        "-metadata title=\"{title}\" {filename}",
        fmt::arg("fps", fps), fmt::arg("width", width), fmt::arg("height", height),
        fmt::arg("encoder_args", ffmpeg_encoder_args()), fmt::arg("title", videotitle),
        fmt::arg("filename", filename));

    ffmpeg = popen(cmd.c_str(), "w");
    ASSERT_ALWAYS(ffmpeg, "unable to open ffmpeg with popen(), can't record output");
  }

  void add_frame() {
    ASSERT_ALWAYS(ffmpeg,
                  "VideoRecorder::start_recording has to be called before using ::add_frame!");

    glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer.data());
    fwrite(buffer.data(), sizeof(GLubyte), buffer.size(), ffmpeg);
  }

  void stop_recording() {
    if (ffmpeg) {
      log_msg("[viewer] Stopping movie recording …");
      pclose(ffmpeg);
      ffmpeg = nullptr;
    }
  }
};