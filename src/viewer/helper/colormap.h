#pragma once

#include "vectors.h"

using Color = Vector4<float>;

/*
 * Collection of selected colormaps, colormap functions extracted from https://github.com/kbinani/glsl-colormap
 */
namespace Colormap {
  using MinMax = std::pair<num, num>;
  
  // Colormap function as typedef
  using colormap = Color (*)(num x, const MinMax &minmax, bool use_transfer_fct);

  float transferfct(num x, bool use_transfer_fct);

  // See https://github.com/kbinani/glsl-colormap#idl
  namespace IDL {
    namespace PRGn {
      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct = false);
    }

    namespace RdBu {
      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct = false);
    }

    namespace PiYG {
      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct = false);
    }

    namespace Oranges {
      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct = false);
    }
  }  // namespace IDL

  namespace MATLAB {
    namespace HSV {
      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct = false);
    }

    namespace seismic {
      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct = false);
    }

    namespace jet {
      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct = false);
    }
  }  // namespace MATLAB

  // See https://matplotlib.org/cmocean/
  namespace cmocean {
    namespace curl {
      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct = false);

      Color colormap_inv(num x, const MinMax &minmax, bool use_transfer_fct = false);
    }  // namespace curl

    namespace delta {
      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct = false);

      Color colormap_inv(num x, const MinMax &minmax, bool use_transfer_fct = false);
    }  // namespace delta

    namespace balance {
      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct = false);
    }  // namespace balance

    namespace phase {
      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct = false);
    }  // namespace phase

    namespace dense {
      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct = false);
    }  // namespace dense

    namespace algae {
      Color colormap(num x, const MinMax &minmax, bool use_transfer_fct = false);
    }  // namespace algae

  }  // namespace cmocean
}  // namespace Colormap
