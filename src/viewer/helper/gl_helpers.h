#pragma once

#include <cstdio>

#ifdef _WIN32  // Windows 32 and 64 bit
#include <windows.h>
#endif

#include <GLFW/glfw3.h>
#ifdef _WIN32  // Windows 32 and 64 bit
#define GL_MULTISAMPLE                    0x809D
#define GL_SAMPLE_BUFFERS                 0x80A8
#define GL_SAMPLES                        0x80A9
#define GL_CLAMP_TO_BORDER                0x812D
#define GL_LINEAR_MIPMAP_LINEAR           0x2703
#define GL_GENERATE_MIPMAP                0x8191
#define GL_COMBINE                        0x8570
#endif

#include "fontstash/src/fontstash.h"

#include "colormap.h"

#ifdef NUM_SINGLE
using GLnum                 = GLfloat;
constexpr auto glTexCoord2n = glTexCoord2f;
constexpr auto glVertex3nv  = glVertex3fv;
#else
using GLnum                 = GLdouble;
constexpr auto glTexCoord2n = glTexCoord2d;
constexpr auto glVertex3nv  = glVertex3dv;
#endif

namespace Colors {
  constexpr Color black{0, 0, 0, 1};
  constexpr Color brown{0.75, 0.5, 0, 0.5};
  constexpr Color blue{0, 0.75, 1, 1};
  constexpr Color white{1, 1, 1, 1};
  constexpr Color red{204.f/255.f, 0, 51.f/255.f, 1};
  constexpr Color green{34.f/255.f, 153.f/255.f, 84.f/255.f, 1};
  constexpr Color gray_medium{0.4, 0.4, 0.4, 1};
  constexpr Color gray_light{0.6, 0.6, 0.6, 1};
}  // namespace Colors

struct LineStyle {
  float width = 1;
  Color color = Colors::black;
};

inline void glTranslate(const Vec3 &offset) {
  glTranslatef(offset[0], offset[1], offset[2]);
}

void glfw_error_callback(int error, const char *description);

// Set up font atlas for draw_text
void fons_init();
void fons_shutdown();

void draw_text(const Vec3 &pos,
               const string &str,
               float size             = 16.f,
               const Color &color     = Colors::black,
               const string &fonttype = "normal",
               int align              = FONS_ALIGN_CENTER | FONS_ALIGN_BASELINE);

void draw_line(const Vec3 &start, const Vec3 &end, const LineStyle &style);

// Get absolute drawing position on canvas
Vector3<float> get_canvas_draw_pos();

void draw_triangle(Vec3 pos0, Color c0, Vec3 pos1, Color c1, Vec3 pos2, Color c2);

void draw_3D_arrow(Vec3 pos,
                   Vec3 forward,
                   float head_size,
                   LineStyle linestyle = {2, Colors::black});

GLuint load_png_as_texture(string png_filename);

GLuint colormap_as_texture(Colormap::colormap cm,
                           bool use_transfer_fct   = false,
                           Colormap::MinMax minmax = {0, 1});

void snapshot(long int frame);