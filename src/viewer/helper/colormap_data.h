#include <array>

namespace Colormap {
  namespace cmocean {
    extern const std::array<std::array<float, 3>, 512> curl_rgb;
    extern const std::array<std::array<float, 3>, 512> delta_rgb;
    extern const std::array<std::array<float, 3>, 256> balance_rgb;
    extern const std::array<std::array<float, 3>, 256> phase_rgb;
    extern const std::array<std::array<float, 3>, 256> dense_rgb;
    extern const std::array<std::array<float, 3>, 256> algae_rgb;
  }  // namespace cmocean
}  // namespace Colormap