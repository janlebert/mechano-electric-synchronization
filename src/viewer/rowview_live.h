#pragma once

#include <functional>

#include "rowview.h"
#include "rowview_3D.h"
#include "simulations/simulation.h"
#include "da_renorm.h"

extern Parameters prm;
extern std::vector<std::shared_ptr<RowView>> rows;

class RowView3DLive : public RowView3D {
 public:
  std::shared_ptr<Simulation> sim;

  RowView3DLive(std::shared_ptr<Simulation> sim, RateType rt) : RowView3D({}, rt, ""), sim(sim) {
    prm.frameskip = 1;
  };

  void readBinaryFileElectro(long int frame) override {
    while (prm.t < frame) {
      sim->timeStep(frame);
      prm.t += 1;
    }

    for (auto& pos : prm.positions) {
      u3D(pos) = sim->Tissue.getMyocyte(pos)->getu();
    }
  }
  void readBinaryFileMechanics(long int frame) override {
    for (auto& pos : prm.positions) {
      pos3D(pos) = sim->Tissue.getMyocyte(pos)->getPos();
    }
  }
};

class RowView3DLiveDA : public RowView3D_DA {
  std::shared_ptr<DataAssimilationSimulation> sim;

 public:
  RowView3DLiveDA(std::shared_ptr<SimulationDataStreams> simDataStream,
                  std::shared_ptr<DataAssimilationSimulation> sim,
                  RateType rt)
      : RowView3D_DA(simDataStream, {}, rt, ""), sim(std::move(sim)) {
    prm.frameskip = 1;
  }

  void readBinaryFileElectro(long int frame) override {
    if (simData) u3D.load(simData->u, frame);

    while (prm.t < frame) {
      sim->timeStep(frame);
      prm.t += 1;
    }

    for (auto& pos : prm.positions) {
      u3D_da(pos) = sim->Tissue.getMyocyte(pos)->getu();
    }
  }

  void readBinaryFileMechanics(long int frame) override {
    if (simData) pos3D.load(simData->pos, frame);

    for (auto& pos : prm.positions) {
      pos3D_da(pos) = sim->Tissue.getMyocyte(pos)->getPos();
    }
  }

  void clearEOF() override { simData = {}; }
};