#pragma once

#include <memory>
#include <stdexcept>

#include "parameters.h"
#include "arrays.h"
#include "helper.h"
#include "rate_factory.h"
#include "helper/colormap.h"
#include "helper/gl_helpers.h"

class RowView {
 protected:
  Colormap::colormap excitation_color = Colormap::IDL::PRGn::colormap;
  Colormap::colormap mechanics_color  = Colormap::IDL::RdBu::colormap;

  /* system size */
  const int Nx;
  const int Ny;
  const int Nz;
  const num h_inv;  // inverse spacing parameter

  num calibrate_factor = 0.8;

  Array3<num> u3D;
  Array3<Vec3> pos3D;

  std::shared_ptr<Rate3D> rate;
  const RateType rt;

  std::shared_ptr<Rate3D> init_rate();

  // Minor helper functions
  static Vec3 left_cell_pos(const Vec3 &offset) {
    return offset - Vec3(prm.Nx + cells_padding, 0, 0);
  }

  static Vec3 right_cell_pos(const Vec3 &offset) {
    return offset + Vec3(prm.Nx + cells_padding, 0, 0);
  }

 public:
  string label;
  num height_px_factor = 1;

  bool compute_rate              = true;
  Color meshcolor                = {0.25, 0.25, 0.25, 1};
  static const int cells_padding = 10;

  std::shared_ptr<SimulationDataStreams> simData;

  RowView(RateType rt, string label) : RowView({}, rt, label){};

  RowView(std::shared_ptr<SimulationDataStreams> simDataStream, RateType rt, string label)
      : Nx(prm.Nx),
        Ny(prm.Ny),
        Nz(prm.Nz),
        h_inv(1. / prm.spacing),
        u3D(prm.dims),
        pos3D(prm.dims),
        rt(rt),
        label(label),
        simData(simDataStream) {

    rate = init_rate();
    u3D.Load(0.);
  };

  virtual ~RowView() = default;

  virtual void display(long int t, Vec3 offset, bool play) = 0;

  void drawLabel(const Vec3 &offset,
                 const string &str,
                 int align       = FONS_ALIGN_CENTER | FONS_ALIGN_BASELINE,
                 string fonttype = "italic");

  static void drawColormap(GLuint tex, MinMax mm, Vec3 offset, Vec2 size, float fontsize);

  virtual void recalibrateColormap(bool forced);

  virtual void readBinaryFileElectro(long int frame);

  virtual void readBinaryFileMechanics(long int frame);

  virtual void clearEOF();

  // Handle user input
  virtual void frameskipChanged(int oldvalue, int newvalue) {}
};

std::shared_ptr<Rate3D> RowView::init_rate() {
  auto r = make_rate(rt, pos3D, simData.get());

  ASSERT_ALWAYS(r != nullptr, "rate creation failed!");

  return r;
}


void RowView::drawLabel(const Vec3 &offset, const string &str, int align, string fonttype) {
  Vec3 pos = Vec3(offset[0], offset[1] + Ny / 2, 0);
  draw_text(pos, str, 14.f, Colors::black, fonttype, align);
}

void RowView::recalibrateColormap(bool forced) {
  rate->calibrate_minmax(forced, calibrate_factor);
}

void RowView::readBinaryFileMechanics(long int frame) {
  pos3D.load(simData->pos, frame);
}

void RowView::readBinaryFileElectro(long int frame) {
  u3D.load(simData->u, frame);
}

void RowView::clearEOF() {
  if (simData) {
    simData->clearEOF();
  }
}

void RowView::drawColormap(GLuint tex, MinMax mm, Vec3 offset, Vec2 size, float fontsize) {

  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, tex);

  offset -= Vec3(size[0] / 2.f, size[1] / 2.f, 0);

  const Vec3 pos1 = offset;
  const Vec3 pos2 = offset + Vec3(size[0], 0, 0);
  const Vec3 pos3 = offset + Vec3(size[0], size[1], 0);
  const Vec3 pos4 = offset + Vec3(0, size[1], 0);

  const Vec2 o = Vec2(size[1] / size[0], 1) * 0.05;
  glBegin(GL_QUADS);
  glTexCoord2n(0 - o[0], 0 - o[1]);
  glVertex3nv(pos1);

  glTexCoord2n(1 + o[0], 0 - o[1]);
  glVertex3nv(pos2);

  glTexCoord2n(1 + o[0], 1 + o[1]);
  glVertex3nv(pos3);

  glTexCoord2n(0 - o[0], 1 + o[1]);
  glVertex3nv(pos4);
  glEnd();
  glDisable(GL_TEXTURE_2D);

  const Vec3 text_offset{0, 0, 0};
  draw_text(pos1 + text_offset, fmt::format("{:.2g} ", mm.first), fontsize, Colors::black, "normal",
            FONS_ALIGN_RIGHT);
  draw_text(pos2 + text_offset, fmt::format(" {:.2g}", mm.second), fontsize, Colors::black, "normal",
            FONS_ALIGN_LEFT);
}
