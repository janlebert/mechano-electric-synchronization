#pragma once

#include "viewer_gl.h"
#include "imgui/imgui.h"
#include "imgui/examples/imgui_impl_glfw.h"
#include "imgui/examples/imgui_impl_opengl2.h"

#include "simulations/induce_spirals.h"

extern Parameters prm;

namespace ImGuiConnector {
  ImGuiIO *io = nullptr;

  void Init(GLFWwindow *window) {
    // Setup ImGui binding
    ImGui::CreateContext();
    io = &ImGui::GetIO();

    // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Disabled as it always wants to capture keyboard

    // Disable imgui.ini creation
    io->IniFilename = nullptr;

    // Bake font from custom glyph ranges
    ImFontAtlas::GlyphRangesBuilder builder;
    builder.AddText(u8"Δσµε₁₂");
    builder.AddRanges(io->Fonts->GetGlyphRangesDefault());
    static ImVector<ImWchar> ranges;
    builder.BuildRanges(&ranges);
    io->Fonts->AddFontFromFileTTF(VENDOR_PWD "DejaVuSerif.ttf", 16.0f, nullptr, ranges.Data);

    // Setup style
    //ImGui::StyleColorsDark();

    // Setup ImGui <-> glfw bindings
    ImGui_ImplGlfw_InitForOpenGL(window, false);
    ImGui_ImplOpenGL2_Init();
    glfwSetCharCallback(window, ImGui_ImplGlfw_CharCallback);
  }

  void NewFrame() {
    if (!io) return;

    ImGui_ImplOpenGL2_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
  }

  void Render() {
    if (!io) return;

    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);
    ImGui::Render();
    ImGui_ImplOpenGL2_RenderDrawData(ImGui::GetDrawData());
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
  }

  void Shutdown() {
    if (!io) return;

    ImGui_ImplOpenGL2_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
  }

  bool WantsMouseInput(GLFWwindow *window, int button, int action, int mods) {
    if (!io) return false;

    ImGui_ImplGlfw_MouseButtonCallback(window, button, action, mods);

    // imgui wants the input, don't dispatch it to our app
    return io->WantCaptureMouse;
  }

  bool WantsScrollInput(GLFWwindow *window, double xoffset, double yoffset) {
    if (!io) return false;

    ImGui_ImplGlfw_ScrollCallback(window, xoffset, yoffset);

    // imgui wants the input, don't dispatch it to our app
    return io->WantCaptureMouse;
  }

  bool WantsKeybourdInput(GLFWwindow *window, int key, int scancode, int action, int mods) {
    if (!io) return false;

    ImGui_ImplGlfw_KeyCallback(window, key, scancode, action, mods);

    // imgui wants the input, don't dispatch it to our app
    return io->WantCaptureKeyboard;
  }
}  // namespace ImGuiConnector

class GuiWindow {
 public:
  bool show_window = true;

  GuiWindow(bool show_window) : show_window(show_window){};

  virtual ~GuiWindow() = default;

  virtual void display(long int t, Vec3 offset) = 0;

 protected:
  template <typename ROW>
  ROW *get_row() {
    for (auto &row : rows) {
      if (ROW *r = dynamic_cast<ROW *>(row.get())) {
        return r;
      }
    }

    FATAL_ERROR("[GuiWindow] could not find specified rowtype in rows");
  }
};

class GuiViewerControl : public GuiWindow {
 public:
  GuiViewerControl(bool show_window) : GuiWindow(show_window) {}

  void display(long int t, Vec3 offset) final {

    if (!show_window) return;

    ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiCond_Appearing);
    ImGui::Begin("Viewer Control", &show_window, ImGuiWindowFlags_NoFocusOnAppearing);

    ImGui::Text("%.1f FPS", ImGui::GetIO().Framerate);
    ImGui::Separator();

    static int t_integer;
    t_integer = t / prm.tw;
    ASSERT_DEBUG(!rows.empty());

    if (rows.front()->simData) {  // No sim data in live simulation
      static int t_end = rows.front()->simData->maxNt() / prm.tw;
      if (ImGui::SliderInt("t", &t_integer, 0, t_end)) {
        log_msg("[viewer] setting t from {} to {}", t, t_integer * prm.tw);
        Viewer::t = t_integer * prm.tw;
      }
    }

    static int frameskip = prm.frameskip;
    if (ImGui::InputInt("frameskip", &frameskip)) {
      Viewer::set_frameskip(frameskip);
    }
    ImGui::InputFloat3("viewpoint", Viewer::viewpoint);
    ImGui::InputFloat3("rotate", Viewer::rotate);
    ImGui::Checkbox("snapshots", &Viewer::makesnapshots);

    static bool do_movie = false;
    if (ImGui::Checkbox("record", &do_movie)) {
      if (do_movie && !Viewer::makemovie) {
        Viewer::makemovie = true;
        Viewer::recorder.start_recording(prm.movie_filename, prm.fps);
      } else if (!do_movie && Viewer::makemovie) {
        Viewer::makemovie = false;
        Viewer::recorder.stop_recording();
      }
    }

    ImGui::End();

    //static bool show = true;
    //ImGui::ShowMetricsWindow(&show);
  }
};

class GuiLiveSimulation : public GuiWindow {
  std::shared_ptr<Simulation> sim;
  const bool show_da;
  array<char, 10> spiral_init_method{};

 public:
  GuiLiveSimulation(std::shared_ptr<Simulation> sim, bool show_window, bool da = false)
      : GuiWindow(show_window), sim(sim), show_da(da) {
    std::copy(prm.init_spiral_method.begin(), prm.init_spiral_method.end(),
              spiral_init_method.begin());
  }

  void display(long int t, Vec3 offset) final {
    if (!show_window) return;

    ImGui::SetNextWindowPos(ImVec2(890, 70), ImGuiCond_Once);
    ImGui::SetNextWindowSize(ImVec2(300, 390), ImGuiCond_Once);
    ImGui::Begin("Parameters", &show_window, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoNav | ImGuiWindowFlags_NoFocusOnAppearing);

    if (!Viewer::play && ImGui::Button("Play")) {
      Viewer::play = true;
    } else if (Viewer::play && ImGui::Button("Pause")) {
      Viewer::play = false;
    }

    ImGui::SameLine();
    if (ImGui::Button("Reset Tissue")) {
      sim->Tissue.resetMedium();
    }

    ImGui::Separator();

    if (!show_da) {

      ImGui::SetNextTreeNodeOpen(true, ImGuiCond_Once);
      if (ImGui::TreeNode("Spiral Init")) {

        ImGui::Columns(2, "spiralinit", false);
        ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.2f);
        ImGui::InputText("Method", spiral_init_method.data(), spiral_init_method.size());
        ImGui::NextColumn();
        ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.25f);
        ImGui::InputInt("Width", &prm.init_spiral_width);
        ImGui::Columns(1);

        ImGui::PushItemWidth(0.0f);

        if (ImGui::Button("Init Spiral")) {
          string str(spiral_init_method.data());
          Spirals spirals(sim->Tissue, prm.init_spiral_width);
          spirals.induce_spiral(str);
        }

        ImGui::TreePop();
      }

    }
#ifdef NUM_SINGLE  // only floats supported, disable when using doubles to avoid compile error
    else {

      ImGui::SetNextTreeNodeOpen(true, ImGuiCond_Once);
      if (ImGui::TreeNode("Data Assimlation")) {

        ImGui::InputFloat("da k", &prm.da_k, 0, 0, 2);

        ImGui::InputInt("da delay", &prm.da_delay);

        static IdxVec sensor_size    = prm.sensor_size;
        static IdxVec sensor_spacing = prm.sensor_spacing;
        ImGui::InputInt3("sensor size", sensor_size);
        ImGui::InputInt3("sensor spacing", sensor_spacing);
        if (ImGui::Button("Reset Sensors")) {
          auto da_sim = reinterpret_cast<DataAssimilationSimulation *>(sim.get());

          ASSERT_ALWAYS(da_sim != nullptr, "cast to DataAssimilationSimulation failed when it should never do so!");

          log_msg("[Viewer] Resetting sensors, new size {}, old size {}; new spacing {}, old spacing {}",
                  sensor_size, prm.sensor_size, sensor_spacing, prm.sensor_spacing);
          da_sim->assim.resetSensors(sensor_size, sensor_spacing);
        }

        ImGui::TreePop();
      }
    }

    ImGui::SetNextTreeNodeOpen(true, ImGuiCond_Once);
    if (ImGui::TreeNode("Simulation Control")) {
      static int Nt      = prm.Nt;
      static int Ntm     = prm.Ntm;

      ImGui::Checkbox("Mechanics", &prm.mechanics);
      ImGui::Checkbox("Electrophysiology", &prm.electrophysiology);
      if (ImGui::InputInt("Nt", &Nt, ImGuiInputTextFlags_EnterReturnsTrue)) {
        prm.Nt = Nt;
      };
      if (ImGui::InputInt("Ntm", &Ntm, ImGuiInputTextFlags_EnterReturnsTrue)) {
        prm.Ntm = Ntm;
      };


      ImGui::Columns(2, "its", false);
      ImGui::InputFloat("Δte", &prm.dt_e);
      ImGui::InputInt("ite", &prm.ite);
      ImGui::NextColumn();
      ImGui::InputFloat("Δtm", &prm.dt_m);
      ImGui::InputInt("itm", &prm.itm);
      ImGui::Columns(1);

      ImGui::InputFloat("initm", &prm.initm);

      ImGui::TreePop();
    }

    ImGui::SetNextTreeNodeOpen(true, ImGuiCond_Once);
    if (ImGui::TreeNode("Aliev Panfilov Parameters")) {
      static num a = prm.a;
      static num b = prm.b;

      ImGui::Columns(2, "ap-params", false);
      if (ImGui::InputFloat("a", &a)) {
        log_msg("New value for a: {}", a);
        prm.a = a;
        for (auto &pos : prm.positions) {
          sim->Tissue.getMyocyteWithPadding(pos)->writea(prm.a);
        }
      };
      if (ImGui::InputFloat("b", &b)) {
        log_msg("New value for b: {}", b);
        prm.b = b;
        for (auto &pos : prm.positions) {
          sim->Tissue.getMyocyteWithPadding(pos)->writea(prm.b);
        }
      };
      ImGui::InputFloat("µ₁", &prm.mu1, 0, 0, 3);
      ImGui::InputFloat("µ₂", &prm.mu2, 0, 0, 3);

      ImGui::NextColumn();

      ImGui::InputFloat("D", &prm.D);
      ImGui::InputFloat("ε", &prm.epsilon, 0, 0, 3);
      ImGui::InputFloat("k", &prm.k, 0, 0, 2);

      ImGui::Columns(1);
      ImGui::TreePop();
    }

    ImGui::SetNextTreeNodeOpen(true, ImGuiCond_Once);
    if (ImGui::TreeNode("Mechanics")) {
      static num ksai = prm.ksai;

      ImGui::Columns(2, "mechs", false);

      ImGui::InputFloat("kT", &prm.kT, 0, 0, 2);
      ImGui::InputFloat("cs", &prm.cs, 0, 0, 2);
      ImGui::InputFloat("hk", &prm.hk, 0, 0, 2);
      ImGui::InputFloat("myofibcontr", &prm.myofibcontr, 0, 0, 2);

      ImGui::NextColumn();

      ImGui::InputFloat("ksi", &prm.ksi, 0, 0, 2);
      if (ImGui::InputFloat("ksai", &ksai, 0, 0, 2)) {
        log_msg("New value for ksai: {}", ksai);
        prm.ksai = ksai;
        prm.ks1  = ksai;
        prm.ks2  = ksai;
        prm.ks3  = ksai;
        prm.ks4  = ksai;
        prm.ks5  = ksai;
        prm.ks6  = ksai;
      }
      ImGui::InputFloat("ksv", &prm.ksv, 0, 0, 2);

      ImGui::Columns(1);

      static Vec3 fiber = prm.linear_fiber;
      static Vec3 hint  = prm.linear_hint;
      ImGui::InputFloat3("fiber", fiber, 2);
      ImGui::InputFloat3("hint", hint, 2);
      if (ImGui::Button("Realign fibers")) {
        sim->Tissue.realignMyocytesLinear(fiber, hint, false);
      }

      ImGui::TreePop();
    }
#endif

    ImGui::End();
  }
};
