#include <cfenv>

#include "CLI/CLI.hpp"

#include "simulations/da_renorm.h"


Parameters prm;

int main(int argc, char **argv) {
  CLI::App app{"Data assimilation routine"};
  app.option_defaults()->always_capture_default();

  string paramsfile = "params.ini";
  string prefix     = "";
  string logfile    = "";
  string daprefix   = "";

  app.add_option("-p,--param", paramsfile, "parameter file")->check(CLI::ExistingFile);
  app.add_option("-l,--logfile", logfile, "log to file instead of stdout");
  app.add_option("-x,--prefix", prefix, "simulation data input prefix");
  app.add_option("-X,--daprefix", daprefix, "output prefix");

  try {
    app.parse(argc, argv);

    if (!io::fileExists(paramsfile)) {
      fmt::print(std::cerr, "ERROR: parameter file {} does not exit\n\n", paramsfile);
      throw CLI::CallForHelp();
    }

  } catch (const CLI::ParseError &e) {
    std::exit(app.exit(e));
  }

  prm.load_file(paramsfile, logfile);

  if (!prefix.empty()) prm.filename_prefix = prefix;
  if (!daprefix.empty()) prm.da_filename_prefix = daprefix;
  std::swap(prm.filename_prefix, prm.da_filename_prefix);

  auto simData = make_shared<SimulationDataStreams>(prm.da_filename_prefix + "_u.dat",
                                             prm.da_filename_prefix + prm.da_pos_suffix, prm.dims,
                                             prm.tw);

  DataAssimRenorm sim(true, simData, RateType::Volumechange);

  sim.run();

  exit(0);
}