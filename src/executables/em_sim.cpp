#include <cfenv>

#include "CLI/CLI.hpp"

#include "parameters.h"
#include "simulations/em_sim.h"

Parameters prm;

int main(int argc, char **argv) {
  CLI::App app{"Electromechanical Simulation"};
  app.option_defaults()->always_capture_default();

  string paramsfile = "params.ini";
  string prefix     = "";
  string logfile    = "";

  app.add_option("-p,--param", paramsfile, "parameter file")->check(CLI::ExistingFile);
  app.add_option("-l,--logfile", logfile, "log to file instead of stdout");
  app.add_option("-x,--prefix", prefix, "output prefix");

  try {
    app.parse(argc, argv);

    if (!io::fileExists(paramsfile)) {
      fmt::print(std::cerr, "ERROR: parameter file {} does not exit\n\n", paramsfile);
      throw CLI::CallForHelp();
    }
  } catch (const CLI::ParseError &e) {
    return app.exit(e);
  }

  prm.load_file(paramsfile, logfile);

  if (!prefix.empty()) {
    prm.filename_prefix = prefix;
  }

#ifdef __linux__  // abort on floating point exceptions
  feenableexcept(FE_INVALID | FE_OVERFLOW | FE_DIVBYZERO);
#endif

  EmSimulation sim(true);
  sim.run();

  exit(0);
}