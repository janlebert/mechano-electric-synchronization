/*
 * Computes excitation phase filaments
 */

#include "CLI/CLI.hpp"

#include "parameters.h"
#include "num.h"
#include "helper/helper.h"
#include "filters/filament.h"

Parameters prm;

void save_arr(CircularArray<num>& in, const string& filepath, const std::vector<long>& t_vec);

int main(int argc, char** argv) {
  CLI::App app{"Computes the filaments from the electric activity of a simulation"};
  app.option_defaults()->always_capture_default();

  string paramsfile = "params.ini";
  string prefix     = "";
  string logfile    = "";

  app.add_option("-p,--param", paramsfile, "parameter file [Default: params.ini]")
      ->check(CLI::ExistingFile);
  app.add_option("-l,--logfile", logfile, "log to file instead of stdout");
  app.add_option("-x,--prefix", prefix, "prefix override which simulation to load");

  long t_start = 0;
  app.add_option("--t_start", t_start, "starting timepoint");
  long t_end = 0;
  app.add_option("--t_end", t_end, "end timepoint");
  int dt = 1;
  app.add_option("--dt", dt, "load every ... timestep");
  num cutoff = 0.99;
  app.add_option("--cutoff", cutoff, "")->check(CLI::Range(1.));

  try {
    app.parse(argc, argv);

    if (!io::fileExists(paramsfile)) {
      fmt::print(std::cerr, "ERROR: parameter file {} does not exit\n\n", paramsfile);
      throw CLI::CallForHelp();
    }
  } catch (const CLI::ParseError& e) {
    return app.exit(e);
  }

  prm.load_file(paramsfile, logfile);

  if (!prefix.empty()) {
    prm.filename_prefix = prefix;
  }

  if (t_end == 0) {
    t_end = prm.Nt - prm.Ntw;
  }

  ASSERT_ALWAYS(t_start < t_end);

  std::vector<long> ts = {};

  for (long t = t_start / prm.tw; t < t_end / prm.tw; t += dt) {
    ts.push_back(t);
  }

  InStream<num> src(prm.filename_prefix + "_u.dat", prm.dims, 1);
  long maxNt = src.maxNt();
  ASSERT_ALWAYS(ts.back() < maxNt);

  CircularArray<num> in(prm.dims, ts.size(), 1);
  for (long i = 0; i < ts.size(); i++) {
    in[i].load(src, ts[i]);
  }
  log_msg("loading complete, window = {}", ts.size());

  CircularArray<num> phase = Filament::compute_phase(in, 0.5f);
  log_msg("hilbert transform complete", in.get_t_end());
  //save_arr(phase, prm.filename_prefix + "_uphase.dat", t_vec);

  // overwrite original in array with filaments to lower memory consumption
  Filament::compute_filaments(phase, in, cutoff);
  save_arr(in, prm.filename_prefix + "_ufilaments.dat", ts);

  exit(0);
}

void save_arr(CircularArray<num>& in, const string& filepath, const std::vector<long>& t_vec) {
  log_msg("saving to {}", filepath);

  OutStream out(filepath, true);
  Array3<num> zeros(prm.dims);

  long i = 0;
  for (long t = 0; t < t_vec.back(); t++) {
    if (t == t_vec[i]) {
      in[i].save(out);
      i += 1;
    } else {
      if (i) {
        in[i].save(out);
      } else {
        zeros.save(out);
      }
    }
  }
}