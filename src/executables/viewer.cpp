#include <cfenv>

#include "CLI/CLI.hpp"

#include "viewer/viewer_gl.h"

#include "simulations/em_sim.h"
#include "simulations/da_renorm.h"

struct ViewerOptions {
  RateType rt                                    = RateType::Volumechange;
  string rate_suffix                             = "_E1.dat";
  bool makevideo                                 = false;
  bool show_gui                                  = true;
  string window_title                            = "";
  std::shared_ptr<SimulationDataStreams> simData = {};
};

ViewerOptions parse_args(CLI::App &app, int argc, char **argv) {

  app.option_defaults()->always_capture_default();

  ViewerOptions viewerOptions;
  string paramsfile = "params.ini";
  string prefix     = "";
  string logfile    = "";
  string daprefix   = "";
  string ratetype   = "volumechange";
  int frameskip     = 5;
  string moviename  = "";
  int fps           = 0;


  app.add_option("-l,--logfile", logfile, "log to file instead of stdout");

  /* General Options */
  app.add_option("-p,--param", paramsfile, "parameter file")
      ->check(CLI::ExistingFile)
      ->group("General Options");
  app.add_option("-x,--prefix", prefix, "simulation data input prefix")->group("General Options");
  app.add_option("-X,--daprefix", daprefix, "data assimilation data prefix")
      ->group("General Options");

  app.add_option("-R,--ratetype", ratetype, "Ratetype to use")
      ->check(CLI::IsMember(DeformationMetrics::RateTypesStrings()))
      ->group("General Options");
  app.add_option("--ratesuffix", viewerOptions.rate_suffix, "rate suffix")->group("General Options");

  /* Viewer Options */
  app.add_option("--frameskip", frameskip, "show every ... timestep")->group("Viewer Options");
  app.add_option("--t_start", Viewer::t, "Start viewer at a specific timepoint")
      ->group("Viewer Options");
  app.add_option("--t_end", Viewer::t_end, "Stop viewer at a specific timepoint")
      ->group("Viewer Options");
  app.add_option("--window-width", Viewer::window_width, "width of the created window in pixels")
      ->group("Viewer Options");
  app.add_option("--msaa-samples", Viewer::msaa_samples,
                 "number of samples for multisample antialiasing (MSAA), 0 disables multisampling")
      ->group("Viewer Options");


  /* Output Options */
  app.add_flag("-m,--makevideo", viewerOptions.makevideo, "save as video and exit")
      ->group("Output Options");
  app.add_option("-o,--videoname", moviename, "output video filename")->group("Output Options");
  app.add_option("--fps", fps, "output video frames per second")->group("Output Options");
  app.add_flag("--screenshot", Viewer::makesnapshots, "save frames as .png files")
      ->group("Output Options");


  /* Sub Commands */
  app.require_subcommand();
  app.fallthrough();
  app.failure_message(CLI::FailureMessage::help);
  app.add_subcommand("sim", "simulation data viewers");
  app.add_subcommand("da", "data assimilation viewers");
  app.add_subcommand("live_sim", "run simulation and display results live");
  app.add_subcommand("live_da", "run data assimilation and display results live");
  app.add_subcommand("filaments", "filaments viewer");

  try {
    app.parse(argc, argv);

    if (!io::fileExists(paramsfile)) {
      fmt::print(std::cerr, "ERROR: parameter file {} does not exit\n\n", paramsfile);
      throw CLI::CallForHelp();
    }

  } catch (const CLI::ParseError &e) {
    std::exit(app.exit(e));
  }

  prm.load_file(paramsfile, logfile);


  if (!prefix.empty()) prm.filename_prefix = prefix;
  if (!daprefix.empty()) prm.da_filename_prefix = daprefix;

  ASSERT_ALWAYS(Viewer::t % prm.tw == 0, "t_start parameter is not a multiple of prm.tw!");
  ASSERT_ALWAYS(Viewer::t_end % prm.tw == 0, "t_end parameter is not a multiple of prm.tw!");

  if (frameskip) prm.frameskip = frameskip;

  if (!moviename.empty()) prm.movie_filename = moviename;
  if (fps) prm.fps = fps;

  viewerOptions.rt = DeformationMetrics::Ratetype(ratetype);

  return viewerOptions;
}

std::shared_ptr<SimulationDataStreams> load_simdata(ViewerOptions &viewerOptions) {
  string electro_fn    = prm.filename_prefix + "_u.dat";
  string fiberpos_fn   = prm.filename_prefix + "_pos.dat";
  string ratesuffix_fn = prm.filename_prefix + viewerOptions.rate_suffix;
  if (!io::fileExists(ratesuffix_fn)) {
    ratesuffix_fn = "";
  }

  return make_shared<SimulationDataStreams>(electro_fn, fiberpos_fn, prm.dims, prm.tw,
                                            ratesuffix_fn);
}

int main(int argc, char **argv) {

  CLI::App app{"Electromechanical simulation viewer"};
  ViewerOptions viewerOptions = parse_args(app, argc, argv);

  int z_idx   = prm.Nz / 2 - 1;
  prm.z_slice = z_idx;

  viewerOptions.show_gui = !viewerOptions.makevideo;

  viewerOptions.window_title =
      fmt::format("Elastic Reaction-Diffusion Model — {}x{}x{} system", prm.Nx, prm.Ny, prm.Nz);


  /* live_sim subcommand */
  if (app.got_subcommand("live_sim")) {  // No need to load simulation data for these

    std::shared_ptr<Simulation> sim = make_shared<EmSimulation>(false);

    rows = {make_shared<RowView3DLive>(sim, viewerOptions.rt)};

    guis = {make_shared<GuiLiveSimulation>(sim, viewerOptions.show_gui)};

  } else {  // No live simulation, load simulation data

    if (app.got_subcommand("filaments")) {
      // For the filament viewers, misuse the filerate rate class to load the filament files
      viewerOptions.rate_suffix = "_ufilaments.dat";
    }

    viewerOptions.simData = load_simdata(viewerOptions);
    ASSERT_ALWAYS(viewerOptions.simData);
  }

  /* sim subcommand */
  if (app.got_subcommand("sim")) {
    rows = {make_shared<RowView3D>(viewerOptions.simData, viewerOptions.rt, "")};
  }

  /* da subcommand */
  if (app.got_subcommand("da")) {
    //if (Viewer::t == 0) {
    //  Viewer::t = clamp(prm.Nt_assim_on - 10 * prm.frameskip * prm.tw, 0, prm.Nt);
    //}

    string da_fn = "";
    if (viewerOptions.rt == RateType::File)
      da_fn = prm.da_filename_prefix + viewerOptions.rate_suffix;

    auto daData = make_shared<SimulationDataStreams>(prm.da_filename_prefix + "_u.dat",
                                                     prm.da_filename_prefix + prm.da_pos_suffix,
                                                     prm.dims, prm.tw, da_fn);

    rows.emplace_back(
        make_shared<RowView3D_DA>(viewerOptions.simData, daData, viewerOptions.rt, ""));
  }

  /* live_da subcommand */
  if (app.got_subcommand("live_da")) {
    std::shared_ptr<DataAssimilationSimulation> sim =
        make_shared<DataAssimRenorm>(false, viewerOptions.simData, viewerOptions.rt);

    rows = {make_shared<RowView3DLiveDA>(viewerOptions.simData, sim, viewerOptions.rt)};

    guis = {make_shared<GuiLiveSimulation>(sim, viewerOptions.show_gui, true)};
  }

  /* filaments subcommand */
  if (app.got_subcommand("filaments")) {
    string fil_suffix = "_ufilaments.dat";
    if (io::fileExists(prm.da_filename_prefix + "_u.dat") &&
        io::fileExists(prm.da_filename_prefix + fil_suffix)) {

      auto daData = make_shared<SimulationDataStreams>(
          prm.da_filename_prefix + "_u.dat", prm.da_filename_prefix + prm.da_pos_suffix, prm.dims,
          prm.tw, prm.da_filename_prefix + fil_suffix);
      rows = {make_shared<RowView3DFilamentsDA>(viewerOptions.simData, daData)};
    } else {
      rows = {make_shared<RowView3DFilaments>(viewerOptions.simData)};
    }
  }

  Viewer v(viewerOptions.makevideo, viewerOptions.window_title);
  v.run();
}