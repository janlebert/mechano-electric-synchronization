#pragma once

#include <ctime>
#include <algorithm>

#include "num.h"
#include "helper.h"
#include "myocardium.h"
#include "rate.h"
#include "assimilation.h"

extern Parameters prm;

class Simulation {
 public:
  Myocardium Tissue;
  const bool write_output;

  explicit Simulation(bool save_output) : Tissue(save_output), write_output(save_output) {}

  virtual ~Simulation() { prm.globalVariablesReset(); };

  virtual void run() {
    for (long t = 0; t < prm.Nt; t++) {
      timeStep(t);
      prm.t += 1;
      printProgress();
    }
  };

  virtual void timeStep(long t) = 0;

  void disablePrintProgress() { last_percentage = std::numeric_limits<int>::max(); }

 protected:
  int last_percentage = -1;

  void printProgress() {
    const int percentage = 100 * prm.t / prm.Nt;
    if (percentage > last_percentage) {
      log_msg("{}% complete", percentage);
      last_percentage = percentage;
    }
  }
};
