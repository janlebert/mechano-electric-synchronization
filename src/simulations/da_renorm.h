#pragma once

#include <limits>
#include <stdexcept>

#include "simulations/da_sim.h"
#include "data_assim/filters/deriche.h"
#include "data_assim/filters/noiseadder.h"
#include "data_assim/filters/renorm.h"


class RenormedData {
  Array3<num> renorm1;
  Array3<num> renorm2;

  std::shared_ptr<SimulationDataStreams> simDataStreams;

 public:
  Array3<num> *prev;
  DataTime t_prev{SimTime(0)};
  Array3<num> *next;
  DataTime t_next{SimTime(0)};

  Array3<Vec3> pos3D;
  Array3<Vec3> pos3D_noised;
  NoiseAdder noiseAdder;
  std::shared_ptr<Rate3D> rd_noised;

  DericheFilter filter;
  std::shared_ptr<Rate3D> rd_filtered;

  Renorm renorm;
  std::shared_ptr<Rate3D> rd_renorm;

  RenormedData(std::shared_ptr<SimulationDataStreams> simData, RateType rt)
      : renorm1(prm.dims),
        renorm2(prm.dims),
        simDataStreams(simData),
        pos3D(prm.dims),
        pos3D_noised(prm.dims),
        noiseAdder(prm.da_noise_stdev * prm.spacing),
        filter(prm.da_smoothing_sigma),
        renorm(prm.dims) {
    prev = &renorm1;
    next = &renorm2;

    rd_noised = make_rate(rt, pos3D_noised, simData.get());
    rd_noised->set_outfile(simData->prefix() + "_noised.dat");
    rd_filtered = make_shared<RateDebug3D>(simData->prefix() + "_filtered.dat");
    rd_renorm   = make_shared<RateDebug3D>(simData->prefix() + "_renorm.dat");
  }

  void loadnext(DataTime t) {
    std::swap(prev, next);
    std::swap(t_prev, t_next);

    t_next = t.next();
    ASSERT_ALWAYS(t_next.available());

    pos3D.load(simDataStreams->pos, t_next.t());

    // Add noise
    noiseAdder.apply(pos3D, pos3D_noised);

    // Compute noised rate
    rd_noised->compute();

    // Gauss filter
    filter.apply(rd_noised->rate, rd_filtered->rate);

    // Renorm
    renorm.apply(rd_filtered->rate, *next);
    rd_renorm->rate.Set(next->data());
  }

  void reset() {
    renorm1.Load(0.f);
    renorm2.Load(0.f);
    t_prev = SimTime(0);
    t_next = SimTime(0);
  }
};

class DataAssimRenorm : public DataAssimilationSimulation {
 public:
  RenormedData renormedData;

  bool load_groundtruth_at_startup = false;
  long Nt_enable_mechanics         = 0;  // time point when to enable mechanics

  DataAssimRenorm(bool write_output,
                  std::shared_ptr<SimulationDataStreams> simData,
                  RateType rt)
      : DataAssimilationSimulation(write_output, simData, rt), renormedData(simData, rt) {

    ASSERT_ALWAYS(rt != RateType::File, "ratetype File not supported");

    rate_data   = make_rate(rt, renormedData.pos3D, simData.get());
    rate_tissue = make_rate(rt, Tissue, prm.filename_prefix);

    string fn_ratediff = prm.filename_prefix + "_ratediff.dat"s;
    rate_diffs = make_unique<FloatDiffRate3D>(rate_data.get(), rate_tissue.get(), fn_ratediff);

    // enable mechanics at the latest possible point to save computational resources
    Nt_enable_mechanics = std::max({0l, prm.Ntm, prm.Nt_assim_on - prm.tw_da});
  }

  void timeStep(long t_long) override {
    SimTime t{t_long};
    DataTime td{t};

    if (prm.t == Nt_enable_mechanics) {
      log_msg("mechanics enabled");
      prm.mechanics = true;
    }

    if (prm.t == std::max(0l, prm.Nt_assim_on - prm.tw)) {
      if (load_groundtruth_at_startup) {
        loadGroundTruth(prm.t);
      } else {
        Tissue.set_uv(0, 0.7);
      }
    }

    if (prm.t == prm.Nt_assim_on) {
      log_msg("assimilation started", prm.t);
      apply_assim = true;
    }

    if (prm.t == prm.Nt_assim_off) {
      log_msg("assimilation stopped");
      apply_assim = false;
    }

    if (td.next().available()) {
      ReadBinaryFileMechanics(td);
    }

    if (prm.t - prm.tw_da > 0) {
      Tissue.timeStep();
    }

    if (apply_assim) {
      rate_tissue->compute();

      auto data = get_nearest_renorm(t);

      const num dt = abs(t - data.second);

      //log_msg("dt {}", dt);

      const num scale = clamp(1 - prm.scalefct * dt / prm.tw_da);

      //log_msg("scale {}", scale);

      assim.apply_sync_renorm(Tissue, *data.first, scale);

    } else {
      assim.assimilated = false;
    }

    if (write_output && prm.t % prm.tw == 0) {
      Tissue.writeBinaryFileElectro();
      Tissue.writeBinaryFileMechanics();

      if (assim.enable_debug_assim) assim.debug_assim.write();

      //rate_data->write();
      //rate_tissue->write();

      //rd_noised->write();
      //
      //rd_filtered->write();
      //rd_renorm->write();
      //rd_filtered->write();
      //
      //rate_diffs->compute();
      //rate_diffs->write();
    }
  }

  std::pair<Array3<num> *, SimTime> get_nearest_renorm(DataTime t) {
    DataTime nearst_t = t.nearestData();
    if (nearst_t == renormedData.t_next) {
      return {renormedData.next, renormedData.t_next};
    } else if (nearst_t == renormedData.t_prev) {
      return {renormedData.prev, renormedData.t_prev};
    } else if (prm.t > prm.Nt - prm.tw_da - prm.da_delay) {
      return {renormedData.next, renormedData.t_next};
    } else
      FATAL_ERROR("prm.t {}, prm.Nt {}, nearest_t {}, {}, {}, {}", prm.t, prm.Nt, nearst_t.t(),
                  renormedData.t_next.t(), renormedData.t_prev.t(),
                  prm.Nt - prm.tw_da - prm.da_delay);
  }

  void ReadBinaryFileMechanics(long t) final {
    FATAL_ERROR("use readBinaryFileMechanics(DataTime )");
  }

  void ReadBinaryFileMechanics(DataTime t) {
    rate_data->compute();
    renormedData.loadnext(t);
  }

  void postRun() override {
    // TODO
    //if (write_output) {
    //  renormedData.renorm.saveMinMaxDB(simDataStreams->prefix, RateType::Volumechange);
    //}

    DataAssimilationSimulation::postRun();
  }

  void reset() override {
    DataAssimilationSimulation::reset();
    renormedData.reset();
    load_groundtruth_at_startup = false;
  }
};


