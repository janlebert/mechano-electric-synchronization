#pragma once

#include <stdexcept>
#include <random>
#include <utility>

#include "myocardium.h"
#include "filters/noiseadder.h"

class Spirals {
 protected:
  Myocardium &tissue;

  NoiseAdder noiseAdder;
  const bool add_init_noise = false;

  num u_init() {
    if (!add_init_noise) return u_init_value;

    return clamp(u_init_value + noiseAdder.random_number());
  }

  num v_init() {
    if (!add_init_noise) return v_init_value;

    return clamp(v_init_value + noiseAdder.random_number());
  }

 public:
  const int N;
  const int Nz;
  const int width;
  const num u_init_value = 0.9;
  const num v_init_value = 0.95;

  Spirals(Myocardium &tissue, const int width = 10)
      : tissue(tissue),
        noiseAdder(0.1),
        add_init_noise(prm.init_with_noise),
        N(prm.Nx - 1),
        Nz(prm.Nz),
        width(width){};

  void induce_spiral(string init_method) {
    if (!prm.init_from_files.empty()) {
      tissue.load_uvT(prm.init_from_files);
      return;
    }

    if (init_method == "none") {
      // Don't do anything
    } else if (init_method == "1spiral") {
      induce_one_spiral();
    } else if (init_method == "2spirals") {
      induce_two_spirals();
    } else {
      FATAL_ERROR("Invalid number of spirals to induce!");
    }
  }

  void induce_one_spiral() {
    log_msg("inducing one spiral");

    for (int x = N / 2 - width; x <= N / 2 + width; x++) {
      for (int y = N / 2; y <= N; y++) {
        for (int z = 0; z < Nz; z++) {
          tissue.getMyocyte(clamp_pos({x, y, z}))->writev(v_init());
        }
      }
    }

    for (int x = N / 2 - 2 * width; x <= N / 2; x++) {
      for (int y = N / 2; y <= N; y++) {
        for (int z = 0; z < Nz; z++) {
          tissue.getMyocyte(clamp_pos({x, y, z}))->writeu(u_init());
        }
      }
    }
  }

  void induce_two_spirals() {
    log_msg("inducing two spirals");

    const int third = N / 4;
    for (int x = N / 2 - width; x <= N / 2 + width; x++) {
      for (int y = 0; y < third; y++) {
        for (int z = 0; z < Nz; z++) {
          tissue.getMyocyte(clamp_pos({x, y, z}))->writev(v_init());
        }
      }

      for (int y = 2 * third; y < N; y++) {
        for (int z = 0; z < Nz; z++) {
          tissue.getMyocyte(clamp_pos({x, y, z}))->writev(v_init());
        }
      }
    }

    for (int x = N / 2 - 3 * width; x <= N / 2; x++) {
      for (int y = 0; y < third; y++) {
        for (int z = 0; z < Nz; z++) {
          tissue.getMyocyte(clamp_pos({x, y, z}))->writeu(u_init());
        }
      }

      for (int y = 3 * third; y < N; y++) {
        for (int z = 0; z < Nz; z++) {
          tissue.getMyocyte(clamp_pos({x, y, z}))->writeu(u_init());
        }
      }
    }
  }
};
