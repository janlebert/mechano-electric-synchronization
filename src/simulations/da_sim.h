#pragma once

#include "simulation.h"
#include "data_assim/rate_factory.h"

class DataTime;

class SimTime {
  long m_t;

 public:
  explicit SimTime(long sim_t) : m_t(sim_t) {}
  explicit SimTime(const DataTime& data_t);

  operator DataTime() const;

  long t() const { return m_t; }

  bool operator==(const SimTime& other) const { return (other.m_t == m_t); }

  bool operator!=(const SimTime& other) const { return (other.m_t != m_t); }

  friend long operator-(const SimTime& x, const SimTime& y) { return x.m_t - y.m_t; }
};

class DataTime {
  long m_t;
  DataTime(long t) : m_t(t) {}

 public:
  explicit DataTime(SimTime sim_t) : m_t(sim_t.t() + prm.da_delay) {}

  operator SimTime() const { return SimTime(*this); }

  long t() const { return m_t; }

  DataTime next() const { return {m_t + prm.tw_da}; }
  DataTime prev() const { return {m_t - prm.tw_da}; }

  bool operator==(const DataTime& other) const { return (other.m_t == m_t); }

  bool operator!=(const DataTime& other) const { return (other.m_t != m_t); }

  bool available() const {
    if (m_t > prm.Nt - prm.tw_da) {
      return false;
    }

    return m_t % prm.tw_da == 0;
  };

  DataTime nearestData() const {
    long shift = m_t % prm.tw_da;

    DataTime result{m_t - shift};

    if (shift > prm.tw_da / 2) {
      result = result.next();
    }

    while (result.t() >= prm.Nt - prm.tw_da) {
      result = result.prev();
    }

    ASSERT_ALWAYS(result.t() >= 0);
    return result;
  }
};

SimTime::SimTime(const DataTime& data_t) {
  m_t = data_t.t() - prm.da_delay;
}
SimTime::operator DataTime() const {
  return DataTime(*this);
}

class DataAssimulationSuccess {
 private:
  Array3<num> u_data;
  std::shared_ptr<SimulationDataStreams> simDataStreams;

  // prm.t, MAE, RMSE, MBE, SSIM, assimilated
  vector<std::tuple<long, double, double, double, double, bool>> arena;
  enum Arena { t = 0, MAE = 1, RMSE = 2, MBE = 3, SSIM = 4, assimilated = 5 };

  /*
   * calculate root mean squared error
   */
  double calc_u_RMSE(Myocardium& Tissue) {

    double r = 0;
    for (auto& pos : u_data.positions) {
      const auto d = Tissue.getMyocyte(pos)->getu() - u_data(pos);
      r += d * d;
    }
    const double size = prm.Nx * prm.Ny * prm.Nz;
    const auto MSE    = r / size;
    return sqrt(MSE);
  }

  /*
   * calculate mean absolute error
   */
  double calc_u_MAE(Myocardium& Tissue) {
    double r = 0;
    for (auto& pos : u_data.positions) {
      r += abs(Tissue.getMyocyte(pos)->getu() - u_data(pos));
    }
    const double size = prm.Nx * prm.Ny * prm.Nz;
    return r / size;
  }

  /*
   * calculate mean bias error
   */
  double calc_u_MBE(Myocardium& Tissue) {
    double r = 0;
    for (auto& pos : u_data.positions) {
      r += Tissue.getMyocyte(pos)->getu() - u_data(pos);
    }
    const double size = prm.Nx * prm.Ny * prm.Nz;
    return r / size;
  }

  double calc_u_SSIM(Myocardium& Tissue) {

    auto getu = [&Tissue](IdxVec pos) -> num { return Tissue.getMyocyte(pos)->getu(); };

    PosFuncHolder<num, decltype(getu)> x(prm.dims, getu);

    return Array::SSIM(u_data, x);
  }

  template <size_t I>
  vector<double> get_udiff() const {
    vector<double> data(arena.size());
    std::transform(arena.begin(), arena.end(), data.begin(), [](auto x) { return std::get<I>(x); });
    return data;
  }

  template <size_t I>
  vector<double> get_udiff(bool assimilated) const {
    vector<double> d;
    for (const auto& v : arena) {
      if (std::get<Arena::assimilated>(v) == assimilated) {
        d.push_back(std::get<I>(v));
      }
    }
    return d;
  }

 public:
  DataAssimulationSuccess(std::shared_ptr<SimulationDataStreams> simData)
      : u_data(prm.dims), simDataStreams(simData) {
    arena.reserve(prm.Nt / prm.tw);
  }

  void reset() { arena.clear(); }

  void timeStep(Myocardium& Tissue, bool assimilated) {
    if (prm.t % prm.tw == 0 && prm.t > 0) {
      u_data.load(simDataStreams->u, prm.t);

      arena.emplace_back(prm.t, calc_u_MAE(Tissue), calc_u_RMSE(Tissue), calc_u_MBE(Tissue),
                         calc_u_SSIM(Tissue), assimilated);
    }
  }

  vector<double> get_udiff_MAE() const { return get_udiff<Arena::MAE>(); }
  vector<double> get_udiff_MAE(bool assimilated) const { return get_udiff<Arena::MAE>(assimilated); }
  vector<double> get_udiff_RMSE() const { return get_udiff<Arena::RMSE>(); }
  vector<double> get_udiff_RMSE(bool assimilated) const {
    return get_udiff<Arena::RMSE>(assimilated);
  }
  vector<double> get_udiff_SSIM() const { return get_udiff<Arena::SSIM>(); }
  vector<double> get_udiff_SSIM(bool assimilated) const {
    return get_udiff<Arena::SSIM>(assimilated);
  }

  void logSuccessMetrics(bool save_vecs) const {
    auto log_mean_stdev = [](const string& label, const vector<double>& v) {
      auto mean_stdev = Array::mean_stdev(v);
      log_msg("{} mean: {}, stdev {}", label, mean_stdev.first, mean_stdev.second);
    };

    log_mean_stdev("u MAE", get_udiff_MAE());
    log_mean_stdev("u MAE non-assimilated", get_udiff_MAE(false));
    log_mean_stdev("u RMSE", get_udiff_RMSE());
    log_mean_stdev("u RMSE non-assimilated", get_udiff_RMSE(false));
    log_mean_stdev("u SSIM", get_udiff_SSIM());
    log_mean_stdev("u SSIM non-assimilated", get_udiff_SSIM(false));

    if (save_vecs) {
      auto fn = fmt::format("{}_udiff.data", prm.filename_prefix);
      io::deleteFile(fn);
      std::ofstream out(fn, ios::out);

      fmt::print(out, "t    MAE    RMSE    MBE    SSIM    assimilated\n");
      for (const auto& s : arena) {
        fmt::print(out, "{}    {}    {}    {}    {}\n", std::get<Arena::t>(s),
                   std::get<Arena::MAE>(s), std::get<Arena::RMSE>(s), std::get<Arena::MBE>(s),
                   std::get<Arena::SSIM>(s), std::get<Arena::assimilated>(s));
      }
    }
  }
};


class DataAssimilationSimulation : public Simulation {
 public:
  std::shared_ptr<SimulationDataStreams> simDataStreams;

  Array3<Vec3> pos3D;

  RateType rt;

  DataAssim assim;

  std::shared_ptr<Rate3D> rate_data;
  std::shared_ptr<Rate3D> rate_tissue;
  std::unique_ptr<FloatDiffRate3D> rate_diffs;

  bool apply_assim = false;
  num da_k;

  DataAssimulationSuccess assimulationSuccess;

  DataAssimilationSimulation(bool write_output,
                             std::shared_ptr<SimulationDataStreams> simData,
                             RateType rt)
      : Simulation(write_output),
        simDataStreams(simData),
        pos3D(prm.dims),
        rt(rt),
        da_k(prm.da_k),
        assimulationSuccess(simData) {

    prm.Nt -= prm.Ntw;

    // Check the filelength of the simulation Data
    long maxNt = simData->maxNt();
    if (prm.Nt > maxNt || prm.Nt <= 0) {
      log_msg("[WARNING] simulation data is smaller than expected, new Nt is now {} (expected {})",
              maxNt, prm.Nt);
      prm.Nt = maxNt;
    }

    prm.da_k      = da_k / 10;
    prm.initm     = 1;
    prm.mechanics = false;
  }

  ~DataAssimilationSimulation() override {
    // restore prm.Nt
    prm.Nt += prm.Ntw;

    prm.globalVariablesReset();
  }

  void run() override {
    for (; prm.t < prm.Nt; prm.t++) {

      timeStep(prm.t);

      if (apply_assim && (prm.da_k < da_k)) {
        prm.da_k += da_k / 100;
      }

      assimulationSuccess.timeStep(Tissue, assim.assimilated);

      printProgress();
    }
    printProgress();

    postRun();
  };

  virtual void postRun() { assimulationSuccess.logSuccessMetrics(write_output); }

  virtual void ReadBinaryFileMechanics(const long t) { pos3D.load(simDataStreams->pos, t); }

  void loadGroundTruth(long t) {
    Tissue.load_pos(simDataStreams->pos.getFilePath(), t);
    Tissue.load_u(simDataStreams->u.getFilePath(), t);

    if (io::fileExists(simDataStreams->prefix() + "_v.dat")) {
      Tissue.load_v(simDataStreams->prefix() + "_v.dat", t);
    }

    if (io::fileExists(simDataStreams->prefix() + "_T.dat")) {
      Tissue.load_T(simDataStreams->prefix() + "_T.dat", t);
    }
  }

  virtual void reset() {
    prm.globalVariablesReset();
    Tissue.resetMedium();
    assimulationSuccess.reset();

    prm.da_k      = da_k / 10;
    prm.initm     = 1;
    prm.mechanics = false;
    apply_assim   = false;
  }
};
