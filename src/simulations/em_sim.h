#pragma once

#include <cmath>
#include <cstdlib>

#include "simulation.h"
#include "induce_spirals.h"

extern Parameters prm;

class EmSimulation : public Simulation {
 public:
  Spirals spirals;

  const num D_final;
  const long t_D_start;
  const long t_D_stop;
  num D_increase_factor;

  EmSimulation(bool write_output)
      : Simulation(write_output),
        spirals(Tissue, prm.init_spiral_width),
        D_final(prm.D),
        t_D_start(prm.t_D_start),
        t_D_stop(prm.t_D_end) {

    if (write_output) {
      Tissue.writeBinaryFileElectro();
      Tissue.writeBinaryFileMechanics();
    }

    spirals.induce_spiral(prm.init_spiral_method);
    if (write_output) {  // write out uv snapshot
      Tissue.snapshot_uv(prm.filename_prefix + "_snapshot_spiralinit");
    }

    prm.D /= prm.D_init_divisor;
    log_msg("D = {}", prm.D);

    // Increase D by a fixed factor in each time step
    if ((prm.D == D_final) || (t_D_stop - t_D_start == 0)) {
      D_increase_factor = 1;
    } else {
      D_increase_factor = std::pow(D_final / prm.D, 1.0 / static_cast<double>(t_D_stop - t_D_start));
    }
  }

  void run() override {
    for (long t = 0; t < prm.Nt; t++) {
      timeStep(t);
      prm.t += 1;
      printProgress();
    }

    if (write_output) {  // write out one last uvT snapshot
      Tissue.snapshot_uvT(prm.filename_prefix + "_snapshot_final");
    }
  };

  void timeStep(long t) override {
    Tissue.timeStep();

    /* simple stepping of diffusion constant to obtain a large rotor at t=100000 */
    if (prm.t == t_D_start) {
      log_msg("starting to increase D");
    }

    if (prm.t > t_D_start && prm.t < t_D_stop) {
      prm.D *= D_increase_factor;
    }

    if (prm.t == t_D_stop) {
      prm.D = D_final;  // fix numerical inaccuracies
      log_msg("final D = {} reached", prm.D);
      checkElectricalActivity();  // if activity died out exit
    }

    /* turn on mechanics */
    if (prm.t == prm.Ntm) {
      checkElectricalActivity();  // if activity died out exit

      if (write_output) {  // write out uv snapshot
        Tissue.snapshot_uv(prm.filename_prefix + "_snapshot_premech");
      }

      prm.mechanics = true;
      log_msg("turned mechanics on");
    }

    if (write_output && prm.t == prm.Ntw) {
      prm.initm = 1;
      log_msg("starting to write out");
    }

    if (write_output && prm.t > prm.Ntw && prm.t % prm.tw == 0) {
      Tissue.writeBinaryFileElectro();
      Tissue.writeBinaryFileMechanics();
    }


    if (t % 1000 == 0) {
      // if activity died out exit
      checkElectricalActivity();
      // if mechanical mesh explodes then stop
      checkMechanicalActivity();
    }

    if (prm.mechanics && prm.initm < 1) {
      // turn on mechanics slowly to avoid numerical instabilities
      prm.initm += 0.001;

      if (prm.initm > 1) prm.initm = 1;
    }
  }

  void checkElectricalActivity() {
    if (!write_output) {  // Don't check if we don't write output, probably we are in a live viewer
      return;
    }

    num u, max_u = 0;

    for (const auto& pos : prm.positions) {
      u = Tissue.getMyocyte(pos)->getu();
      if (u > max_u) {
        max_u = u;
      }
    }

    if (max_u <= 0.005) {
      log_msg("[ERROR] all excitation decayed, simulation stopped, exiting");
      std::exit(EXIT_FAILURE);
    } else if (max_u > 10) {
      log_msg("[ERROR] maximum excitation is larger than 10 – unstable; exiting");
      std::exit(EXIT_FAILURE);
    }
  }

  void checkMechanicalActivity() {
    if (!prm.mechanics ||
        !write_output) {  // Don't check if we don't write output, probably we are in a live viewer
      return;
    }

    num dist, max_dist = 0;

    for (const auto& pos : prm.positions) {
      Vec3 p = Tissue.getMyocyte(pos)->getPos();

      dist = p.length();
      if (dist > max_dist) {
        max_dist = dist;
      }
    }

    if (max_dist >= 1000) {
      log_msg("[ERROR] mesh exploded, exiting");
      std::exit(EXIT_FAILURE);
    }
  }
};
