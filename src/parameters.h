#pragma once

#include "helper/parameter_handler.h"
#include "helper/iterators.h"

class Parameters : public ParameterHandler {
 public:
  using ParameterHandler::ParameterHandler;

  /* global counters and variables */
  // global time counter
  long t = 0;
  // for smooth initiation of mechanical contraction at the onset of mechanical simulation
  num initm = 0;


  // WIP
  num scalefct = 5;

  bool mechanics         = false;
  bool electrophysiology = true;

  // Iteratate through all valid IdxVec's (without padding), e.g. for(auto& pos : prm.positions) { … }
  Positions positions = Positions({0, 0, 0});
  // Iteratate through all valid IdxVec's with padding
  Positions positions_padded = Positions({0, 0, 0});

  /* variables to loaded parameter file */
  /* simulation */
  long Nt;      // Total simulation time
  long Ntw;     // Timepoint from which to start writing out
  long Ntm;     // Timepoint when to enable mechanics
  int tw;       // write out every `tw` step, sufficient for visualization purposes
  bool noflux;  // True: zero-flux boundaries, False: 0-Dirichlet boundary conditions

  /* simulation init */
  string init_spiral_method;  // See induce_spirals.h for values
  int init_spiral_width;      // Block width for inducting spirals, see induce_spirals.h
  bool init_with_noise;       // Add noise to initial uv values
  num D_init_divisor;         // Initial value for Aliev-Panfilov Diffusion to start spirals
  long t_D_start;             // Time point when to start increasing D
  long t_D_end;               // Time point when the D should be at the final value
  string init_from_files;

  /* system size */
  int Nx;  // system dimensions
  int Ny;
  int Nz;
  IdxVec dims;  // identical to {Nx, Ny, Nz}
  IdxVec padding_dims;
  int Lx;  // padded dimensions, = dims + 2 * padding_dims
  int Ly;
  int Lz;
  IdxVec dims_padded;  // identical to {Lx, Ly, Lz}
  num spacing;

  /* settings */
  bool reilly_laplace_stencil;
  num dt_e;  // Timestep electrophysiology
  num dt_m;  // Timestep mechanics
  int ite;   // howmany electrophysiology updates in timestep
  int itm;   // howmany mechanics updates in timestep
  string filename_prefix;
  string da_filename_prefix;
  string da_pos_suffix;

  /* Aliev-Panfilov */
  num a;
  num b;    // not necessary in step model
  num mu1;  // not necessary in step model
  num mu2;  // not necessary in step model
  num k;
  num epsilon;  // not necessary in step model
  num D;        // Diffusion, larger == larger wave

  /* elasticity */
  num kT;  // epsilon, the larger this number the larger T variable and the larger contraction
  num cs;  // parameter for contraction strength
  num myofibcontr;
  num ksi;   // spring parameter isotropic
  num ksai;  // spring parameter anisotropic
  num ks1, ks2, ks3, ks4, ks5, ks6;
  num ksv;  // volume springs, preserve hexaedral
  num hk;   // spring parameter for home springs
  Vec3 linear_fiber;
  Vec3 linear_hint;
  num fiber_zmax_angle;

  /* data assimilation */
  num da_k;
  IdxVec sensor_size;
  IdxVec sensor_spacing;
  num da_noise_stdev;
  num da_smoothing_sigma;
  int da_delay;
  int tw_da;
  long Nt_assim_on;
  long Nt_assim_off;
  num assim_scale_negative;

  /* viewer */
  int frameskip;
  int z_slice;
  int refinement;
  string movie_filename;
  int fps;

  void globalVariablesReset() {
    t     = 0;
    initm = 0;

    mechanics         = false;
    electrophysiology = true;
  }

  void load_file(string filepath, string logfilename = "") { load(filepath, logfilename, true); }

  void load_string(string input, string logfilename = "") { load(input, logfilename, false); }

  void load(string str, string logfilename = "", bool load_str_as_file = true) {
    globalVariablesReset();

    this->init(str, logfilename, load_str_as_file);

    enter_section("simulation");
    Nt  = get_integer("Nt", 60000, "Total simulation time");
    Ntm = get_integer("Ntm", 15000, "Time when to enable mechanics");
    Ntw = get_integer("Ntw", 20000, "Time when to start writing output");
    tw  = get_integer("tw", 10, "write output every … time steps");
    ASSERT_ALWAYS(tw > 0);
    noflux = get_boolean("noflux", true,
                         "true: Zero-flux boundary conditions"
                         "\tfalse: 0-Dirichlet boundary conditions");
    leave_section();




    enter_section("simulation init");
    init_spiral_method = get("spiral initialization method", "1spiral");
    init_spiral_width  = get_integer("spiral initialization width", 10);
    init_with_noise    = get_boolean("spiral initialization noise", false);
    D_init_divisor     = get_real("D init divisor", 1,
                              "Divide diffusion constant by … at start of simulation "
                              "and slowly increase D back to the original value");
    t_D_start          = get_integer("t_D start", -1, "Time point when to start increasing D");
    t_D_end = get_integer("t_D end", -1, "Time point when the D should be at the final value");
    if (D_init_divisor != 1) {
      ASSERT_ALWAYS(t_D_start <= t_D_end && t_D_end >= t_D_start,
                    "t_D start and end configuration invalid!");
    }
    init_from_files =
        get("init filename prefix", "", "Initialize uvT from files, overrides other init methods");
    leave_section();




    enter_section("system size");
    dims         = get_IdxVec("dims", {100, 100, 10}, "system dimensions");
    padding_dims = get_IdxVec("padding", {10, 10, 10}, "elastomechanical padding on each side");
    dims_padded  = dims + 2 * padding_dims;
    Nx           = dims[0];
    Ny           = dims[1];
    Nz           = dims[2];
    Lx           = dims_padded[0];
    Ly           = dims_padded[1];
    Lz           = dims_padded[2];

    spacing = get_real("spacing", 1);
    leave_section();




    /* settings */
    enter_section("settings");
    reilly_laplace_stencil = get_boolean("reilly laplace stencil", false);
    dt_e                   = get_real("dt_e", 0.02, "timestep electrics");
    dt_m                   = get_real("dt_m", 0.035, "timestep mechanics");
    ite                    = get_integer("ite", 1, "electrics iterations");
    itm                    = get_integer("itm", 2, "mechanics iterations");
    filename_prefix        = get("filename_prefix", "prefix");
    da_filename_prefix     = get("da_filename_prefix", "da_prefix");
    da_pos_suffix          = get("da_pos_suffix", "_pos.dat");
    leave_section();




    enter_section("Aliev-Panfilov");
    a       = get_real("a", 0.09, "du/dt = -k*u*(u - a)*(u - 1) - u*v + D*laplace");
    b       = get_real("b", 0.20, "dv/dt = (epsilon + mu1*v/(mu2 + u)) * (-v - k*u*(u - b - 1))");
    mu1     = get_real("mu1", 0.1);
    mu2     = get_real("mu2", 0.1);
    k       = get_real("k", 8.0);
    epsilon = get_real("epsilon", 0.01);
    D       = get_real("D", 0.003);
    leave_section();




    enter_section("elasticity");
    kT = get_real("kT", 3.0, "dT/dt = (epsilon + mu1*T/(mu2 + u)) * (-T - kT*u*(u - b - 1))");
    cs = get_real("cs", 10.0);
    myofibcontr = get_real("myofibcontr", 0.8);
    ksi         = get_real("ksi", 0.5);
    ksai        = get_real("ksai", 0.5, "F1 = - nl1*myofibcontr*ksai*(L1 - L01/(1 + cs*T))");
    ks1 = ksai;
    ks2 = ksai;
    ks3 = ksai;
    ks4 = ksai;
    ks5 = ksai;
    ks6 = ksai;

    ksv              = get_real("ksv", 5);
    hk               = get_real("hk", 32);
    linear_fiber     = get_Vec3("linear fiber", Vec3(1, 0, 0));
    linear_hint      = get_Vec3("linear hint", Vec3(0, 0, 1));
    fiber_zmax_angle = get_real(
        "zmax fiber angle", 0,
        "rotate fiber orientation with increasing z-layer, maximum rotation angle for z=Nz");
    leave_section();




    enter_section("data assimilation");
    da_k = get_real("k", 1);
    sensor_size = get_IdxVec("sensor size", {std::max(Nx / 10, 1), std::max(Ny / 10, 1), std::max(Nz / 5, 1)});
    sensor_spacing = get_IdxVec("sensor spacing", IdxVec(1, 1, 0));
    da_delay       = get_integer("delay", 0);
    da_noise_stdev = get_real("noise", 0.01,
                              "stdev of addative white Gaussian noise to be added on the vertex positions");
    da_smoothing_sigma = get_real("smoothing", 1, "sigma of Gaussian smoothing of the rate");
    tw_da = get_integer("tw_da", 2, "read every … time steps of data for data assimilation");
    tw_da *= tw;
    ASSERT_ALWAYS(tw_da > 0);

    Nt_assim_on = get_integer("Nt assim on", 0, "time point to switch data assimilation on");
    if (Nt_assim_on < da_delay + 3 * tw_da) {
      Nt_assim_on = da_delay + 3 * tw_da;
    }
    ASSERT_ALWAYS(Nt_assim_on >= 0);
    Nt_assim_off = get_integer("Nt assim off", 0, "time point to switch data assimilation off");
    assim_scale_negative = get_real("assim negative scale", 0.1);
    leave_section();





    enter_section("viewer");
    frameskip  = get_integer("frameskip", 5, "display every … frames which were written out");
    z_slice    = get_integer("z_slice", Nz / 2 - 1, "which z plane to show");
    refinement = get_integer("refinement", 3, "show gridlines every … myocytes");

    movie_filename = get("movie_filename", "output.mp4");
    fps            = get_integer("fps", 15);
    leave_section();





    /* Initialize global variables */
    positions        = Positions(dims);
    positions_padded = Positions(dims_padded);
  }
};