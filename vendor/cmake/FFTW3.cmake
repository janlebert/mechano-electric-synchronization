set(FFTW_BUILD_DIR "${CMAKE_SOURCE_DIR}/vendor/build/fftw3")

# Search FFTW3, if not found build it from source
find_path(FFTW_INCLUDES    NAMES  fftw3.h  HINTS ${FFTW_BUILD_DIR}/include)
find_library(FFTW_LIBRARY  NAMES  fftw3    HINTS ${FFTW_BUILD_DIR}/lib)
find_library(FFTWF_LIBRARY NAMES  fftw3f   HINTS ${FFTW_BUILD_DIR}/lib)
find_library(FFTW_LIBRARY_THREADS  NAMES  fftw3_threads    HINTS ${FFTW_BUILD_DIR}/lib)
find_library(FFTWF_LIBRARY_THREADS NAMES  fftw3f_threads   HINTS ${FFTW_BUILD_DIR}/lib)

if(NOT OPTION_DOUBLES)
    set(FFTW_LIBRARY ${FFTWF_LIBRARY})
    set(FFTW_LIBRARY_THREADS ${FFTWF_LIBRARY_THREADS})
endif()

if(FFTW_INCLUDES AND FFTW_LIBRARY AND FFTW_LIBRARY_THREADS)
    set(FFTW_FOUND TRUE)
    message(STATUS "Found FFTW3: ${FFTW_LIBRARY} ${FFTW_LIBRARY_THREADS}")
else()
    set(FFTW_FOUND FALSE)
    message(WARNING "FFTW3 not found, downloading FFTW3 and builing it from source")

    if(NOT OPTION_DOUBLES)
        list(APPEND FFTW_CMAKE_CMDS -DENABLE_FLOAT=ON)
        set(FFTW_LIBNAME fftw3f)
    else()
        set(FFTW_LIBNAME fftw3)
    endif()

    list(APPEND FFTW_CMAKE_CMDS
            -DENABLE_THREADS=ON
            -DBUILD_SHARED_LIBS=OFF -DBUILD_TESTS=OFF
            -DENABLE_SSE=ON -DENABLE_SSE2=ON
            -DENABLE_AVX=ON
            #-DENABLE_AVX2=ON
            )

    if(CMAKE_COMPILER_IS_GNUCC)
        list(APPEND FFTW_CMAKE_CMDS -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_C_FLAGS=-fPIC)
    endif()

    include(ExternalProject)
    ExternalProject_Add(
            FFTW3-dl
            PREFIX FFTW3-dl

            URL ftp://ftp.fftw.org/pub/fftw/fftw-3.3.8.tar.gz
            URL_MD5 8aac833c943d8e90d51b697b27d4384d
            UPDATE_DISCONNECTED ON

            CMAKE_ARGS
                -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_LIBDIR=lib
                -DCMAKE_POSITION_INDEPENDENT_CODE=ON
                -DCMAKE_INSTALL_PREFIX=${FFTW_BUILD_DIR} ${FFTW_CMAKE_CMDS}
            INSTALL_COMMAND
                ${CMAKE_COMMAND}
                --build .
                --target install
                --config Release
    )

    SET(FFTW_INCLUDES "${FFTW_BUILD_DIR}/include")
    SET(FFTW_LIBRARY  "${FFTW_BUILD_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${FFTW_LIBNAME}${CMAKE_STATIC_LIBRARY_SUFFIX}")
    SET(FFTW_LIBRARY_THREADS  "${FFTW_BUILD_DIR}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${FFTW_LIBNAME}_threads${CMAKE_STATIC_LIBRARY_SUFFIX}")

    file(MAKE_DIRECTORY "${FFTW_INCLUDES}")
endif()

add_library(fftw3 STATIC IMPORTED GLOBAL)
set_target_properties(fftw3 PROPERTIES
        IMPORTED_LOCATION              "${FFTW_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES  "${FFTW_INCLUDES}")
add_library(fftw3_threads STATIC IMPORTED GLOBAL)
set_target_properties(fftw3_threads PROPERTIES
        IMPORTED_LOCATION              "${FFTW_LIBRARY_THREADS}"
        INTERFACE_INCLUDE_DIRECTORIES  "${FFTW_INCLUDES}")
if(NOT FFTW_FOUND)
    add_dependencies(fftw3 FFTW3-ext)
    add_dependencies(fftw3_threads FFTW3-ext)
endif()