This is the code repository for

 > Lebert, J., & Christoph, J. (2019). Synchronization-based reconstruction of electromechanical wave dynamics in elastic excitable media. Chaos: An Interdisciplinary Journal of Nonlinear Science, 29(9), 93117. https://doi.org/10.1063/1.5101041

## Build Requirements

### Linux
Supported compiler versions:
- gcc 5.0 and newer
- clang 3.4 and newer

```bash
sudo apt install build-essential cmake mesa-utils xorg-dev libfftw3-dev
```

### macOS
Download [CMake](https://cmake.org/download/) or install it with Homebrew:

```bash
brew install cmake fftw
```

The dependency on [FFTW](http://www.fftw.org) is optional, it will be downloaded and build automatically if not found.

### Windows
Install [CMake](https://cmake.org/download/) and a C++ compiler like Microsoft Visual C++ or MinGW.

## Compilation

This project uses [CMake](https://cmake.org/) to generate cross-platform build configurations. Either use `cmake-gui` or the terminal from the project folder (where *CMakeList* is located):

```bash
# Create a subfolder (generally called build)
mkdir build
cd build

# Generate a project for the default platform
cmake ..

# Alternatively display the available platforms and generate the project for the platform of your choice
cmake --help    (optional)
cmake .. -G "Xcode"    (example)
cmake .. -G "Unix Makefiles"    (example)
cmake .. -G "Visual Studio 15 2017 Win64"    (example)

# Then use your IDE or use CMake to compile
cmake --build .
```

### Compilation Options

The following compilation options are available, they can be used either with

```bash
cmake -DCMAKE_BUILD_TYPE=Debug ..
```

or the GUIs `cmake-gui` or `ccmake`.

| Option | Description | Default Value |
|--------|-------------|---------------|
| CMAKE_BUILD_TYPE | Type of build, options are: Debug, Release, or RelWithDebInfo | Release |
| BUILD_VIEWER  | build visualizations if OpenGL is available | ON |
| BUILD_EXTRAS | build specialized programms, tests, and old (possibly oudates) programms | OFF |
| RNG_SEED | seed for the random number generator, if empty random seed |  |
| OPTION_DOUBLES | use doubles instead of floats | OFF |
| OPTION_OPTIMIZATION | build for this specific CPU | ON |
| OPTION_WRITEOUT_EVERYTHING | write out additional files with debug information from the simulation | OFF |
| OPTION_STACKTRACES_ON_CRASH | [show stacktraces on crash](https://github.com/bombela/backward-cpp), increases build time but no runtime execution cost (Linux/MacOS only) | OFF |

## How to use it

The main executables are:
- `em_sim` — Electromechanical simulation
- `da` — Data assimilation based on the mechanical deformation of a `em_sim` simulation
- `viewer` — Viewer of the simulation results of `em_sim` and `da`, also capable of running those live

A paramater file `params.ini` is required to run the executables, examples can be found in the [case_studies](case_studies) folder and can be provided with `-p path/to/params.ini` command line option. The command line option `--help` provides an overview of the available command line arguments.

As an example to start a live simulation with visualization

```bash
./viewer live_sim -p ../case_studies/big_spiral/params.ini
```
or to run the data generation and reconstruction:

```bash
./em_sim -p ../case_studies/chaos/params.ini
./da -p ../case_studies/chaos/params.ini
```

For more information use the `--help` command line option.
